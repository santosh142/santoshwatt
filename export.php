<?php
$profileId = 1; #All Products
#$profileId = 16; #OnSale
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$profile = Mage::getModel('dataflow/profile');
$userModel = Mage::getModel('admin/user');
$userModel->setUserId(0);
Mage::getSingleton('admin/session')->setUser($userModel);
$profile->load($profileId);
if (!$profile->getId()) {
 Mage::getSingleton('adminhtml/session')->addError('[ERROR] Invalid ID');
}
Mage::register('current_convert_profile', $profile);
$profile->run();
$recordCount = 0;
?>
