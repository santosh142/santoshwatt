<?php

class WP_Amazonimportproducts_Block_Adminhtml_Import_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('amazonimportproducts_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('amazonimportproducts')->__('Amazon Item Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('amazonimportproducts')->__('General'),
            'title'     => Mage::helper('amazonimportproducts')->__('General'),
            'content'   => $this->getLayout()->createBlock('amazonimportproducts/adminhtml_import_edit_tab_form')->toHtml(),
        ));

        $this->addTab('attributes_section', array(
            'label'     => $this->__('Additional Attributes'),
            'title'     => $this->__('Additional Amazon API attributes'),
            'content'   => $this->getLayout()->createBlock('amazonimportproducts/adminhtml_import_edit_tab_attributes')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
