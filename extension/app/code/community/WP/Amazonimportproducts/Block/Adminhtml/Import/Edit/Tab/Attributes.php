<?php

class WP_Amazonimportproducts_Block_Adminhtml_Import_Edit_Tab_Attributes
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('amazonimportproducts/widget/form/renderer/fieldset.phtml');
        $fieldset = $form->addFieldset('amazonimportproducts_attributes', array(
            'legend'        => Mage::helper('amazonimportproducts')->__('Additional Attributes'),
            'comment'       => Mage::helper('amazonimportproducts')->__(
                'You can use this <b>Amazon API attributes</b> for update Magento attributes through <a target="_blank" href="%s">Attributes Mapping</a>.',
                Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit/section/amazon_import_products', array('_fragment' => 'amazon_import_products_attributes_mapping-head'))),
            'class'         => 'fieldset-wide fieldset-additional-attributes',
        ))->setRenderer($renderer);

        $fieldset->addType('extended_text','WP_Amazonimportproducts_Block_Html_Extendedtext');

        $item = $this->_getAmazonItem();
        $fields = Mage::helper('amazonimportproducts')->getAttributesMap($item);
        foreach ($fields as $fieldId => $field) {
            $fieldset->addField('attr_' . $fieldId, 'extended_text', array(
                'label'     => $field['placeholder'],
                'value'     => $field['value'],
                'required'  => false,
                'disabled'  => true,
            ));
        }

        return parent::_prepareForm();
    }

    private function _getAmazonItem()
    {
        $data = Mage::registry('amazonimportproducts_data')->getData();
        $item = unserialize($data['data_serialized']);
        return $item;
    }
}
