<?php

class WP_Amazonimportproducts_Block_Adminhtml_Import_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('amazonimportproductsGrid');
        $this->setDefaultSort('amazonimportproducts_id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setVarNameFilter('filter');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amazonimportproducts/amazonimportproducts')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getColumnFilters()
    {
        return array('amazonprice' => 'amazonimportproducts/adminhtml_grid_column_filter_price');
    }

    protected function _prepareColumns()
    {
        $this->addColumn('amazonimportproducts_id',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'amazonimportproducts_id',
        ));

        $locales = Mage::getModel('amazonimportproducts/source_local')->getOptionArray();
        $this->addColumn('amazon_local', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Local'),
            'align'     => 'left',
            'width'     => '100px',
            'index'     => 'amazon_local',
            'type'      => 'options',
            'options'   => $locales,
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('amazonimportproducts')->__('SKU'),
            'align'     => 'left',
            'width'     => '100px',
            'index'     => 'sku',
        ));

        $this->addColumn('categories', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Categories'),
            'align'     => 'left',
            'width'     => '150px',
            'index'     => 'categories',
            'sortable'  => false,
        ));

        $this->addColumn('title', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Name'),
            'align'     => 'left',
            'index'     => 'title',
        ));

        $currency = Mage::getSingleton('amazonimportproducts/source_currency')->getOptionArray();
        $this->addColumn('currency_code', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Currency'),
            'width'     => '50px',
            'align'     => 'center',
            'index'     => 'currency_code',
            'type'      => 'options',
            'options'   => $currency,
        ));

        $this->addColumn('price', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Converted Price *'),
            'width'     => '100px',
            'align'     => 'right',
            'index'     => 'price',
            'renderer'  => 'amazonimportproducts/adminhtml_grid_column_renderer_price',
            'type'      => 'currency',
            'currency_code' => $this->_getStore()->getBaseCurrency()->getCode(),
            'header_css_class' => 'conv_price',
        ));

        $this->addColumn('quantity', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Qty'),
            'width'     => '100px',
            'align'     => 'right',
            'index'     => 'quantity',
            'type'      => 'number',
        ));

        $statuses = Mage::getSingleton('amazonimportproducts/source_stock')->getOptionArray();
        $this->addColumn('is_in_stock', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Stock'),
            'width'     => '90px',
            'align'     => 'right',
            'index'     => 'is_in_stock',
            'type'      => 'options',
            'options'   => $statuses,
        ));

        $this->addColumn('images_count', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Images'),
            'width'     => '100px',
            'align'     => 'right',
            'index'     => 'images_count',
            'type'      => 'number',
        ));

        $statuses = Mage::getModel('amazonimportproducts/source_status')->toOptionArray();
        $this->addColumn('status', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Import Status'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => $statuses,
        ));

        $this->addColumn('amazon_product_url', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Amazon'),
            'filter'    => false,
            'sortable'  => false,
            'width'     => '70px',
            'index'     => 'amazon_product_url',
            'align'     => 'center',
            'renderer'  => 'amazonimportproducts/adminhtml_grid_column_renderer_amazonlink',
        ));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('amazonimportproducts')->__('Action'),
                'width'     => '70px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('amazonimportproducts')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('amazonimportproducts')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('amazonimportproducts')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('amazonimportproducts_id');
        $this->getMassactionBlock()->setFormFieldName('amazonimportproducts');

        $this->getMassactionBlock()->addItem('import', array(
             'label'    => Mage::helper('amazonimportproducts')->__('Import only selected'),
             'url'      => 'forceImport',
        ));

        $statuses = Mage::getSingleton('amazonimportproducts/source_status')->toOptionArray();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('amazonimportproducts')->__('Change import status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('amazonimportproducts')->__('Import Status'),
                         'values' => $statuses
                     )
             )
        ));

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('amazonimportproducts')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('amazonimportproducts')->__('Are you sure?')
        ));
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
}
