<?php

class WP_Amazonimportproducts_Block_Adminhtml_Grid_Column_Renderer_Synclinks
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $rowData = $row->getData();
        $entityId = $rowData['entity_id'];
        $link = WP_Amazonimportproducts_Model_Amazonoffer::getProductOfferUrl($rowData['wp_amazon_product_url'], $rowData['wp_amazon_offer_id']);
        $formatLink = '<a onclick="SyncProduct(\'' . $entityId . '\'); return false;" title="' . Mage::helper('amazonimportproducts')->__('Run the synchronization process only for this product') . '" href="javascript:;">' . Mage::helper('amazonimportproducts')->__('Sync') . '</a> | ' .
        '<a onclick="ReloadProduct(\'' . $entityId . '\'); return false;" title="' . Mage::helper('amazonimportproducts')->__('Reload all data of this product from Amazon website') . '" href="javascript:;">' . Mage::helper('amazonimportproducts')->__('Reload') . '</a> | ' .
        '<a title="' . Mage::helper('amazonimportproducts')->__('Go to the product page on Amazon website') . '" target="_blank" href="' . $link . '">' . Mage::helper('amazonimportproducts')->__('To link') . '</a>';
        return $formatLink;
    }
}
