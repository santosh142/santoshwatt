<?php
class WP_Amazonimportproducts_Block_Adminhtml_Import extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_import';
        $this->_blockGroup = 'amazonimportproducts';
        $this->_headerText = Mage::helper('amazonimportproducts')->__('WebAndPeople: Import Products from Amazon');
        parent::__construct();
        $this->_removeButton('add');

        $this->_addButton('add', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Find Products'),
            'onclick'   => 'SearchParamsFormSubmit(); return false;',
            'class'     => 'add',
        ), 0, 1);

        $this->_addButton('import', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Import Products'),
            'title'     => Mage::helper('amazonimportproducts')->__('Will import all found the products, if not marked specially'),
            'onclick'   => 'ImportProductsList(); return false;',
            'class'     => 'save',
        ), 0, 2);

        $this->_addButton('clear', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Clear List'),
            'onclick'   => 'clearImportList(); return false;',
            'class'     => 'delete',
        ), 0, 3);
    }

    protected function _prepareLayout()
    {
        $this->setChild('grid', $this->getLayout()->createBlock('amazonimportproducts/adminhtml_import_grid'));
        return parent::_prepareLayout();
    }
}
