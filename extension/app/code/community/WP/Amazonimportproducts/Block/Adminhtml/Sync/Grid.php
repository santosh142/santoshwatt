<?php

class WP_Amazonimportproducts_Block_Adminhtml_Sync_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setVarNameFilter('product_filter');
        $this->setUseAjax(true);
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection()
    {
        $store = $this->_getStore();
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('wp_amazon_asin', array('neq' => ''))
            ->addAttributeToSelect('wp_amazon_sync')
            ->addAttributeToSelect('wp_amazon_asin')
            ->addAttributeToSelect('wp_amazon_local')
            ->addAttributeToSelect('wp_amazon_offer_id')
            ->addAttributeToSelect('wp_amazon_last_update')
            ->addAttributeToSelect('wp_amazon_product_url')
            ->addAttributeToSelect('amazon_product_url')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('type_id')
            ->joinField('is_in_stock',
                'cataloginventory/stock_item',
                'is_in_stock',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left')
            ->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');

        if ($store->getId()) {
            //$collection->setStoreId($store->getId());
            $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
            $collection->addStoreFilter($store);
            $collection->joinAttribute('name', 'catalog_product/name', 'entity_id', null, 'inner', $adminStore);
            $collection->joinAttribute('custom_name', 'catalog_product/name', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute('price', 'catalog_product/price', 'entity_id', null, 'left', $store->getId());
        } else {
            $collection->addAttributeToSelect('price');
            $collection->addAttributeToSelect('status');
            $collection->addAttributeToSelect('visibility');
        }

        $this->setCollection($collection);

        parent::_prepareCollection();
        $this->getCollection()->addWebsiteNamesToResult();
        return $this;
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField('websites',
                    'catalog/product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left');
            }
        }
        return parent::_addColumnFilterToCollection($column);
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'entity_id',
        ));

        $locales = Mage::getModel('amazonimportproducts/source_local')->getOptionArray();
        $this->addColumn('wp_amazon_local',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('Local'),
                'width' => '100px',
                'index' => 'wp_amazon_local',
                'type'      => 'options',
                'options'   => $locales,
        ));

        $this->addColumn('wp_amazon_asin',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('ASIN'),
                'width' => '100px',
                'index' => 'wp_amazon_asin',
        ));

        $this->addColumn('sku',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('SKU'),
                'width' => '100px',
                'index' => 'sku',
        ));

        $this->addColumn('name',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('Name'),
                'index' => 'name',
        ));

        $store = $this->_getStore();
        if ($store->getId()) {
            $this->addColumn('custom_name',
                array(
                    'header'=> Mage::helper('amazonimportproducts')->__('Name in %s', $store->getName()),
                    'index' => 'custom_name',
            ));
        }

        $store = $this->_getStore();
        $this->addColumn('price',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('Price'),
                'type'  => 'currency',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
        ));

        $this->addColumn('qty',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('Qty'),
                'width' => '100px',
                'type'  => 'number',
                'index' => 'qty',
        ));

        $statuses = Mage::getSingleton('amazonimportproducts/source_stock')->getOptionArray(false);
        $this->addColumn('is_in_stock', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Stock'),
            'width'     => '90px',
            'align'     =>'right',
            'index'     => 'is_in_stock',
            'type'      => 'options',
            'options'   => $statuses,
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper( 'catalog' )->__( 'Status' ),
            'width'     => '70px',
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_status')->getOptionArray()
        ));

        $this->addColumn('wp_amazon_last_update', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Last Update'),
            'align'     =>'left',
            'width'     => '160px',
            'index'     => 'wp_amazon_last_update',
            'type'      => 'datetime',
        ));

        $statuses = Mage::getModel('amazonimportproducts/source_status')->toOptionArray();
        $this->addColumn('wp_amazon_sync',
            array(
                'header'=> Mage::helper('amazonimportproducts')->__('Sync Status'),
                'width' => '80px',
                'index' => 'wp_amazon_sync',
                'type'  => 'options',
                'options' => $statuses,
        ));

        $this->addColumn('wp_amazon_product_url', array(
            'header'    => Mage::helper('amazonimportproducts')->__('Amazon'),
            'filter'    => false,
            'sortable'  => false,
            'width'     => '135px',
            'index'     => 'wp_amazon_product_url',
            'align'     => 'center',
            'renderer'  => 'amazonimportproducts/adminhtml_grid_column_renderer_synclinks',
        ));

        $this->addColumn('action',
            array(
                'header'    => Mage::helper('amazonimportproducts')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('amazonimportproducts')->__('Edit'),
                        'url'     => array(
                            'base'=>'adminhtml/catalog_product/edit',
                            'params'=>array('store'=>$this->getRequest()->getParam('store'))
                        ),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('amazonimportproducts')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('amazonimportproducts')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> Mage::helper('catalog')->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
             'confirm' => Mage::helper('catalog')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('amazonimportproducts/source_status')->toOptionArray();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('wp_amazon_sync', array(
             'label'=> Mage::helper('amazonimportproducts')->__('Change sync status'),
             'url'  => $this->getUrl('*/*/massSyncStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'wp_amazon_sync',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('amazonimportproducts')->__('Sync Status'),
                         'values' => $statuses
                     )
             )
        ));

        $statuses = Mage::getSingleton('catalog/product_status')->getOptionArray();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('catalog')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('catalog')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));

        Mage::dispatchEvent('amazonimportproducts_sync_product_grid_prepare_massaction', array('block' => $this));
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/catalog_product/edit', array(
            'store'=>$this->getRequest()->getParam('store'),
            'id'=>$row->getId())
        );
    }
}
