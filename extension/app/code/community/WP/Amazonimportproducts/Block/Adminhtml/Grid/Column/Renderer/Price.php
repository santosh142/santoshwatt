<?php

class WP_Amazonimportproducts_Block_Adminhtml_Grid_Column_Renderer_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Currency
{
    public function render(Varien_Object $row)
    {
        $currencyFrom       = $row->getData('currency_code');
        $currencyTo         = $this->_getCurrencyCode($row);
        $price              = $row->getData($this->getColumn()->getIndex());
        $priceAddPercent    = $row->getData('price_add_percent');
        $priceAddUnit       = $row->getData('price_add_unit');
        $priceNewAddC       = $row->getData('price_new_add_c');
        $formatPrice        = WP_Amazonimportproducts_Model_Amazonoffer::convertPrice($price, $currencyFrom, $currencyTo, true);
        $convPrice          = WP_Amazonimportproducts_Model_Amazonoffer::convertPrice($price, $currencyFrom, $currencyTo, false);
        $finalPrice         = WP_Amazonimportproducts_Model_Amazonoffer::finalPrice($convPrice, $priceAddPercent, $priceAddUnit, $priceNewAddC);
        $formatFinalPrice   = WP_Amazonimportproducts_Model_Amazonoffer::formatPrice($finalPrice, $currencyTo);
        return $formatPrice . '<br /><span class="final_price">[ ' . $formatFinalPrice . ' ]</span>';
    }
}
