<?php

class WP_Amazonimportproducts_Block_Adminhtml_Import_Edit_Tab_Form
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('amazonimportproducts_form', array(
            'legend'=>Mage::helper('amazonimportproducts')->__('General'))
        );

        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'title',
        ));

        $statuses = Mage::getModel('amazonimportproducts/source_status')->toOptionArray();
        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Import Status'),
            'name'      => 'status',
            'required'  => true,
            'class'     => 'required-entry',
            'values'    => $statuses,
        ));

        $fieldset->addField('sku', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('SKU'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'sku',
        ));

        $statuses = Mage::getSingleton('amazonimportproducts/source_stock')->getOptionArray();
        $fieldset->addField('is_in_stock', 'select', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Stock'),
            'name'      => 'is_in_stock',
            'required'  => true,
            'class'     => 'required-entry',
            'values'    => $statuses,
        ));

        $fieldset->addField('quantity', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Quantity'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'quantity',
        ));

        $fieldset->addField('currency_code', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Currency'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'currency_code',
        ));

        $fieldset->addField('offer_id', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Merchant ID'),
            'required'  => false,
            'name'      => 'offer_id',
        ));

        $fieldset->addField('price', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Price'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'price',
        ));

        $fieldset->addField('price_add_percent', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Price Adjustment, pA (%)'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'price_add_percent',
        ));

        $fieldset->addField('price_add_unit', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Price Adjustment, pB'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'price_add_unit',
        ));

        $fieldset->addField('price_new_add_c', 'text', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Price Adjustment, pC (%)'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'price_new_add_c',
        ));

        $config['document_base_url']        = $this->getData('store_media_url');
        $config['store_id']                 = $this->getData('store_id');
        $config['add_variables']            = false;
        $config['add_widgets']              = false;
        $config['add_directives']           = true;
        $config['use_container']            = true;
        $config['container_class']          = 'hor-scroll';
        $config['directives_url']           = Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive');
        $config['files_browser_window_url'] = Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index');

        $fieldset->addField('description', 'editor', array(
          'name'      => 'description',
          'label'     => Mage::helper('amazonimportproducts')->__('Description'),
          'title'     => Mage::helper('amazonimportproducts')->__('Description'),
          'style'     => 'width:700px; height:500px;',
          'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig($config),
          'wysiwyg'   => true,
          'required'  => false,
        ));

        if (Mage::getSingleton('adminhtml/session')->getAmazonimportproductsData()) {
            $data = Mage::getSingleton('adminhtml/session')->getAmazonimportproductsData();
            Mage::getSingleton('adminhtml/session')->setAmazonimportproductsData(null);
        } elseif ( Mage::registry('amazonimportproducts_data') ) {
            $data = Mage::registry('amazonimportproducts_data')->getData();
            $form->setValues($data);
        }

        $item = unserialize($data['data_serialized']);

        $fieldset->addField('images', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Images'),
            'required'  => false,
            'name'      => 'image',
            'text'      => self::_getImagesHTML(Mage::getModel('amazonimportproducts/observer')->getImages($item)),
            'tabindex'  => '0'
        ));

        $link = WP_Amazonimportproducts_Model_Amazonoffer::getProductOfferUrl($data['amazon_product_url'], $data['offer_id']);
        $fieldset->addField('amazon_product_url', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Amazon Product URL'),
            'required'  => false,
            'name'      => 'amazon_product_url',
            'text'      => '<a href="' . $link . '" target="_blank">' . $this->__('To link') . '</a>',
            'tabindex'  => '0'
        ), 'title');

        $fieldset->addField('categories', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Categories'),
            'required'  => false,
            'name'      => 'categories',
            'text'      => self::_getCategoriesHTML(Mage::getModel('amazonimportproducts/observer')->getProductCategories($item)),
            'tabindex'  => '0'
        ), 'price_new_add_c');

        $fieldset->addField('converted_price', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Converted Price'),
            'required'  => false,
            'name'      => 'converted_price',
            'text'      => self::_getConvertedPrice($data['price'], $data['currency_code']),
            'tabindex'  => '0'
        ), 'price_new_add_c');

        $fieldset->addField('final_price', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Final Price'),
            'required'  => false,
            'name'      => 'final_price',
            'text'      => self::_getFinalPrice($data['price'], $data['currency_code'], $data['price_add_percent'], $data['price_add_unit'], $data['price_new_add_c']),
            'tabindex'  => '0'
        ), 'converted_price');

        $fieldset->addField('customer_reviews', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Amazon Customer Reviews'),
            'required'  => false,
            'name'      => 'customer_reviews',
            'text'      => self::_getCustomerReviewsUrl($item),
            'tabindex'  => '0'
        ), 'amazon_product_url');

        $fieldset->addField('offers_list', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Amazon Offers List'),
            'required'  => false,
            'name'      => 'offers_list',
            'text'      => self::_getOffersListUrl($item),
            'tabindex'  => '0'
        ), 'final_price');

        $fieldset->addField('asin', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('ASIN'),
            'required'  => false,
            'name'      => 'asin',
            'text'      => $item->ASIN,
            'tabindex'  => '0'
        ), 'sku');

        $fieldset->addField('ean', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('EAN (ISBN-13)'),
            'required'  => false,
            'name'      => 'ean',
            'text'      => isset($item->ItemAttributes->EAN) ? $item->ItemAttributes->EAN : Mage::helper('amazonimportproducts')->__('No'),
            'tabindex'  => '0'
        ), 'asin');

        $fieldset->addField('isbn', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('ISBN (ISBN-10)'),
            'required'  => false,
            'name'      => 'isbn',
            'text'      => isset($item->ItemAttributes->ISBN) ? $item->ItemAttributes->ISBN : Mage::helper('amazonimportproducts')->__('No'),
            'tabindex'  => '0'
        ), 'ean');

        $priceTypes = Mage::getSingleton('amazonimportproducts/source_price')->toOptionArray();
        $fieldset->addField('price_type', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Price Group'),
            'required'  => false,
            'name'      => 'price_type',
            'tabindex'  => '0',
            'text'      => $priceTypes[$data['price_type']],
        ), 'currency_code');

        $conditions = Mage::getSingleton('amazonimportproducts/source_condition')->toOptionArray();
        $fieldset->addField('condition', 'note', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Condition Group'),
            'required'  => false,
            'name'      => 'condition',
            'tabindex'  => '0',
            'text'      => $conditions[$data['condition']],
        ), 'price_type');

        return parent::_prepareForm();
    }

    private static function _getCustomerReviewsUrl($item)
    {
        if (isset($item->CustomerReviews->IFrameURL)) {
            return '<a href="' . $item->CustomerReviews->IFrameURL . '" target="_blank">' . Mage::helper('amazonimportproducts')->__('To link') . '</a>';
        }
        return Mage::helper('amazonimportproducts')->__('No');
    }

    private static function _getOffersListUrl($item)
    {
        if (isset($item->Offers->MoreOffersUrl) && $item->Offers->MoreOffersUrl) {
            return '<a href="' . $item->Offers->MoreOffersUrl . '" target="_blank">' . Mage::helper('amazonimportproducts')->__('To link') . '</a>';
        }
        return Mage::helper('amazonimportproducts')->__('No');
    }

    private static function _getImagesHTML($images)
    {
        if (!count($images)) return Mage::helper('amazonimportproducts')->__('No images');
        $html = '<div class="amazon_gallery">';
        foreach ($images as $image) {
            $html.= '<div class="amazon_gallery_image"><img src="' . $image['url'] . '" width="' . $image['width'] . '" height="' . $image['height'] . '"/></div>';
        }
        $html.= '</div>';
        return $html;
    }

    private static function _getCategoriesHTML($categories)
    {
        if (!count($categories)) return Mage::helper('amazonimportproducts')->__('No categories');
        $html = '';
        foreach ($categories as $catLine) {
            $line = array();
            $line = self::_getCatLine($catLine, $line);
            $line = array_reverse($line);
            $html.= '<div>' . implode(' > ', $line) . '</div>';
        }
        return $html;
    }

    private static function _getCatLine(&$catLine, &$line)
    {
        $line[] = $catLine['name'];
        if (isset($catLine['parent'])) self::_getCatLine($catLine['parent'], $line);
        return $line;
    }

    private static function _getConvertedPrice($price, $currencyFrom)
    {
        $currencyTo     = Mage::app()->getStore(0)->getBaseCurrency()->getCode();
        $formatPrice    = WP_Amazonimportproducts_Model_Amazonoffer::convertPrice($price, $currencyFrom, $currencyTo, true);
        return $formatPrice;
    }

    private static function _getFinalPrice($price, $currencyFrom, $priceAddPercent, $priceAddUnit, $priceNewAddC)
    {
        $currencyTo         = Mage::app()->getStore(0)->getBaseCurrency()->getCode();
        $convPrice          = WP_Amazonimportproducts_Model_Amazonoffer::convertPrice($price, $currencyFrom, $currencyTo, false);
        $finalPrice         = WP_Amazonimportproducts_Model_Amazonoffer::finalPrice($convPrice, $priceAddPercent, $priceAddUnit, $priceNewAddC);
        $formatFinalPrice   = WP_Amazonimportproducts_Model_Amazonoffer::formatPrice($finalPrice, $currencyTo);
        return $formatFinalPrice;
    }
}
