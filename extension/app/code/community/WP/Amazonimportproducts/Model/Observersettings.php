<?php

class WP_Amazonimportproducts_Model_Observersettings
{
    public function addCustomLayoutHandle(Varien_Event_Observer $observer)
    {
        $controllerAction   = $observer->getEvent()->getAction();
        $layout             = $observer->getEvent()->getLayout();
        if ($controllerAction && $layout && $controllerAction instanceof Mage_Adminhtml_System_ConfigController) {
            if ($controllerAction->getRequest()->getParam('section') == 'amazon_import_products') {
                $layout->getUpdate()->addHandle('wp_amazon_import_products_settings');
            }
        }
        return $this;
    }
}
