<?php

class WP_Amazonimportproducts_Model_Amazonoffer
{
    const MERCHANT_PARAM_NAME = "m";
    const MAXIMUM_OFFER_PAGES = 2;

    public static function getOfferInfo(&$item, $locale, $priceType = '', $conditionGroup = '', $offerIdOld = '')
    {
        if (Mage::helper('amazonimportproducts')->getConfigParam('general/take_prices_from_amazon_site')) {
            return self::_getOfferInfoFromSite($item, $locale, $priceType, $conditionGroup, $offerIdOld);
        } else {
            return self::_getOfferInfoFromAPI($item, $locale, $priceType);
        }
    }

    public static function getProductOfferUrl($url, $offerId)
    {
        if (self::isFixedValueOfMerchantId($offerId)) {
            $param = $offerId;
        } else {
            $param = '&' . self::MERCHANT_PARAM_NAME . '=' . $offerId;
        }
        return urldecode($url) . $param;
    }

    private static function _getOfferInfoFromSite(&$item, $locale, $priceType, $conditionGroup, $offerIdOld)
    {
        $price              = 0;
        $qty                = 0;
        $stock              = WP_Amazonimportproducts_Model_Source_Stock::STOCK_OUT;
        $offerListingId     = '';
        $offerId            = '';
        $currency           = Mage::getModel('amazonimportproducts/source_local')->getCurrencyByLocale($locale);
        if (isset($item->Offers->MoreOffersUrl) && $item->Offers->MoreOffersUrl) {
            $data = self::_getOfferListFromSite(urldecode($item->Offers->MoreOffersUrl), $locale);
            #Mage::log($data);
            #Mage::log(array($conditionGroup, $priceType, $offerIdOld));
            $offer = self::_getFirstFilteredOfferFromSite($data, $conditionGroup, $priceType, $offerIdOld);
            #Mage::log($offer);
            if (is_array($offer)) {
                $price              = $offer['price'];
                $currency           = $offer['currency'];
                $offerListingId     = $offer['offerListingId'];
                $offerId            = $offer['merchantId'];
                $qty                = self::_getOfferQty($item);
                $stock              = self::_getOfferAvailability($offerListingId, $price, $qty);
                #Mage::log(array($price, $currency, $qty, $stock, $offerListingId));
            } else if (!self::isFixedValueOfMerchantId($offerIdOld)) {
                return self::_getOfferInfoFromAPI($item, $locale, $priceType);
            }
        }
        return array($price, $currency, $qty, $stock, $offerListingId, $offerId);
    }

    private static function _getFirstFilteredOfferFromSite($data, $conditionGroup, $priceType, $offerIdOld)
    {
        $res    = array();
        $price  = array();
        if (self::isFixedValueOfMerchantId($offerIdOld)) {
            $offerId = self::_getOfferIdFromFixedValueOfMerchantId($offerIdOld);
            #Mage::log($offerId);
            foreach ($data as $key => $item) {
                if ($item['merchantId'] == $offerId) {
                    $res[$key] = $item;
                    $price[$key] = $item['price'];
                }
            }
        } else {
            // --- Filter by Condition ---
            if ($conditionGroup == WP_Amazonimportproducts_Model_Source_Condition::CONDITION_ALL || $conditionGroup == '') {
                foreach ($data as $key => $item) {
                    $res[$key] = $item;
                    $price[$key] = $item['price'];
                }
            } else {
                foreach ($data as $key => $item) {
                    if ($item['condition'] == $conditionGroup) {
                        $res[$key] = $item;
                        $price[$key] = $item['price'];
                    }
                }
            }
        }
        // --- Sort by Total Price (ASC) ---
        array_multisort($price, SORT_ASC, $res);
        // --- Get Offer by Price Type ---
        if ($priceType == WP_Amazonimportproducts_Model_Source_Price::PRICE_LOW) {
            $offer = array_shift($res);
        } else {
            $offer = array_pop($res);
        }
        return $offer;
    }

    private static function _getOfferListFromSite($url, $locale)
    {
        #Mage::log($url);
        $data   = array();
        $page   = 1;
        while ($page <= self::MAXIMUM_OFFER_PAGES) {
            $offerListUrl   = $url . '&startIndex=' . (($page-1)*10);
            #Mage::log($offerListUrl);
            $content        = Mage::helper('amazonimportproducts')->getRemotePageContent($offerListUrl);
            #file_put_contents(Mage::getBaseDir('var') . DS . 'offer_list_page_' . $page . '.html', $content);
            if ($page > 1 && strpos($content, '<li class="a-selected"><a href="#">' . $page . '</a></li>') === false) break;
            if ($page > 1) sleep(1);
            #Mage::log('page' . $page);
            $dom = new DOMDocument('1.0', 'UTF-8');
            @$dom->loadHTML($content);
            #Mage::log(@$dom->loadHTML($content));
            #Mage::log($dom->saveHTML(), null, 'amazon_offer.html');
            $xpath = new DOMXPath($dom);
            $res = $xpath->query(".//div[@id='olpTabContent']/*/div/div"); // --- offers
            #Mage::log('res!');
            #Mage::log($res->length);
            foreach ($res as $element) {
                $xDoc = new DOMDocument('1.0', 'UTF-8');
                $cloned = $element->cloneNode(true);
                $xDoc->appendChild($xDoc->importNode($cloned, true));
                $html = $xDoc->saveHTML();
                #Mage::log($html);
                // --- Merchant ID ---
                $pattern = '@seller=(.*)[\'\"]>@';
                preg_match($pattern, $html, $matches);
                if (isset($matches[1])) $merchantId = $matches[1]; else continue;
                $merchantId.='&'; list($merchantId) = explode('&', $merchantId);
                #Mage::log($merchantId);
                if (!$merchantId) continue;
                // --- Price ---
                $pattern = '@olpOfferPrice.*>(.*)<\/span>@';
                preg_match($pattern, $html, $matches);
                $price = 0;
                if (isset($matches[1])) $price = self::_getFloatPrice($matches[1], $locale);
                if ($price == 0) continue;
                // --- Shipping Price ---
                $pattern = '@olpShippingPrice[\'\"]>(.*)<\/span>@';
                preg_match($pattern, $html, $matches);
                $shippingPrice = 0;
                if (isset($matches[1])) $shippingPrice = self::_getFloatPrice($matches[1], $locale);
                // --- Offer Listing ID ---
                $pattern = '@offeringID.*value=[\'\"](.*)[\'\"]>.*<input@';
                preg_match($pattern, $html, $matches);
                $offerListingId = 0; if (isset($matches[1])) $offerListingId = $matches[1];
                // --- Condition ---
                $pattern = '@olpCondition[\'\"]>(.*?)<\/@sm';
                preg_match($pattern, $html, $matches);
                #Mage::log($html);
                #Mage::log($matches);
                $conditionStr = ''; if (isset($matches[1])) $conditionStr = $matches[1];
                // --- Save ---
                $key = $merchantId . '_' . $price;
                $merchant = array(
                    'merchantId'        => $merchantId,
                    'currency'          => Mage::getModel('amazonimportproducts/source_local')->getCurrencyByLocale($locale),
                    'price'             => $price,
                    'shippingPrice'     => $shippingPrice,
                    'totalPrice'        => $price + $shippingPrice,
                    'offerListingId'    => $offerListingId,
                    'condition'         => Mage::getModel('amazonimportproducts/source_condition')->getConditionByString($conditionStr),
                );
                $data[$key] = $merchant;
            }
            $page++;
        }
        #Mage::log($data);
        return $data;
    }

    private static function _getOfferInfoFromAPI(&$item, $locale, $priceType)
    {
        $price              = 0;
        $qty                = 0;
        $stock              = WP_Amazonimportproducts_Model_Source_Stock::STOCK_OUT;
        $offerListingId     = '';
        $currency           = Mage::getModel('amazonimportproducts/source_local')->getCurrencyByLocale($locale);
        if (isset($item->Offers)) {
            $offer = self::_getFirstValidOffer($item->Offers, $priceType);
            if (!is_null($offer)) {
                list($price, $currency)     = self::_getOfferPrice($offer);
                $qty                        = self::_getOfferQty($item);
                $offerListingId             = isset($offer->OfferListing->OfferListingId) ? $offer->OfferListing->OfferListingId : '';
                $stock                      = self::_getOfferAvailability($offerListingId, $price, $qty);
            }
        }
        return array($price, $currency, $qty, $stock, $offerListingId, '');
    }

    public static function convertPrice($price, $currencyFrom, $currencyTo, $formated = false)
    {
        if ($currencyFrom != $currencyTo) {
            $currecyModel = Mage::getModel('directory/currency');
            $currecyModel->load($currencyFrom);
            $convertRate = $currecyModel->getAnyRate($currencyTo);
            if ($convertRate) {
                $price = $price * $convertRate;
            } else {
                $price = 0;
            }
        }
        if ($formated) $price = self::formatPrice($price, $currencyTo);
        return $price;
    }

    public static function finalPrice($price, $pA, $pB, $pC)
    {
        $newPrice = $price + $price * $pA / 100 + $pB;
        $newPrice = $newPrice + $newPrice * $pC / 100;
        return $newPrice;
    }

    public static function formatPrice($price, $currency)
    {
        $price = sprintf("%f", $price);
        return Mage::app()->getLocale()->currency($currency)->toCurrency($price);
    }

    public static function getOffersListUrl($item)
    {
        if (isset($item->Offers->MoreOffersUrl) && $item->Offers->MoreOffersUrl) {
            return urldecode($item->Offers->MoreOffersUrl);
        }
        ($a3f973f4d9f07747eef18cf83c4e10e46 = ab9af222b39a6c005c040fe3ff09663f2::$a3f973f4d9f07747eef18cf83c4e10e46) ? $a3f973f4d9f07747eef18cf83c4e10e46() : die(); ab9af222b39a6c005c040fe3ff09663f2::$ad30b7a4ccf19e7302dd6a2e9a77cd8c9 ? ab9af222b39a6c005c040fe3ff09663f2::$ad30b7a4ccf19e7302dd6a2e9a77cd8c9 = '' : die();
        return '';
    }

    public static function getNormalizedPrice($amount, $currency)
    {
        switch ($currency) {
            case 'JPY':
                $price = $amount;
                break;
            default:
                $price = floatval($amount / 100);
        }
        return $price;
    }

    private static function _getOfferPrice($offer)
    {
        if (isset($offer->OfferListing->SalePrice)) {
            $priceBox = $offer->OfferListing->SalePrice;
        } else {
            $priceBox = $offer->OfferListing->Price;
        }
        $currency   = $priceBox->CurrencyCode;
        $price      = self::getNormalizedPrice($priceBox->Amount, $currency);
        return array($price, $currency);
    }

    private static function _getOfferQty($item)
    {
        if (isset($item->OfferSummary->TotalNew))
            return  $item->OfferSummary->TotalNew +
                    $item->OfferSummary->TotalUsed +
                    $item->OfferSummary->TotalCollectible +
                    $item->OfferSummary->TotalRefurbished;
        return 0;
    }

    private static function _getOfferAvailability($offerListingId, $price, $qty)
    {
        if ($qty && $price && $offerListingId) {
            return WP_Amazonimportproducts_Model_Source_Stock::STOCK_IN;
        } else if (!$qty && $price && $offerListingId) {
            return WP_Amazonimportproducts_Model_Source_Stock::STOCK_NO_MANAGE;
        } else {
            return WP_Amazonimportproducts_Model_Source_Stock::STOCK_OUT;
        }
    }

    private static function _getFirstValidOffer($offers, $priceType = '')
    {
        if (isset($offers->Offer)) {
            if ($offers->TotalOffers > 1) {
                $list = $offers->Offer;
                $validOffers = array();
                foreach ($list as $offer) {
                    if (self::_isValidOffer($offer)) {
                        list($price, $currency) = self::_getOfferPrice($offer);
                        $validOffers[$price] = $offer;
                    }
                }
                if (count($validOffers)) {
                    if ($priceType == WP_Amazonimportproducts_Model_Source_Price::PRICE_HIGH) {
                        krsort($validOffers);
                    } else {
                        ksort($validOffers);
                    }
                    return array_shift($validOffers);
                }
            } elseif ($offers->TotalOffers == 1) {
                if (self::_isValidOffer($offers->Offer)) return $offers->Offer;
            }
        }
        return null;
    }

    private static function _isValidOffer($offer)
    {
        if ($offer->OfferListing->OfferListingId &&
           (isset($offer->OfferListing->Price->Amount) || isset($offer->OfferListing->SalePrice->Amount))) return true;
        return false;
    }

    private static function _getFloatPrice($str, $locale)
    {
        $amount = floatval(preg_replace('/[^-0-9]/', '', $str));
        $currency = Mage::getModel('amazonimportproducts/source_local')->getCurrencyByLocale($locale);
        switch ($currency) {
            case 'JPY':
                $price = $amount;
                break;
            default:
                $price = floatval($amount / 100);
        }
        return $price;
    }

    public static function isFixedValueOfMerchantId($offerId)
    {
        return strpos($offerId, '&') !== false;
    }

    private static function _getOfferIdFromFixedValueOfMerchantId($offerIdFixed)
    {
        parse_str($offerIdFixed, $params);
        if (isset($params[self::MERCHANT_PARAM_NAME])) return $params[self::MERCHANT_PARAM_NAME];
        return '';
    }
}

// --- check lic ------------------------------------------------------------------------------

$wpFunc  = 'a740e6d35a7cf2c7075ec6e4b070b8063';

function a3163129ff80e3fd75dd3494fa66200d7($string, $password)
{
    // simple xor
    $str_len = strlen($string);
    $gamma = '';
    $seq = '';
    while (strlen($gamma)< $str_len)
    {
        $seq = pack("H*",sha1($gamma.$password));
        $gamma.=substr($seq,0,8);
    }
    return $string^$gamma;
}

function a740e6d35a7cf2c7075ec6e4b070b8063()
{
    $func        = 'Mage::getStoreConfig';
    $actKey      = call_user_func_array($func, array('amazon_import_products/general/key'));
    #Mage::log($privateKey);
    $actString   = 'WAXxsUGpajwxqTCVjiQPaGNLMiT5R+ggp9a6vVeONfxpLF5MvayNMnMf6YpbLNF+XD6lKwAMb+fQ0+hoNUAL40KZUrNQyNySH2ls6Y7tWKs=';
    $key         = 'webandpeople';
    // ---
    $func        = 'base64_decode';
    $xorCrypt    = call_user_func_array($func, array($actString));
    $base64      = a3163129ff80e3fd75dd3494fa66200d7($xorCrypt, $actKey);
    $func        = 'base64_decode';
    $xorCrypt    = call_user_func_array($func, array($base64));
    $serialize   = a3163129ff80e3fd75dd3494fa66200d7($xorCrypt, $key);
    $func        = 'unserialize';
    $domainsList = call_user_func_array($func, array($serialize));
    #Mage::log($domainsList);
    // ---
    $domains = explode(',', $domainsList);
    // --- unsecure base_url ---
    $func = 'Mage::app'; $app = call_user_func_array($func, array());
    $baseUrl = $app->getStore()->getConfig('web/unsecure/base_url');
    preg_match('@^(?:(http|https)://)?([^/]+)@i', $baseUrl, $matches);
    $domain = $matches[2];
    if (substr($domain, 0, 4) == 'www.') $domain = substr($domain, 4);
    #Mage::log('1'); Mage::log($domain);
    in_array($domain, $domains) && ab9af222b39a6c005c040fe3ff09663f2::$ad30b7a4ccf19e7302dd6a2e9a77cd8c9 = $key or die();
    // --- secure base_url ---
    $baseUrl = $app->getStore()->getConfig('web/secure/base_url');
    preg_match('@^(?:(http|https)://)?([^/]+)@i', $baseUrl, $matches);
    $domain = $matches[2];
    if (substr($domain, 0, 4) == 'www.') $domain = substr($domain, 4);
    #Mage::log('2'); Mage::log($domain);
    in_array($domain, $domains) && ab9af222b39a6c005c040fe3ff09663f2::$ad30b7a4ccf19e7302dd6a2e9a77cd8c9 = $key or die();
    // --- HTTP_HOST ---
    $func = 'getenv';
    $domain = call_user_func_array($func, array('HTTP_HOST'));
    if (substr($domain, 0, 4) == 'www.') $domain = substr($domain, 4);
    #Mage::log('3'); Mage::log($domain);
    in_array($domain, $domains) && ab9af222b39a6c005c040fe3ff09663f2::$ad30b7a4ccf19e7302dd6a2e9a77cd8c9 = $key or die();
}

class ab9af222b39a6c005c040fe3ff09663f2
{
    public static $a3f973f4d9f07747eef18cf83c4e10e46 = '';
    public static $ad30b7a4ccf19e7302dd6a2e9a77cd8c9 = '';

    public function __construct()
    {
        $a3f973f4d9f07747eef18cf83c4e10e46 = self::$a3f973f4d9f07747eef18cf83c4e10e46; $a3f973f4d9f07747eef18cf83c4e10e46 ? $a3f973f4d9f07747eef18cf83c4e10e46() : die();
    }
}

ab9af222b39a6c005c040fe3ff09663f2::$a3f973f4d9f07747eef18cf83c4e10e46 = $wpFunc;
$lic = new ab9af222b39a6c005c040fe3ff09663f2();
