<?php

class WP_Amazonimportproducts_Model_Source_Price extends Varien_Object
{
    const PRICE_LOW     = 'low';
    const PRICE_HIGH    = 'high';

    static public function toOptionArray()
    {
        return array(
            self::PRICE_LOW     => Mage::helper('amazonimportproducts')->__('Lowest available'),
            self::PRICE_HIGH    => Mage::helper('amazonimportproducts')->__('Highest available'),
        );
    }
}
