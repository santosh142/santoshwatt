<?php

class WP_Amazonimportproducts_Model_Source_Ratingcategories extends Varien_Object
{
    static public function toOptionArray()
    {
        $options = array();
        $collection = Mage::getModel('rating/rating')
            ->getResourceCollection()
            ->addEntityFilter(Mage_Review_Model_Review::ENTITY_PRODUCT_CODE);

        foreach($collection as $rating) {
            #Mage::log($rating->getData());
            $options[$rating->getRatingId()] = Mage::helper('amazonimportproducts')->__($rating->getRatingCode());
        }
        return $options;
    }
}
