<?php

class WP_Amazonimportproducts_Model_Source_Condition extends Varien_Object
{
    const CONDITION_ALL         = 'All';
    const CONDITION_NEW         = 'New';
    const CONDITION_USED        = 'Used';
    const CONDITION_COLLECTIBLE = 'Collectible';
    const CONDITION_REFURBISHED = 'Refurbished';

    public static function toOptionArray()
    {
        return array(
            self::CONDITION_ALL         => Mage::helper('amazonimportproducts')->__('All'),
            self::CONDITION_NEW         => Mage::helper('amazonimportproducts')->__('New'),
            self::CONDITION_USED        => Mage::helper('amazonimportproducts')->__('Used'),
            self::CONDITION_COLLECTIBLE => Mage::helper('amazonimportproducts')->__('Collectible'),
            self::CONDITION_REFURBISHED => Mage::helper('amazonimportproducts')->__('Refurbished'),
        );
    }

    public static function getConditionByString($str)
    {
        $str = preg_replace('/\s+/', ' ', $str);
        list($str) = explode('-', $str);
        $str = trim($str);
        $conditionList = array(
            /* US */
            'New' => self::CONDITION_NEW,
            'Used' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* UK */
            'New' => self::CONDITION_NEW,
            'Used' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* DE */
            'Neu' => self::CONDITION_NEW,
            'Gebraucht' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* JP */
            '新品' => self::CONDITION_NEW,
            '中古品' => self::CONDITION_USED,
            'コレクター商品' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* FR */
            'Neuf' => self::CONDITION_NEW,
            'D\'occasion' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* CA */
            'New' => self::CONDITION_NEW,
            'Used' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* IT */
            'Nuovo' => self::CONDITION_NEW,
            'Usato' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* CN */
            '全新品' => self::CONDITION_NEW,
            '二手商品' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
            /* ES */
            'Nuevo' => self::CONDITION_NEW,
            'De 2ª mano' => self::CONDITION_USED,
            'Collectible' => self::CONDITION_COLLECTIBLE,
            'Refurbished' => self::CONDITION_REFURBISHED,
        );
        if (isset($conditionList[$str])) return $conditionList[$str];
        return '';
    }
}
