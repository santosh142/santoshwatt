<?php

class WP_Amazonimportproducts_Model_Source_Currency extends Varien_Object
{
    static public function getOptionArray()
    {
        $tableName  = Mage::getSingleton('core/resource')->getTableName('amazonimportproducts');
        $read       = Mage::getSingleton('core/resource')->getConnection('core_read');
        $query      = "SELECT DISTINCT currency_code FROM {$tableName}";
        $collection = $read->fetchCol($query);
        $currency   = array();
        foreach ($collection as $item) {
            $currency[$item] = $item;
        }
        return $currency;
    }
}
