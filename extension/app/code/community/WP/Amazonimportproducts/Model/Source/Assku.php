<?php

class WP_Amazonimportproducts_Model_Source_Assku extends Varien_Object
{
    const SKU_ASIN          = 1;
    const SKU_ISBN_ASIN     = 2;
    const SKU_EAN_ISBN_ASIN = 3;
    const SKU_SKU_ASIN      = 4;

    static public function toOptionArray()
    {
        return array(
            self::SKU_ASIN          => Mage::helper('amazonimportproducts')->__('ASIN'),
            self::SKU_SKU_ASIN      => Mage::helper('amazonimportproducts')->__('SKU, ASIN'),
            self::SKU_ISBN_ASIN     => Mage::helper('amazonimportproducts')->__('ISBN (ISBN-10), ASIN'),
            self::SKU_EAN_ISBN_ASIN => Mage::helper('amazonimportproducts')->__('EAN (ISBN-13), ISBN (ISBN-10), ASIN'),
        );
    }
}
