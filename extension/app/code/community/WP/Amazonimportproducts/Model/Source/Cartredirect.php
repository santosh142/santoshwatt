<?php

class WP_Amazonimportproducts_Model_Source_Cartredirect extends Varien_Object
{
    const CART_REDIRECT_DEFAULT             = 0;
    const CART_REDIRECT_AMAZON_ADD_TO_CART  = 1;
    const CART_REDIRECT_AMAZON_PRODUCT_PAGE = 2;
    const CART_REDIRECT_AMAZON_OFFERS_LIST  = 3;

    static public function toOptionArray()
    {
        return array(
            self::CART_REDIRECT_DEFAULT             => Mage::helper('amazonimportproducts')->__('Disable'),
            self::CART_REDIRECT_AMAZON_ADD_TO_CART  => Mage::helper('amazonimportproducts')->__('Amazon Cart'),
            self::CART_REDIRECT_AMAZON_PRODUCT_PAGE => Mage::helper('amazonimportproducts')->__('Amazon Product Page'),
            self::CART_REDIRECT_AMAZON_OFFERS_LIST  => Mage::helper('amazonimportproducts')->__('Amazon Offers List'),
        );
    }
}
