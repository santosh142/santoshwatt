<?php

class WP_Amazonimportproducts_Model_Mysql4_Amazonimportproducts extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the amazonimportproducts_id refers to the key field in your database table.
        $this->_init('amazonimportproducts/amazonimportproducts', 'amazonimportproducts_id');
    }
}
