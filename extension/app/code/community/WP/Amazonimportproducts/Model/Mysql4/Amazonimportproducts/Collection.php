<?php

class WP_Amazonimportproducts_Model_Mysql4_Amazonimportproducts_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('amazonimportproducts/amazonimportproducts');
    }
}