<?php

require_once Mage::getModuleDir('', 'WP_Amazonimportproducts') . DS . 'Model' . DS . 'lib' . DS . 'AmazonECS.class.php';

class ExceptionAmazonAPI extends Exception { }

class AmazonAPI extends AmazonECS
{
    private $_convertCodeLocations = array("us" => "com", "uk" => "co.uk", "jp" => "co.jp");

    public function __construct($accessKey, $secretKey, $country = "", $associateTag = "")
    {
        // --- Check SOAP ---
        if (!class_exists('SoapClient', false)) {
            throw new ExceptionAmazonAPI('SOAP extension not found. Maybe it is not enabled.');
        }
        // --- Check OpenSSL ---
        if (!function_exists('openssl_pkey_get_public')) {
            throw new ExceptionAmazonAPI('OpenSSL extension not found. Maybe it is not enabled.');
        }
        // --- Check Params ---
        if (empty($accessKey) || empty($secretKey)) {
            throw new ExceptionAmazonAPI('No Access Key or Secret Key has been set.');
        }
        parent::__construct($accessKey, $secretKey, $country, $associateTag);
    }

    public function country($country = null)
    {
        if (!$country) return $this;

        // --- Check Associate Tag ---
        $configAssociateTag = Mage::helper('amazonimportproducts')->getConfigParam('associate_tag');
        $tagKey             = strtolower('amazon_' . $country);
        $associateTag       = isset($configAssociateTag[$tagKey]) ? $configAssociateTag[$tagKey] : '';

        Mage::helper('amazonimportproducts')->log(array(
            'country'       => $country,
            'associateTag'  => $associateTag
        ), 'Amazon API', 'country');

        if (!$associateTag) {
            $countries = Mage::getModel('amazonimportproducts/source_local')->getOptionArray();
            $message =  Mage::helper('amazonimportproducts')->__('Required Associate Tag for %s', $countries[$country]);
            throw new ExceptionAmazonAPI($message);
        }

        $this->associateTag($associateTag);

        // --- codes for US, JP, UK ---
        if (isset($this->_convertCodeLocations[$country])) {
            $country = $this->_convertCodeLocations[$country];
        }

        return parent::country($country);
    }

    public function search($pattern, $nodeId = null)
    {
        $response = parent::search($pattern, $nodeId);
        $this->_checkErrors($response);
        return $response;
    }

    public function lookup($asin)
    {
        $response = parent::lookup($asin);
        $this->_checkErrors($response);
        return $response;
    }

    private function _checkErrors($response)
    {
        if (isset($response->Items->Request->IsValid) && $response->Items->Request->IsValid != 'True') {
            if (isset($response->Items->Request->Errors->Error)) {
                $errors = $response->Items->Request->Errors->Error;
                if (!is_array($errors)) $errors = array($errors);
                $messages = array();
                foreach ($errors as $error) {
                    $messages[] = $error->Message;
                }
                $messages = str_replace("','", "', '", $messages);
                if (count($messages)) {
                    throw new ExceptionAmazonAPI(implode(' ', $messages));
                }
            }
        }
    }

    public function cartAdd($itemsData, $cartIds = null)
    {
        $listItems = array();
        foreach ($itemsData as $offerListingId => $qty) {
            $listItems['Item'][] = array(
                'OfferListingId'    => $offerListingId,
                'Quantity'          => $qty,
            );
        }
        $data = array('Items' => $listItems);
        if (is_null($cartIds)) {
            $operation = 'CartCreate';
        } else {
            $operation = 'CartAdd';
            $data = array_merge($cartIds, $data);
        }
        $params = $this->buildRequestParams($operation, $data);
        return $this->returnData(
            $this->performSoapRequest($operation, $params)
        );
    }

    public function cartModify($itemsData, $cartIds)
    {
        $listItems = array();
        foreach ($itemsData as $cartItemId => $qty) {
            $listItems['Item'][] = array(
                'CartItemId'    => $cartItemId,
                'Quantity'      => $qty,
            );
        }
        $data       = array('Items' => $listItems);
        $data       = array_merge($cartIds, $data);
        $operation  = 'CartModify';
        $params     = $this->buildRequestParams($operation, $data);
        return $this->returnData(
            $this->performSoapRequest($operation, $params)
        );
    }

    public function cartClear($cartIds)
    {
        $operation  = 'CartClear';
        $params     = $this->buildRequestParams($operation, $cartIds);
        return $this->returnData(
            $this->performSoapRequest($operation, $params)
        );
    }

    public function cartState($cartIds)
    {
        $operation  = 'CartGet';
        $params     = $this->buildRequestParams($operation, $cartIds);
        return $this->returnData(
            $this->performSoapRequest($operation, $params)
        );
    }
}
