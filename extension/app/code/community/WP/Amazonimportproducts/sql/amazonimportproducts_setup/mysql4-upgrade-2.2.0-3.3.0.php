<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

// ----------------------
// --- add new fields ---
// ----------------------

$newFields = array(
    'wp_amazon_asin_parent' => array(
        'label'          => 'Parent ASIN',
        'unique'         => true,
        'position'       => 16,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_id' => array(
        'label'          => 'Merchant ID',
        'unique'         => false,
        'position'       => 51,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
);
foreach($newFields as $attributeName => $attributeDefs)
{
    $setup->addAttribute('catalog_product', $attributeName, array(
        'group'             => 'Amazon Import Products',
        'position'          => $attributeDefs['position'],
        'type'              => $attributeDefs['type'],
        'label'             => $attributeDefs['label'],
        'backend'           => $attributeDefs['backend'],
        'unique'            => $attributeDefs['unique'],
        'input'             => $attributeDefs['input'],
        'default'           => $attributeDefs['default'],
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'class'             => '',
        'source'            => '',
        'default'           => '',
        'apply_to'          => '',
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'searchable'        => true,
        'filterable'        => true,
        'comparable'        => true,
        'visible_on_front'  => true,
        'is_configurable'   => false,
    ));
}

// -----------------------------
// --- reinstall temp tables ---
// -----------------------------

$installer->run("

DROP TABLE IF EXISTS {$installer->getTable('amazonimportproducts')};
CREATE TABLE {$installer->getTable('amazonimportproducts')} (
    `amazonimportproducts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `parent_asin` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
    `sku` varchar(255) NOT NULL,
    `condition` varchar(15) NOT NULL DEFAULT 'All',
    `categories` text NOT NULL,
    `title` text NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` text NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `price_add_percent` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_add_unit` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_type` varchar(5) NOT NULL DEFAULT 'low',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_id` varchar(100) NOT NULL DEFAULT '',
    `offer_listing_id` text NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportproducts_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$installer->getTable('amazonimportbooks')};
CREATE TABLE {$installer->getTable('amazonimportbooks')} (
    `amazonimportbooks_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `sku` varchar(255) NOT NULL,
    `condition` varchar(15) NOT NULL DEFAULT 'All',
    `categories` text NOT NULL,
    `title` text NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` text NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `price_add_percent` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_add_unit` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_type` varchar(5) NOT NULL DEFAULT 'low',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_id` varchar(100) NOT NULL DEFAULT '',
    `offer_listing_id` text NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportbooks_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();
