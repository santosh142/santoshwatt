<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$installer->getTable('amazonimportproducts')}
    ADD `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `checked` ,
    ADD INDEX ( `updated` );

ALTER TABLE {$installer->getTable('amazonimportbooks')}
    ADD `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `checked` ,
    ADD INDEX ( `updated` );

    ");

$installer->endSetup();
