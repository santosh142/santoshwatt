<?php

require_once('app/Mage.php');
Mage::app();

try {

    $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

    $oldFields = array(
        'amazonimportproducts_local'                => 'wp_amazon_local',
        'amazonimportproducts_asin'                 => 'wp_amazon_asin',
        'amazonimportproducts_ean'                  => 'wp_amazon_ean',
        'amazonimportproducts_isbn'                 => 'wp_amazon_isbn',
        'amazonimportproducts_offer_condition'      => 'wp_amazon_offer_condition',
        'amazonimportproducts_offer_price_type'     => 'wp_amazon_offer_price_type',
        'amazonimportproducts_offer_price'          => 'wp_amazon_offer_price',
        'amazonimportproducts_offer_price_currency' => 'wp_amazon_offer_currency',
        'amazonimportproducts_offer_seller_id'      => 'wp_amazon_offer_seller_id',
        'amazonimportproducts_offer_listing_id'     => 'wp_amazon_offer_listing_id',
        'amazonimportproducts_offer_exchange_id'    => 'wp_amazon_offer_exchange_id',
        'amazonimportproducts_product_url'          => 'wp_amazon_product_url',
        'amazonimportproducts_reviews_url'          => 'wp_amazon_reviews_url',
        'amazonimportproducts_sync'                 => 'wp_amazon_sync',
        'amazonimportproducts_use_categories'       => 'wp_amazon_use_categories',
        'amazonimportproducts_last_update'          => 'wp_amazon_last_update',
        'amazonimportproducts_sync_last_touch'      => 'wp_amazon_sync_last_touch',
        'wp_amazon_price_add_percent'               => 'wp_amazon_price_add_percent',
        'wp_amazon_price_add_unit'                  => 'wp_amazon_price_add_unit',
        'wp_amazon_offers_list_url'                 => 'wp_amazon_offers_list_url',
        'wp_amazon_asin_parent'                     => 'wp_amazon_asin_parent',
        'wp_amazon_offer_id'                        => 'wp_amazon_offer_id',
        'wp_amazon_price_new_add_c'                 => 'wp_amazon_price_new_add_c',
    );

    // -------------------------
    // --- delete old fields ---
    // -------------------------
    foreach ($oldFields as $oldAttributeName => $newAttributeName)
    {
        $setup->removeAttribute('catalog_product', $oldAttributeName);
        $setup->removeAttribute('catalog_product', $newAttributeName);
    }

    // ----------------------------
    // --- delete attribute set ---
    // ----------------------------

    $setup->removeAttributeGroup('catalog_product', 'Default', 'Amazon Import Products');

    // --------------------------------------------------
    // --- delete tables and records of the extension ---
    // --------------------------------------------------

    $db = Mage::getSingleton('core/resource')->getConnection('core_write');
    $table_prefix = Mage::getConfig()->getTablePrefix();
    $db->query("DELETE FROM `{$table_prefix}core_resource` WHERE `code` = 'amazonimportproducts_setup' LIMIT 1");
    $db->query("DELETE FROM `{$table_prefix}core_config_data` WHERE `path` LIKE  'amazon_import_products/%'");
    $db->query("DROP TABLE IF EXISTS `{$table_prefix}amazonimportbooks`");
    $db->query("DROP TABLE IF EXISTS `{$table_prefix}amazonimportproducts`");

    // -----------------------------
    // --- delete files and dirs ---
    // -----------------------------

    function deleteDirectories($path)
    {
        if (!is_dir($path)) return false;
        $dir = opendir($path);
        while (($file = readdir($dir)) !== false)
        {
            if ($file != '.' && $file != '..')
            {
                $item = $path . '/' . $file;
                if (is_dir($item))
                    deleteDirectories($item);
                else if (file_exists($item))
                    unlink($item);
            }
        }
        closedir($dir);
        rmdir($path);
    }

    $list = array(
        'app/code/local/WP/Amazonimportproducts',
        'app/code/community/WP/Amazonimportproducts',
        'app/design/adminhtml/default/WP/layout/amazonimportproducts.xml',
        'app/design/adminhtml/default/WP/template/amazonimportproducts',
        'app/design/adminhtml/default/default/layout/amazonimportproducts.xml',
        'app/design/adminhtml/default/default/template/amazonimportproducts',
        'app/etc/modules/WP_Amazonimportproducts.xml',
        'app/locale/en_US/WP_Amazonimportproducts.csv',
        'app/locale/ru_RU/WP_Amazonimportproducts.csv',
        'skin/adminhtml/default/WP/amazonimportproducts',
        'skin/adminhtml/default/default/amazonimportproducts',
    );

    $rootDir = dirname(__FILE__);

    foreach ($list as $item)
    {
        $del = $rootDir . '/' . $item;
        if (is_dir($del)) deleteDirectories($del);
            else if (file_exists($item))
                unlink($del);
    }

    // ---------------
    // --- finish! ---
    // ---------------
    $msg = 'Magento Amazon Products Manager has been removed successfully!';
} catch (Exception $e) {
    $msg = 'Failure!';
}

print_r($msg);
