<?php
error_reporting(E_ALL | E_STRICT);
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;
ini_set('memory_limit', '512M');
ini_set('max_execution_time', 1000);
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
Mage::app('admin');
$size = 500;
$page = 1;
function getAttrIdByCode($code)
{
	$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
	$attr_id = $eavAttribute->getIdByCode('catalog_product', $code);
	return $attr_id;
}
$attrid = getAttrIdByCode('wattusage');
$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');	
$table1 = $resource->getTableName('catalog_product_entity_varchar');
$query="SELECT * FROM `{$table1}` WHERE attribute_id='{$attrid}';";
//$products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('wattusage');
$products = $writeConnection->fetchAll($query);
if(!isset($_GET['p']) || isset($_GET['pass'])){
 $page = isset($_GET['p'])?$_GET['p']:1;
//Mage::getModel('catalog/product')->getCollection();
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'wattusagecp', array(
    'type'       => 'decimal',
	'group'		=> 'General',
    'input'      => 'text',
    'label'      => 'Energy Use',
    'sort_order' => 1000,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'used_for_sort_by' => 1

));
?>
Progress : <span id="progress">started.</span>

<script>
	var $page =<?php echo $page ?>;
	var $size=<?php echo $size ?>;
	
	var xhttp = new XMLHttpRequest();
	var url= '<?php echo Mage::getUrl('',array('_secure'=>true)) ?>';
	function copyData(url,page){
	  xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			if(xhttp.responseText=='done'){
				
				document.getElementById('progress').innerHTML+= '.'/*(page*$size*100)/$total*/;
				copyData(url,page+1);
			}
			else
			if(xhttp.responseText=='completed')
			{
				document.getElementById('progress').innerHTML='completed';;
			}
			else
			{
				console.log(xhttp.responseText);
				copyData(url,page);
			}
		}
	  };
	  var d = new Date();
	  xhttp.open("GET",url+'?p='+page+'&time='+d.getTime(), true);
	  xhttp.send();
	}
	copyData(url,$page);
</script>
<?php
}
if(isset($_GET['p'])){
$count = 0;
$i = 0;
$page = $_GET['p'];

foreach($products as $product)
{
	if($count++<(($page-1)*$size))
		continue;
	if($i++<$size){
		//$product->setWattusagecp($product->getWattusage())->save();
		$wattusage = $product['value'];
		$attr_id = getAttrIdByCode('wattusagecp');
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');	
		$table = $resource->getTableName('catalog_product_entity_decimal');
		if($wattusage){
			$query="UPDATE `{$table}` SET value='{$wattusage}' WHERE attribute_id='{$attr_id}' AND entity_id={$product['entity_id']};" ;
			$writeConnection->query($query);
			
		}
		
	}
	else
	{
		//fclose($h);
		echo 'done';
		die;
	}
	
}  

	
	echo 'completed'; 
	die;
}	
?>