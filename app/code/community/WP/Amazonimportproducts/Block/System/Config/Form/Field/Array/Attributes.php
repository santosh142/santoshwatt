<?php

class WP_Amazonimportproducts_Block_System_Config_Form_Field_Array_Attributes
    extends WP_Amazonimportproducts_Block_System_Config_Form_Field_Array_Abstract
{
    public function _prepareToRender()
    {
        $this->setTemplate('amazonimportproducts/system/config/form/field/array.phtml');
        $this->addColumn('magento_attribute', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Magento Attribute'),
            'style'     => 'width:220px',
            'class'     => 'select wp_init',
            'renderer'  => Mage::getBlockSingleton('amazonimportproducts/html_magentoattributes'),
        ));
        $this->addColumn('amazon_api_attribute', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Expression with Amazon API Attribute'),
            'style'     => 'width:330px',
        ));
        $this->addColumn('exclude_from_sync', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Excl. from Sync'),
            'style'     => 'width:90px',
            'class'     => 'select wp_init',
            'renderer'  => Mage::getBlockSingleton('amazonimportproducts/html_yesno'),
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('amazonimportproducts')->__('Add Attribute Mapping');
    }
}
