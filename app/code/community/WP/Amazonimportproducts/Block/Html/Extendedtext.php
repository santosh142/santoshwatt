<?php

class WP_Amazonimportproducts_Block_Html_Extendedtext
    extends Varien_Data_Form_Element_Text
{
    public function getLabelHtml($idSuffix = '')
    {
        if (!is_null($this->getLabel())) {
            $html = '<input onclick="Form.Element.select(this)" id="label_'.$this->getHtmlId().'" name="label_'.$this->getName()
             .'" value="' . htmlentities($this->getLabel()) . '" readonly="readonly" type="text" class="label-text input-text" />'."\n";
        } else {
            $html = '';
        }
        return $html;
    }
}
