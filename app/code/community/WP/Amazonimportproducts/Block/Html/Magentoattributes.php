<?php

class WP_Amazonimportproducts_Block_Html_Magentoattributes
    extends WP_Amazonimportproducts_Block_Html_Select
{
    public function getOptionArray()
    {
        $options = array();
        /** @var $attributeCollection Mage_Catalog_Model_Resource_Product_Attribute_Collection */
        $attributeCollection = Mage::getResourceModel('catalog/product_attribute_collection')
            ->addFieldToFilter('frontend_input', array('nin' => array('media_image', 'gallery')))
            ->addFieldToFilter('attribute_code', array('nlike' => 'wp_amazon_%'))
            ->addVisibleFilter();
        foreach ($attributeCollection as $attribute) {
            $options[] = array(
                'value' => $attribute->getAttributecode(),
                'label' => addslashes($attribute->getFrontendLabel())
            );
        }
        return $options;
    }
}
