<?php

class WP_Amazonimportproducts_Block_Html_Yesno
    extends WP_Amazonimportproducts_Block_Html_Select
{
    public function getOptionArray()
    {
        $options = array(
            array(
                'value' => 0,
                'label' => 'No'
            ),
            array(
                'value' => 1,
                'label' => 'Yes'
            )
        );
        return $options;
    }
}
