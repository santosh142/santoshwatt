<?php

class WP_Amazonimportproducts_Block_Adminhtml_Categories extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Categories
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amazonimportproducts/categories.phtml');
    }
}
