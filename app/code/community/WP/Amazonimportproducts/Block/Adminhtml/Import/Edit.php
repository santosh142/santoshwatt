<?php

class WP_Amazonimportproducts_Block_Adminhtml_Import_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'amazonimportproducts';
        $this->_controller = 'adminhtml_import';

        $this->_updateButton('save', 'label', Mage::helper('amazonimportproducts')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('amazonimportproducts')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('amazonimportproducts_description') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'amazonimportproducts_description');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'amazonimportproducts_description');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('amazonimportproducts_data') && Mage::registry('amazonimportproducts_data')->getId() )
        {
            return Mage::helper('amazonimportproducts')->__("Edit Amazon Item '%s'", $this->htmlEscape(Mage::registry('amazonimportproducts_data')->getTitle()));
        }
        return '';
    }
}
