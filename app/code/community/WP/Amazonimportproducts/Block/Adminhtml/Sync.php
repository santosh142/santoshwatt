<?php

class WP_Amazonimportproducts_Block_Adminhtml_Sync extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_sync';
        $this->_blockGroup = 'amazonimportproducts';
        $this->_headerText = Mage::helper('amazonimportproducts')->__('WebAndPeople: Synchronization with Amazon');
        parent::__construct();
        $this->_removeButton('add');

        $this->_addButton('add', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Synchronize information manually'),
            'onclick'   => 'SyncProductsList(); return false;',
            'class'     => 'save',
        ), 0, 1);

        $this->_addButton('reset', array(
            'label'     => Mage::helper('amazonimportproducts')->__('Reset parking status for all products'),
            'onclick'   => 'ResetProductsParking(); return false;',
            'class'     => 'delete',
        ), 0, 2);
    }

    protected function _prepareLayout()
    {
        $this->setChild('grid', $this->getLayout()->createBlock('amazonimportproducts/adminhtml_sync_grid'));
        return parent::_prepareLayout();
    }
}
