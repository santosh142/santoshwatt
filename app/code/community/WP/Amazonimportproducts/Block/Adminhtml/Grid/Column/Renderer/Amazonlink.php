<?php

class WP_Amazonimportproducts_Block_Adminhtml_Grid_Column_Renderer_Amazonlink
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $rowData = $row->getData();
        $link = WP_Amazonimportproducts_Model_Amazonoffer::getProductOfferUrl($rowData['amazon_product_url'], $rowData['offer_id']);
        $formatLink = '<a title="' . Mage::helper('amazonimportproducts')->__('Go to the product page on Amazon website') . '" target="_blank" href="' . $link . '">' . Mage::helper('amazonimportproducts')->__('To link') . '</a>';
        return $formatLink;
    }
}
