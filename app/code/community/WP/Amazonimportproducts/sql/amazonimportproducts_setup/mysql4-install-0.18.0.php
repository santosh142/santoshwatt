<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

// --- Adding attribute group ---
$setup->addAttributeGroup('catalog_product', 'Default', 'Amazon Import Products', 1000);

// --- The attribute added will be displayed under the group/tab Amazon Import Products in product edit page ---
$newFields = array(
    'amazonimportproducts_local' => array(
        'label'          => 'Amazon Local',
        'unique'         => false,
        'position'       => 10,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_asin' => array(
        'label'          => 'Amazon Standard Item Number',
        'unique'         => true,
        'position'       => 20,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_ean' => array(
        'label'          => 'European Article Number',
        'unique'         => true,
        'position'       => 21,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_isbn' => array(
        'label'          => 'International Standard Book Number',
        'unique'         => true,
        'position'       => 22,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_offer_price' => array(
        'label'          => 'Price of Seller on Amazon',
        'unique'         => false,
        'position'       => 30,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_offer_price_currency' => array(
        'label'          => 'Currency of price of Seller on Amazon',
        'unique'         => false,
        'position'       => 30,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_offer_seller_id' => array(
        'label'          => 'Unique identifier of seller on Amazon',
        'unique'         => false,
        'position'       => 30,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_offer_listing_id' => array(
        'label'          => 'Uniquely identifies an item sold by a seller or merchant on Amazon',
        'unique'         => false,
        'position'       => 40,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_offer_exchange_id' => array(
        'label'          => 'Uniquely identifies an item sold by a seller or merchant on Amazon (additional)',
        'unique'         => false,
        'position'       => 50,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_product_url' => array(
        'label'          => 'Amazon Product URL',
        'unique'         => false,
        'position'       => 60,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_reviews_url' => array(
        'label'          => 'Amazon Customer Reviews URL',
        'unique'         => false,
        'position'       => 70,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'amazonimportproducts_sync' => array(
        'label'          => 'Sync with Amazon',
        'unique'         => false,
        'position'       => 80,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'int',
        'backend'        => '',
    ),
    'amazonimportproducts_use_categories' => array(
        'label'          => 'Use categories from Amazon',
        'unique'         => false,
        'position'       => 90,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'int',
        'backend'        => '',
    ),
    'amazonimportproducts_last_update' => array(
        'label'          => 'Amazon Last Update',
        'unique'         => false,
        'position'       => 100,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'datetime',
        'backend'        => '',
    ),
    'amazonimportproducts_sync_last_touch' => array(
        'label'          => 'Last Touch for Amazon Synchronize',
        'unique'         => false,
        'position'       => 110,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'datetime',
        'backend'        => '',
    )
);
foreach($newFields as $attributeName => $attributeDefs)
{
    $setup->addAttribute('catalog_product', $attributeName, array(
        'group'             => 'Amazon Import Products',
        'position'          => $attributeDefs['position'],
        'type'              => $attributeDefs['type'],
        'label'             => $attributeDefs['label'],
        'backend'           => $attributeDefs['backend'],
        'unique'            => $attributeDefs['unique'],
        'input'             => $attributeDefs['input'],
        'default'           => $attributeDefs['default'],
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'class'             => '',
        'source'            => '',
        'default'           => '',
        'apply_to'          => '',
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'searchable'        => true,
        'filterable'        => true,
        'comparable'        => true,
        'visible_on_front'  => true,
        'is_configurable'   => false,
    ));
}

$installer->run("

DROP TABLE IF EXISTS {$installer->getTable('amazonimportproducts')};
CREATE TABLE {$installer->getTable('amazonimportproducts')} (
    `amazonimportproducts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `sku` varchar(255) NOT NULL,
    `categories` varchar(255) NOT NULL,
    `title` varchar(255) NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` varchar(255) NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_seller_id` varchar(255) NOT NULL,
    `offer_listing_id` varchar(255) NOT NULL,
    `offer_exchange_id` varchar(255) NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportproducts_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$installer->getTable('amazonimportbooks')};
CREATE TABLE {$installer->getTable('amazonimportbooks')} (
    `amazonimportbooks_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `sku` varchar(255) NOT NULL,
    `categories` varchar(255) NOT NULL,
    `title` varchar(255) NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` varchar(255) NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_seller_id` varchar(255) NOT NULL,
    `offer_listing_id` varchar(255) NOT NULL,
    `offer_exchange_id` varchar(255) NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportbooks_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();
