<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

// --- Adding attribute group ---
$setup->addAttributeGroup('catalog_product', 'Default', 'Amazon Import Products', 1000);

// ----------------------
// --- add new fields ---
// ----------------------

// --- The attribute added will be displayed under the group/tab Amazon Import Products in product edit page ---
$newFields = array(
    'wp_amazon_local' => array(
        'label'          => 'Amazon Local',
        'unique'         => false,
        'position'       => 10,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_asin' => array(
        'label'          => 'Amazon Standard Item Number',
        'unique'         => true,
        'position'       => 20,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_ean' => array(
        'label'          => 'European Article Number',
        'unique'         => true,
        'position'       => 21,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_isbn' => array(
        'label'          => 'International Standard Book Number',
        'unique'         => true,
        'position'       => 22,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_condition' => array(
        'label'          => 'Condition Group',
        'unique'         => false,
        'position'       => 28,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_price_type' => array(
        'label'          => 'Price Group',
        'unique'         => false,
        'position'       => 29,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_price' => array(
        'label'          => 'Price of Seller on Amazon',
        'unique'         => false,
        'position'       => 30,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_currency' => array(
        'label'          => 'Currency of price of Seller on Amazon',
        'unique'         => false,
        'position'       => 31,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_seller_id' => array(
        'label'          => 'Unique identifier of seller on Amazon',
        'unique'         => false,
        'position'       => 32,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_listing_id' => array(
        'label'          => 'Uniquely identifies an item sold by a seller or merchant on Amazon',
        'unique'         => false,
        'position'       => 40,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offer_exchange_id' => array(
        'label'          => 'Uniquely identifies an item sold by a seller or merchant on Amazon (additional)',
        'unique'         => false,
        'position'       => 50,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_product_url' => array(
        'label'          => 'Amazon Product URL',
        'unique'         => false,
        'position'       => 60,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_reviews_url' => array(
        'label'          => 'Amazon Customer Reviews URL',
        'unique'         => false,
        'position'       => 70,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_sync' => array(
        'label'          => 'Sync with Amazon',
        'unique'         => false,
        'position'       => 80,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'int',
        'backend'        => '',
    ),
    'wp_amazon_use_categories' => array(
        'label'          => 'Use categories from Amazon',
        'unique'         => false,
        'position'       => 90,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'int',
        'backend'        => '',
    ),
    'wp_amazon_last_update' => array(
        'label'          => 'Amazon Last Update',
        'unique'         => false,
        'position'       => 100,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'datetime',
        'backend'        => '',
    ),
    'wp_amazon_sync_last_touch' => array(
        'label'          => 'Last Touch for Amazon Synchronize',
        'unique'         => false,
        'position'       => 110,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'datetime',
        'backend'        => '',
    )
);

foreach($newFields as $attributeName => $attributeDefs)
{
    $setup->addAttribute('catalog_product', $attributeName, array(
        'group'             => 'Amazon Import Products',
        'position'          => $attributeDefs['position'],
        'type'              => $attributeDefs['type'],
        'label'             => $attributeDefs['label'],
        'backend'           => $attributeDefs['backend'],
        'unique'            => $attributeDefs['unique'],
        'input'             => $attributeDefs['input'],
        'default'           => $attributeDefs['default'],
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'class'             => '',
        'source'            => '',
        'default'           => '',
        'apply_to'          => '',
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'searchable'        => true,
        'filterable'        => true,
        'comparable'        => true,
        'visible_on_front'  => true,
        'is_configurable'   => false,
    ));
}

// ----------------------------------------
// --- copy value from old to new field ---
// ----------------------------------------

$oldFields = array(
    'amazonimportproducts_local'                => 'wp_amazon_local',
    'amazonimportproducts_asin'                 => 'wp_amazon_asin',
    'amazonimportproducts_ean'                  => 'wp_amazon_ean',
    'amazonimportproducts_isbn'                 => 'wp_amazon_isbn',
    'amazonimportproducts_offer_condition'      => 'wp_amazon_offer_condition',
    'amazonimportproducts_offer_price_type'     => 'wp_amazon_offer_price_type',
    'amazonimportproducts_offer_price'          => 'wp_amazon_offer_price',
    'amazonimportproducts_offer_price_currency' => 'wp_amazon_offer_currency',
    'amazonimportproducts_offer_seller_id'      => 'wp_amazon_offer_seller_id',
    'amazonimportproducts_offer_listing_id'     => 'wp_amazon_offer_listing_id',
    'amazonimportproducts_offer_exchange_id'    => 'wp_amazon_offer_exchange_id',
    'amazonimportproducts_product_url'          => 'wp_amazon_product_url',
    'amazonimportproducts_reviews_url'          => 'wp_amazon_reviews_url',
    'amazonimportproducts_sync'                 => 'wp_amazon_sync',
    'amazonimportproducts_use_categories'       => 'wp_amazon_use_categories',
    'amazonimportproducts_last_update'          => 'wp_amazon_last_update',
    'amazonimportproducts_sync_last_touch'      => 'wp_amazon_sync_last_touch',
);

$collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('amazonimportproducts_asin', array('neq' => ''));

foreach ($oldFields as $oldAttributeName => $newAttributeName)
{
    $collection->addAttributeToSelect($oldAttributeName);
}

$storeId = 0;
foreach ($collection as $productId => $info)
{
    $data = $info->getData();
    $attributeData = array();
    foreach ($oldFields as $oldAttributeName => $newAttributeName)
    {
        if (isset($data[$oldAttributeName]) && $data[$oldAttributeName])
            $attributeData[$newAttributeName] = $data[$oldAttributeName];
    }
    Mage::getSingleton('catalog/product_action')
        ->updateAttributes(array($productId), $attributeData, $storeId);
}

// -------------------------
// --- delete old fields ---
// -------------------------
foreach ($oldFields as $oldAttributeName => $newAttributeName)
{
    $setup->removeAttribute('catalog_product', $oldAttributeName);
}

$installer->endSetup();
