<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

// ----------------------
// --- add new fields ---
// ----------------------

$newFields = array(
    'wp_amazon_price_add_percent' => array(
        'label'          => 'Price Adjustment, pA (%)',
        'unique'         => false,
        'position'       => 80,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_price_add_unit' => array(
        'label'          => 'Price Adjustment, pB',
        'unique'         => false,
        'position'       => 45,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
    'wp_amazon_offers_list_url' => array(
        'label'          => 'Amazon Offers List URL',
        'unique'         => false,
        'position'       => 60,
        'input'          => 'text',
        'default'        => '',
        'type'           => 'text',
        'backend'        => '',
    ),
);
foreach($newFields as $attributeName => $attributeDefs)
{
    $setup->addAttribute('catalog_product', $attributeName, array(
        'group'             => 'Amazon Import Products',
        'position'          => $attributeDefs['position'],
        'type'              => $attributeDefs['type'],
        'label'             => $attributeDefs['label'],
        'backend'           => $attributeDefs['backend'],
        'unique'            => $attributeDefs['unique'],
        'input'             => $attributeDefs['input'],
        'default'           => $attributeDefs['default'],
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'class'             => '',
        'source'            => '',
        'default'           => '',
        'apply_to'          => '',
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'searchable'        => true,
        'filterable'        => true,
        'comparable'        => true,
        'visible_on_front'  => true,
        'is_configurable'   => false,
    ));
}

// -------------------------
// --- delete old fields ---
// -------------------------

$oldFields = array(
    'wp_amazon_offer_seller_id',
    'wp_amazon_offer_exchange_id',
);

foreach ($oldFields as $oldAttributeName)
{
    $setup->removeAttribute('catalog_product', $oldAttributeName);
}

// -----------------------------
// --- reinstall temp tables ---
// -----------------------------

$installer->run("

DROP TABLE IF EXISTS {$installer->getTable('amazonimportproducts')};
CREATE TABLE {$installer->getTable('amazonimportproducts')} (
    `amazonimportproducts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `sku` varchar(255) NOT NULL,
    `condition` varchar(15) NOT NULL DEFAULT 'All',
    `categories` varchar(255) NOT NULL,
    `title` varchar(255) NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` varchar(255) NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `price_add_percent` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_add_unit` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_type` varchar(5) NOT NULL DEFAULT 'low',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_listing_id` varchar(255) NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportproducts_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$installer->getTable('amazonimportbooks')};
CREATE TABLE {$installer->getTable('amazonimportbooks')} (
    `amazonimportbooks_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `sku` varchar(255) NOT NULL,
    `condition` varchar(15) NOT NULL DEFAULT 'All',
    `categories` varchar(255) NOT NULL,
    `title` varchar(255) NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` varchar(255) NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `price_add_percent` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_add_unit` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_type` varchar(5) NOT NULL DEFAULT 'low',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_listing_id` varchar(255) NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportbooks_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();
