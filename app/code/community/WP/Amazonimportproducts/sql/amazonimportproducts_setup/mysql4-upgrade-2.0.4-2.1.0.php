<?php

$installer = $this;

$installer->startSetup();

// -----------------------------
// --- reinstall temp tables ---
// -----------------------------

$installer->run("

DROP TABLE IF EXISTS {$installer->getTable('amazonimportproducts')};
CREATE TABLE {$installer->getTable('amazonimportproducts')} (
    `amazonimportproducts_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `sku` varchar(255) NOT NULL,
    `condition` varchar(15) NOT NULL DEFAULT 'All',
    `categories` text NOT NULL,
    `title` text NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` text NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `price_add_percent` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_add_unit` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_type` varchar(5) NOT NULL DEFAULT 'low',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_listing_id` text NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportproducts_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$installer->getTable('amazonimportbooks')};
CREATE TABLE {$installer->getTable('amazonimportbooks')} (
    `amazonimportbooks_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `amazon_local` varchar(2) NOT NULL,
    `asin` varchar(255) NOT NULL,
    `sku` varchar(255) NOT NULL,
    `condition` varchar(15) NOT NULL DEFAULT 'All',
    `categories` text NOT NULL,
    `title` text NOT NULL,
    `images_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
    `amazon_product_url` text NOT NULL,
    `price` decimal(12,4) unsigned NOT NULL DEFAULT '0.0000',
    `price_add_percent` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_add_unit` decimal(11,4) NOT NULL DEFAULT '0.0000',
    `price_type` varchar(5) NOT NULL DEFAULT 'low',
    `currency_code` varchar(3) NOT NULL,
    `quantity` int(10) unsigned NOT NULL DEFAULT '0',
    `is_in_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `offer_listing_id` text NOT NULL,
    `description` text NOT NULL,
    `data_serialized` text NOT NULL,
    `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `checked` tinyint(1) unsigned NOT NULL DEFAULT '0',
    `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (`amazonimportbooks_id`),
    UNIQUE KEY `asin` (`asin`),
    UNIQUE KEY `sku` (`sku`),
    KEY `checked` (`checked`),
    KEY `status` (`status`),
    KEY `updated` (`updated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();
