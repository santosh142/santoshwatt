<?php

class WP_Amazonimportproducts_Model_Source_Searchby extends Varien_Object
{
    const SEARCHBY_CATEGORY = 0;
    const SEARCHBY_ASIN     = 1;

    static public function getOptionArray()
    {
        return array(
            self::SEARCHBY_CATEGORY => Mage::helper('amazonimportproducts')->__('Category'),
            self::SEARCHBY_ASIN     => Mage::helper('amazonimportproducts')->__('ASIN'),
        );
    }
}
