<?php

class WP_Amazonimportproducts_Model_Source_Syncattributes extends Varien_Object
{
    const SYNC_SKU             = 'sku';
    const SYNC_NAME            = 'name';
    const SYNC_PRICE           = 'price';
    const SYNC_QTY             = 'qty';
    const SYNC_STOCK           = 'stock';
    const SYNC_DESCRIPTION     = 'description';
    const SYNC_IMAGES          = 'images';
    const SYNC_CATEGORIES      = 'categories';

    static public function toOptionArray()
    {
        $options[self::SYNC_SKU] = array(
            'value' => self::SYNC_SKU,
            'label' => Mage::helper('amazonimportproducts')->__('SKU')
        );
        $options[self::SYNC_NAME] = array(
            'value' => self::SYNC_NAME,
            'label' => Mage::helper('amazonimportproducts')->__('Name')
        );
        $options[self::SYNC_PRICE] = array(
            'value' => self::SYNC_PRICE,
            'label' => Mage::helper('amazonimportproducts')->__('Price')
        );
        $options[self::SYNC_QTY] = array(
            'value' => self::SYNC_QTY,
            'label' => Mage::helper('amazonimportproducts')->__('Qty')
        );
        $options[self::SYNC_STOCK] = array(
            'value' => self::SYNC_STOCK,
            'label' => Mage::helper('amazonimportproducts')->__('Inventory (Stock)')
        );
        $options[self::SYNC_DESCRIPTION] = array(
            'value' => self::SYNC_DESCRIPTION,
            'label' => Mage::helper('amazonimportproducts')->__('Description / Short Description')
        );
        $options[self::SYNC_IMAGES] = array(
            'value' => self::SYNC_IMAGES,
            'label' => Mage::helper('amazonimportproducts')->__('Images')
        );
        $options[self::SYNC_CATEGORIES] = array(
            'value' => self::SYNC_CATEGORIES,
            'label' => Mage::helper('amazonimportproducts')->__('Categories')
        );
        return $options;
    }
}
