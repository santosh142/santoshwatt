<?php

class WP_Amazonimportproducts_Model_Source_Status extends Varien_Object
{
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 2;

    static public function toOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('amazonimportproducts')->__('Included'),
            self::STATUS_DISABLED   => Mage::helper('amazonimportproducts')->__('Excluded'),
        );
    }
}
