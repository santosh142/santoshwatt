<?php

class WP_Amazonimportproducts_Model_Source_Stock extends Varien_Object
{
    const STOCK_OUT         = 0;
    const STOCK_IN          = 1;
    const STOCK_NO_MANAGE   = 2;

    static public function getOptionArray($isImport = true)
    {
        $opt[self::STOCK_OUT]               = Mage::helper('cataloginventory')->__('Out of Stock');
        $opt[self::STOCK_IN]                = Mage::helper('cataloginventory')->__('In Stock');
        if ($isImport) {
            $opt[self::STOCK_NO_MANAGE]     = Mage::helper('amazonimportproducts')->__('No Manage');
        }
        return $opt;
    }
}
