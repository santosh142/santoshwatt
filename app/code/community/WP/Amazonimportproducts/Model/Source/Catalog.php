<?php

class WP_Amazonimportproducts_Model_Source_Catalog extends Varien_Object
{
    static public function toOptionArray()
    {
        $catalogs = array();
        $collection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addFieldToFilter('level', 1)
            ->addFieldToFilter('parent_id', array('neq' => 0))
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('is_active');

        foreach($collection as $col) {
            $catalogs[$col->getPath()] = $col->getName();
        }
        return $catalogs;
    }
}
