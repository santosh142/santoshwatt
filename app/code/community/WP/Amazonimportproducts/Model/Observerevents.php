<?php

class WP_Amazonimportproducts_Model_Observerevents
{
    public function addEvent(Varien_Event_Observer $observer)
    {
        $fullActionName = $observer->getEvent()->getControllerAction()->getFullActionName();
        $data = array(
            'request' => $observer->getEvent()->getControllerAction()->getRequest(),
        );
        switch ($fullActionName) {
            case 'checkout_cart_add':
                Mage::dispatchEvent('amazonimportproducts_checkout_cart_product_add_before', $data);
                break;
            case 'checkout_onepage_index':
                Mage::dispatchEvent('amazonimportproducts_checkout_before', $data);
                break;
        }
        return $this;
    }

    public function doCheckMagentoCart(Varien_Event_Observer $observer)
    {
        Mage::getModel('amazonimportproducts/amazoncart')->checkMagentoCart();
        return $this;
    }

    public function doCheckoutBefore(Varien_Event_Observer $observer)
    {
        if (Mage::helper('amazonimportproducts')->getConfigParam('general/checkout_on_amazon')) {
            $url = Mage::getModel('amazonimportproducts/amazoncart')->sync(true)->getPurchaseURL();
            if ($url) {
                Mage::app()->getFrontController()->getResponse()->setRedirect($url);
                Mage::app()->getResponse()->sendResponse();
                exit;
            }
        }
        return $this;
    }

    public function doUpdateAmazonCart(Varien_Event_Observer $observer)
    {
        if (Mage::helper('amazonimportproducts')->getConfigParam('general/checkout_on_amazon')) {
            Mage::getModel('amazonimportproducts/amazoncart')->sync();
        }
        return $this;
    }

    public function doAddToCartBefore(Varien_Event_Observer $observer)
    {
        $request    = $this->_getProductRequest($observer->getEvent()->getRequest()->getParams());
        $product    = $this->_getProduct($observer->getEvent()->getRequest()->getParam('product', 0));
        if (is_null($product)) return $this;
        $this->_redirectToAmazon($request, $product);
        return $this;
    }

    private function _redirectToAmazon($request, $product)
    {
        if (Mage::helper('amazonimportproducts')->getConfigParam('general/checkout_on_amazon')) return;
        $url            = '';
        $redirectType   = Mage::helper('amazonimportproducts')->getConfigParam('general/amazon_cart_redirect');
        switch ($redirectType) {
            case WP_Amazonimportproducts_Model_Source_Cartredirect::CART_REDIRECT_AMAZON_ADD_TO_CART:
                $asin            = $product->getWpAmazonAsin();
                $offerListingId  = $product->getWpAmazonOfferListingId();
                if ($asin && $offerListingId) {
                    $local              = $product->getWpAmazonLocal();
                    $config             = Mage::helper('amazonimportproducts')->getConfigParam('general');
                    $configAssociateTag = Mage::helper('amazonimportproducts')->getConfigParam('associate_tag');
                    $tagKey             = 'amazon_' . $local;
                    $associateTag       = isset($configAssociateTag[$tagKey]) ? $configAssociateTag[$tagKey] : '';
                    $associateTag       = urlencode($associateTag);
                    $localUrl           = Mage::helper('amazonimportproducts')->getUrlByLocal($local);

                    $baseUrl  = 'http://' . $localUrl . '/gp/item-dispatch/' . $associateTag;
                    $params[] = 'tag-value=' . $associateTag;
                    $params[] = 'tag_value=' . $associateTag;
                    $params[] = 'tag=' . $associateTag;
                    $params[] = 'ASIN.1=' . $asin;
                    $params[] = 'offeringID.1=' . $offerListingId;
                    $params[] = 'quantity.1=' . $request->getQty();
                    $params[] = 'submit.addToCart=addToCart';
                    $url      = $baseUrl . '?' . implode('&', $params);
                }
                break;
            case WP_Amazonimportproducts_Model_Source_Cartredirect::CART_REDIRECT_AMAZON_PRODUCT_PAGE:
                $amazonProductUrl   = $product->getWpAmazonProductUrl();
                $amazonOfferId      = $product->getWpAmazonOfferId();
                if ($amazonProductUrl && $amazonOfferId) {
                    $url = WP_Amazonimportproducts_Model_Amazonoffer::getProductOfferUrl($amazonProductUrl, $amazonOfferId);
                }
                break;
            case WP_Amazonimportproducts_Model_Source_Cartredirect::CART_REDIRECT_AMAZON_OFFERS_LIST:
                $amazonOffersListUrl = $product->getWpAmazonOffersListUrl();
                if ($amazonOffersListUrl) {
                    $url = urldecode($amazonOffersListUrl);
                }
                break;
        }
        if ($url) {
            Mage::app()->getFrontController()->getResponse()->setRedirect($url);
            Mage::app()->getResponse()->sendResponse();
            exit;
        }
    }

    private function _getProductRequest($requestInfo)
    {
        $request = new Varien_Object($requestInfo);
        if (!$request->hasQty()) {
            $request->setQty(1);
        }
        return $request;
    }

    private function _getProduct($productInfo)
    {
        $product = Mage::getModel('catalog/product')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($productInfo);
        $currentWebsiteId = Mage::app()->getStore()->getWebsiteId();
        if (!$product
            || !$product->getId()
            || !is_array($product->getWebsiteIds())
            || !in_array($currentWebsiteId, $product->getWebsiteIds())
        ) {
            return null;
        }
        return $product;
    }
}
