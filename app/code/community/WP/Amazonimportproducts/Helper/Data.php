<?php

class WP_Amazonimportproducts_Helper_Data extends Mage_Core_Helper_Abstract
{
    const LOG_FILE              = "wp_amazon_import";
    const CACHE_REVIEWS_DIR     = "wp_amazonimportproducts_reviews";
    const TEMP_DIR              = "wp_amazonimportproducts_tmp";
    const USER_AGENT            = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.17 Safari/537.36";
    const ATTR_PLACEHOLDER      = "{{var attr=\"%s\"}}";

    private $_localUrls         = null;

    public function emptySettingsInfo()
    {
        $config = Mage::helper('amazonimportproducts')->getConfigParam('general');
        if (!$config['access_key_id'] || !$config['secret_access_key']) {
            return '<p class="amazon-attention">' . $this->__('Go to <a href="%s">Settings</a> and fill in the required fields: "Access Key ID" and "Secret Access Key"', Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit/section/amazon_import_products')) . '</p>';
        }
        return '';
    }

    public function convertedPriceInfo()
    {
        return $this->__('<span class="note">* Converted Price</span>. <b>If you see 0.00</b>, the currency conversion system probably isn\'t adjusted (System - <a href="%s">Manage Currency Rates</a>). In square brackets the final price is calculated by the formula:<br /><b>New Price</b> = Converted Price + Converted Price * pA / 100 + pB, <u><b>Final Price</b></u> = <b>New Price</b> + <b>New Price</b> * pC / 100.',
                            Mage::helper('adminhtml')->getUrl('adminhtml/system_currency/index'));
    }

    public function syncParamsInfo()
    {
        $config = Mage::helper('amazonimportproducts')->getConfigParam('sync_params');
        $attrList[] = $this->__('Amazon Product URL');
        $attrList[] = $this->__('Amazon Customer Reviews URL');
        $attrList[] = $this->__('Amazon Offers List URL');
        if ($config['updated_attributes']) {
            $additionalAttrKeys = explode(',', $config['updated_attributes']);
            $additionalAttr = Mage::getModel('amazonimportproducts/source_syncattributes')->toOptionArray();
            foreach ($additionalAttrKeys as $key) {
                $attrList[] = $additionalAttr[$key]['label'];
            }
        }
        return '<p class="amazon-notification">' . $this->__('Updated product attributes: <b>%s</b>. Parking time: <b>%d min</b>. Go to <a href="%s">Settings</a> to change it.', implode(', ', $attrList), $config['parking_time'], Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit/section/amazon_import_products')) . '</p>';
    }

    public function getAdvancedParameters($local, $rootCategory)
    {
        $advParams = Mage::getModel('amazonimportproducts/source_local')->getAdvSearchParams($local, $rootCategory);
        $fields = array();
        if (count($advParams)) {
            foreach ($advParams as $key => $field) {
                $fields[$key]['label'] = '<label for="' . $field['id'] . '">' . $this->__($field['title']) . '</label>';
                $fields[$key]['field'] = '<input type="text" class=" input-text" value="' . $field['value'] . '" name="' . $field['id'] . '" id="' . $field['id'] . '">';
            }
        }
        return $fields;
    }

    public function getUrlByLocal($local)
    {
        if (!$this->_localUrls) $this->_localUrls = Mage::getModel('amazonimportproducts/source_local')->getLocalUrlArray();
        if (isset($this->_localUrls[$local])) return $this->_localUrls[$local];
        return array_shift($this->_localUrls);
    }

    public function getConfigParam($key)
    {
        $key = 'amazon_import_products/' . $key;
        $data = Mage::getStoreConfig($key);
        if (!is_array($data)) return trim($data);
            else return array_map('trim', $data);
    }

    public function log($data, $info, $type = '', $isApiResponse = false)
    {
        if ($this->getConfigParam('debug/amazon_debug')) {
            $fileName = self::LOG_FILE . '.log';
            Mage::log('', null, $fileName); // --- blank line
            Mage::log('', null, $fileName);
            Mage::log('', null, $fileName);
            Mage::log('---------- START: ' . $info . ' ----------', null, $fileName);
            if ($type) Mage::log('---------- type:  ' . $type, null, $fileName);
            if ($isApiResponse) {
                $responseLogFile = self::LOG_FILE . '_response_' . date('Y-m-d_H-i-s') . '.log';
                Mage::log($data, null, $responseLogFile);
                $data = $responseLogFile;
            }
            Mage::log($data, null, $fileName);
            Mage::log('---------- END:   ' . $info . ' ----------', null, $fileName);
        }
    }

    public function getRemotePageContent($url, $timeoutGeneral = 20, $timeoutConnection = 10)
    {
        $cookieFile = Mage::getBaseDir('var') . DS . self::TEMP_DIR . DS . 'cookie.txt';
        if (file_exists($cookieFile)) unlink($cookieFile);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, self::USER_AGENT);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFile);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFile);
        #curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        if ((int)$timeoutGeneral) curl_setopt($ch, CURLOPT_TIMEOUT, (int)$timeoutGeneral);
        if ((int)$timeoutConnection) curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, (int)$timeoutConnection);
        $html = self::_curlRedirectExec($ch);
        return $html;
    }

    private static function _curlRedirectExec($ch)
    {
        static $redirects = 10;
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpCode == 301 || $httpCode == 302) {
            list($header) = explode("\r\n\r\n", $data, 2);
            $matches = array();
            preg_match('/(Location:|URI:)(.*?)\n/', $header, $matches);
            $url = trim(array_pop($matches));
            $urlParsed = parse_url($url);
            if (isset($urlParsed)) {
                curl_setopt($ch, CURLOPT_URL, $url);
                $redirects--;
                if ($redirects <= 0) return '';
                return self::_curlRedirectExec($ch);
            }
        }
        $res = explode("\r\n\r\n", $data, 2);
        if (isset($res[1])) $html = $res[1]; else $html = '';
        return $html;
    }

    public function saveReviewContent($locale, $asin, $url)
    {
        try {
            #Mage::log($url);
            $text = Mage::helper('amazonimportproducts')->getRemotePageContent($url);
            #throw new Exception ('test');
        }
        catch (Exception $e) {
            $text = '';
            Mage::helper('amazonimportproducts')->log($e->getMessage(), 'saveReviewContent - Get IFrame Content Error');
        }

        if (strpos($text, '</body>') !== false) {
            $script = <<<HTML
<script type="text/javascript">
//<![CDATA[
    try
    {
        parent.$('amazon_customer_reviews').style.width = '100%';
        parent.$('amazon_customer_reviews').style.display = 'block';
        parent.$('amazon_customer_reviews').style.height = getBodyHeight() + 'px';
    }
    catch(err)
    {
        // ---
    }

    function getBodyHeight()
    {
        var frameBody = document.body, frameHtml = document.documentElement, myHeight = 0;
        if (frameBody.scrollHeight > myHeight) myHeight = frameBody.scrollHeight;
        if (frameBody.offsetHeight > myHeight) myHeight = frameBody.offsetHeight;
        if (frameHtml.clientHeight > myHeight) myHeight = frameHtml.clientHeight;
        if (frameHtml.scrollHeight > myHeight) myHeight = frameHtml.scrollHeight;
        if (frameHtml.offsetHeight > myHeight) myHeight = frameHtml.offsetHeight;
        return myHeight;
    }
//]]>
</script>
HTML;
            $charset = Mage::getModel('amazonimportproducts/source_local')->getCharsetByLocale($locale);
            if ($charset) $text = iconv($charset, 'UTF-8', $text);
            $urlInfo = parse_url($url);
            $text = str_replace('</body>', $script . '</body>', $text);
            $text = str_replace('href="/', 'href="' . $urlInfo['scheme'] . '://' . $urlInfo['host'] . '/', $text);
            $text = str_replace('/reviews/iframe/uedata/', $urlInfo['scheme'] . '://' . $urlInfo['host'] . '/reviews/iframe/uedata/', $text);
            $text = str_replace('=\'/gp/uedata/', '=\'' . $urlInfo['scheme'] . '://' . $urlInfo['host'] . '/gp/uedata/', $text);
            $text = str_replace('=\'/gp/customer-reviews/', '=\'' . $urlInfo['scheme'] . '://' . $urlInfo['host'] . '/gp/customer-reviews/', $text);
            $text = str_replace(
                '<div style="padding-top: 10px; clear: both; width: 100%;">',
                '<hr size="1" style="border-top: 1px dashed #ACACAC;"/><div style="padding-top: 10px; clear: both; width: 100%; display:none;">',
                $text
            );
        } else {
            $text = '';
        }

        try {
            $cacheDir = Mage::getBaseDir('var') . DS . self::CACHE_REVIEWS_DIR;
            if (!file_exists($cacheDir)) mkdir($cacheDir, 0755, true);
            $filePath = $cacheDir . DS . $asin . '.txt';
            if ($text) {
                file_put_contents($filePath, $text);
            } else if (file_exists($filePath)) {
                unlink($filePath);
            }
        } catch (Exception $e) {
            Mage::helper('amazonimportproducts')->log($e->getMessage(), 'saveReviewContent - Save Review Error');
        }

        return $text;
    }

    public function loadReviewContent($asin)
    {
        $filePath = Mage::getBaseDir('var') . DS . self::CACHE_REVIEWS_DIR . DS . $asin . '.txt';
        if (file_exists($filePath)) {
            return file_get_contents($filePath);
        }
        return '';
    }

    public static function uploadImageByUrl($url)
    {
        Mage::helper('amazonimportproducts')->log($url, 'Amazon Product Images: Url', 'uploadImageByUrl');
        if ($url) {
            try {
                $valid_exts = array('jpg', 'jpeg', 'gif', 'png'); // default image only extensions
                $tmpArr = explode('.', strtolower(basename($url)));
                $ext = end($tmpArr);
                if (in_array($ext, $valid_exts)) {
                    $rand = rand(10000, 99999);
                    $importDir = Mage::getBaseDir('var') . DS . self::TEMP_DIR;
                    if (!file_exists($importDir)) mkdir($importDir, 0755, true);
                    $filename = $importDir . DS . $rand . basename($url);
                    Mage::helper('amazonimportproducts')->log($filename, 'Amazon Product Images: Save to Filename', 'uploadImageByUrl');

                    $ch = curl_init($url);
                    $fp = fopen($filename, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    #curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);

                    return $filename;
                }
            } catch (Exception $e) {
                Mage::helper('amazonimportproducts')->log($e->getMessage(), 'uploadImageByUrl Error');
            }
        }
        #{licCode}#
        return '';
    }

    public static function getAttributesMap($item)
    {
        $fields = array();
        if (isset($item->ItemAttributes)) {
            self::_buildAttributesMap($item->ItemAttributes, '', $fields);
        }
        return $fields;
    }

    private static function _buildAttributesMap($xmlObj, $path, &$result)
    {
        foreach ($xmlObj as $key => $value) {
            if (is_object($value)) {
                self::_buildAttributesMap($value, $path . $key . '/', $result);
            } else if (!is_array($value)){
                $result[] = array(
                    'name'          => $key,
                    'value'         => $value,
                    'path'          => $path . $key,
                    'placeholder'   => sprintf(self::ATTR_PLACEHOLDER, $path . $key),
                );
            }
        }
    }
}
