<?php
/**
 * SitemapPro extends Sitemap model
 *
 * @method Mage_Sitemap_Model_Resource_Sitemap _getResource()
 * @method Mage_Sitemap_Model_Resource_Sitemap getResource()
 * @method string getSitemapType()
 * @method Mage_Sitemap_Model_Sitemap setSitemapType(string $value)
 * @method string getSitemapFilename()
 * @method Mage_Sitemap_Model_Sitemap setSitemapFilename(string $value)
 * @method string getSitemapPath()
 * @method Mage_Sitemap_Model_Sitemap setSitemapPath(string $value)
 * @method string getSitemapTime()
 * @method Mage_Sitemap_Model_Sitemap setSitemapTime(string $value)
 * @method int getStoreId()
 * @method Mage_Sitemap_Model_Sitemap setStoreId(int $value)
 *
 * @category    Mage
 * @package     Mage_Sitemap
 * @author      Magento Core Team <core@magentocommerce.com>
 */
ini_set('memory_limit','4096M');
ini_set('display_errors', 1);
ini_set('max_execution_time','3600');
ini_set('max_input_time','3600'); 
class SitemapPro_IndexCreator_Model_Sitemap extends Mage_Sitemap_Model_Sitemap
{
    /**
     * Real file path
     *
     * @var string
     */
    protected $_filePath;

    /**
     * Init model
     */
    protected function _construct()
    {
        $this->_init('sitemap/sitemap');
    }

    protected function _beforeSave()
    {
        $io = new Varien_Io_File();
        $realPath = $io->getCleanPath(Mage::getBaseDir() . '/' . $this->getSitemapPath());

        /**
         * Check path is allow
         */
        if (!$io->allowedPath($realPath, Mage::getBaseDir())) {
            Mage::throwException(Mage::helper('sitemap')->__('Please define correct path'));
        }
        /**
         * Check exists and writeable path
         */
        if (!$io->fileExists($realPath, false)) {
            Mage::throwException(Mage::helper('sitemap')->__('Please create the specified folder "%s" before saving the sitemap.', Mage::helper('core')->htmlEscape($this->getSitemapPath())));
        }

        if (!$io->isWriteable($realPath)) {
            //Mage::throwException(Mage::helper('sitemap')->__('Please make sure that "%s" is writable by web-server.', $this->getSitemapPath()));
            mkdir($realPath,'0775');
        }
        /**
         * Check allow filename
         */
        if (!preg_match('#^[a-zA-Z0-9_\.]+$#', $this->getSitemapFilename())) {
            Mage::throwException(Mage::helper('sitemap')->__('Please use only letters (a-z or A-Z), numbers (0-9) or underscore (_) in the filename. No spaces or other characters are allowed.'));
        }
        if (!preg_match('#\.xml$#', $this->getSitemapFilename())) {
            $this->setSitemapFilename($this->getSitemapFilename() . '.xml');
        }

        //$this->setSitemapPath(rtrim(str_replace(str_replace('\\', '/', Mage::getBaseDir()), '', $realPath), '/') . '/');

        return parent::_beforeSave();
    }

    /**
     * Return real file path
     *
     * @return string
     */
    protected function getPath()
    {
        if (is_null($this->_filePath)) {
            $this->_filePath = str_replace('//', '/', Mage::getBaseDir() .
                $this->getSitemapPath());
        }
        if(!$this->getSitemapPath()){
        mkdir($this->getSitemapPath());
        }
        
        return $this->_filePath;
    }

    /**
     * Return full file name with path
     *
     * @return string
     */
    public function getPreparedFilename()
    {
        return $this->getPath() . $this->getSitemapFilename();
    }
    
    public function _getConnection($type = 'core_read'){
	$connect_cache[$type] = Mage::getSingleton('core/resource')->getConnection($type);
	
    	return $connect_cache[$type];
    }

    public function _getTableName($tableName){
	 $table_cache[$tableName] = Mage::getSingleton('core/resource')->getTableName($tableName);
	
    	return $table_cache[$tableName];
    }
	public function getBlogUrls(){

	    $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
	    $sql = "SELECT identifier
	                FROM aw_blog";

	    $connection->fetchAll($sql);
	  
	}    
     

    /**
     * Generate XML file
     *
     * @return Mage_Sitemap_Model_Sitemap
     */
    public function generateXml()
    {
        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true);
        if($io->open(array('path' => $this->getPath()))){
        
        }
        else{
        mkdir($this->getPath());
        }

        if ($io->fileExists($this->getSitemapFilename()) && !$io->isWriteable($this->getSitemapFilename())) {
            Mage::throwException(Mage::helper('sitemap')->__('File "%s" cannot be saved. Please, make sure the directory "%s" is writeable by web server.', $this->getSitemapFilename(), $this->getPath()));
        }
        
        /**
         * Generate blog sitemap
         */
     	$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
	$sql = "SELECT identifier FROM aw_blog";
	$collection = $connection->fetchAll($sql);
	if($collection){
	       $io->streamOpen(str_replace('.xml','',$this->getSitemapFilename())."_blog.xml");
	
	        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
	        $io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
	
	        $storeId = $this->getStoreId();
	
		$changefreq = (string)Mage::getStoreConfig('sitemap/category/changefreq', $storeId);
	        $priority   = (string)Mage::getStoreConfig('sitemap/category/priority', $storeId);
	         $date = date('c',time()-144000);
	 	
	        $baseUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK).'blog/';
	        foreach ($collection as $item) {
	
	            $xml = sprintf('<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>',
	                htmlspecialchars($baseUrl . $item["identifier"]),
	                $date,
	                $changefreq,
	                $priority
	            );
	            $io->streamWrite($xml);
	        }
	
	       
	 	$io->streamWrite('</urlset>');
	        $io->streamClose();
	        unset($collection);
        }
        $baseUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
            
       $io->streamOpen(str_replace('.xml','',$this->getSitemapFilename())."_cats.xml");

        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
        $io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

        $storeId = $this->getStoreId();
        /**
         * Generate categories sitemap
         */
        $changefreq = (string)Mage::getStoreConfig('sitemap/category/changefreq', $storeId);
        $priority   = (string)Mage::getStoreConfig('sitemap/category/priority', $storeId);
        $collection = Mage::getResourceModel('sitemap/catalog_category')->getCollection($storeId);
         $date = date('c',time()-144000);
     
        foreach ($collection as $item) {

            $xml = sprintf('<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>',
                htmlspecialchars($baseUrl . $item->getUrl()),
                $date,
                $changefreq,
                $priority
            );
            $io->streamWrite($xml);
        }

       
 	$io->streamWrite('</urlset>');
        $io->streamClose();
        unset($collection);
        
        
         /**
         * Generate products sitemap
         */
        $c=0;$totalCount=0; 
         $io->streamOpen(str_replace('.xml','',$this->getSitemapFilename()).$c.".xml");

        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
        $io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');       
         
        $changefreq = (string)Mage::getStoreConfig('sitemap/product/changefreq', $storeId);
        $priority   = (string)Mage::getStoreConfig('sitemap/product/priority', $storeId);
        //$prodCollection = Mage::getResourceModel('sitemap/catalog_product')->getCollection($storeId);
       
	//$count = count($prodCollection);
	//unset($prodCollection);
	
	//$c=1;
	$totalCount = 0;
	$collection = Mage::getResourceModel('catalog/product_collection')->addAttributeToSelect('entity_id')
        		->addAttributeToFilter('status', array('eq' => 1))
        		->addAttributeToFilter('visibility', array('neq' => 1));
	$count = $collection->getSize();
	
        while($c < ceil($count/49999)){
        	/*if($totalCount < $count-49999){
        		$collection = Mage::getResourceModel('catalog/product_collection')
        		->addAttributeToSelect('url_path')
        		->addAttributeToSelect('updated_at')
        		->addFieldToFilter('entity_id',array('from'=>$totalCount,'to'=>$totalCount+49999));
        		
        	}
        	else{
        		$collection = Mage::getResourceModel('catalog/product_collection')
        		->addAttributeToSelect('url_path')
        		->addAttributeToSelect('updated_at')
        		->addFieldToFilter('entity_id',array('from'=>$totalCount,'to'=>$count));
        		
        	}*/
        	$store_id = Mage::app()->getStore()->getId();
        	Mage::app()->setCurrentStore($store_id);
        	$collection = Mage::getResourceModel('catalog/product_collection')
        		->setStoreId($this->getStoreId())
        		->addStoreFilter()
        		->addAttributeToSelect('url_path')
        		->addAttributeToSelect('updated_at')
        		->addAttributeToFilter('status', array('eq' => 1))
        		->addAttributeToFilter('visibility', array('neq' => 1))
        		;
        		
        	$collection->getSelect()->limit(49999,$totalCount);

	        foreach ($collection as $item) {
	     	$updatedAt = strtotime($item->getData('updated_at'));
	        $date = date('c',$updatedAt-36000);
	            $xml =  '<url><loc>'.Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
	            .$item->getData('url_path')
	            .'</loc><lastmod>'.$date
	            .'</lastmod><changefreq>'
	            .$changefreq
	            .'</changefreq><priority>'
	            .$priority
	            .'</priority></url>';
	            
	            $io->streamWrite($xml);
	            $z++;
	            $totalCount++;
	      
	        }
	        $c++;
	        $io->streamWrite('</urlset>');
	        $io->streamClose();
	        if($c < ceil($count/49999)){
		        $io->streamOpen(str_replace('.xml','',$this->getSitemapFilename()).$c.".xml");
		
		        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
		        $io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
	        }
        }
        
        if($c < ceil($count/49999)){
        $io->streamWrite('</urlset>');
        $io->streamClose();
        }
        unset($collection);
        /**
         * Generate cms pages sitemap
         */
         $io->streamOpen(str_replace('.xml','',$this->getSitemapFilename())."_cms_pages.xml");

        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
        $io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');
        $changefreq = (string)Mage::getStoreConfig('sitemap/page/changefreq', $storeId);
        $priority   = (string)Mage::getStoreConfig('sitemap/page/priority', $storeId);
        $collection = Mage::getResourceModel('sitemap/cms_page')->getCollection($storeId);
        $date = date('c',time()-144000);

        foreach ($collection as $item) {

            $xml = sprintf('<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>',
                Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$item->getUrl(),
                $date,
                $changefreq,
                $priority
            );

            $io->streamWrite($xml);

            }

        unset($collection);

        $io->streamWrite('</urlset>');
        $io->streamClose();
        
        
         /**
         * Generate sitemap index file (sitemap.xml)
         */
         
      
         $io->streamOpen($this->getPreparedFilename());


        $io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
        $io->streamWrite('<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n");

        for($z=0;$z<$c;$z++) {

           
           $xml = '<sitemap>'."\n\t".'<loc>'.preg_replace('{/$}','',Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)).$this->getSitemapPath().str_replace('.xml','',$this->getSitemapFilename()).$z.'.xml'.'</loc>'."\n\t".
            '<lastmod>'.$date.'</lastmod>'."\n".'</sitemap>'."\n";
            $io->streamWrite($xml);            
            
        }
	    $xmlCats = '<sitemap>'."\n\t".'<loc>'.preg_replace('{/$}','',Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)).$this->getSitemapPath().str_replace('.xml','',$this->getSitemapFilename()).'_cats.xml'.'</loc>'."\n\t".
            '<lastmod>'.$date.'</lastmod>'."\n".'</sitemap>'."\n";
            $io->streamWrite($xmlCats);
	    
	    $xmlCMS = '<sitemap>'."\n\t".'<loc>'.str_replace('.xml','',preg_replace('{/$}','',Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)).$this->getSitemapPath().str_replace('.xml','',$this->getSitemapFilename())).'_cms_pages.xml'.'</loc>'."\n\t".
            '<lastmod>'.$date.'</lastmod>'."\n".'</sitemap>'."\n";
            $io->streamWrite($xmlCMS);
            
            if(file_exists($this->getSitemapFilename())."_blog.xml"){
	    	$xmlBlog = '<sitemap>'."\n\t".'<loc>'.str_replace('.xml','',preg_replace('{/$}','',Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)).$this->getSitemapPath().str_replace('.xml','',$this->getSitemapFilename())).'_blog.xml'.'</loc>'."\n\t".
	            '<lastmod>'.$date.'</lastmod>'."\n".'</sitemap>'."\n";
	        $io->streamWrite($xmlBlog);
            }
            
            
        unset($collection);

        $io->streamWrite('</sitemapindex>');
        $io->streamClose();
        
        
        

        $this->setSitemapTime(Mage::getSingleton('core/date')->gmtDate('Y-m-d H:i:s'));
        $this->save();

        return $this;
    }
    
}
