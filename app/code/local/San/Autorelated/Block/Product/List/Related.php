<?php
/**
 * Product related items block
 *
 * @category   San
 * @package    San_Autorelated
 * @author     Santosh
 */
class San_Autorelated_Block_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{

    protected $_itemCollection;
	
	/*Function gets collection of related products */
    protected function _prepareData()
    {
		
 
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */
		
		/* getting category of current product*/
        if (Mage::registry('current_category')) {
            $category = Mage::registry('current_category');
        } else {
            $catids = $product->getCategoryIds();
            $cat_id = (int) array_pop($catids);
            if ($cat_id <= 0) return $this;
            $category = Mage::getModel("catalog/category")->load($cat_id);
        }
		$maxProducts=3; 
        $this->_itemCollection = Mage::getModel('catalog/product')->getCollection() /*Getting product collection*/
		 //->addCategoryFilter($category) /*adding category filter to collection */
		 ->addStoreFilter() /*adding store filter to collection,so it will get only current store products */
		 ->addAttributeToSelect('*') /* adding all attributes to collection */
		 ->addAttributeToFilter('entity_id', array('neq' =>$product->getId())) /* removing current product from collection */
		 ->addAttributeToFilter('wattusage', array('lteq' => (int)$product->getWattusage())); /*Filter for watt usage*/
		 if((int)$product->getBattery())
		 {
			$this->_itemCollection ->addAttributeToFilter('battery', array('lteq' => (int)$product->getBattery()));
	 	 }
		 $this->_itemCollection->addAttributeToFilter('attribute_set_id',$product->getAttributeSetId())/*Adding filter for current product attribute set*/
		 ->setPageSize($maxProducts)
		 ->setOrder('battery','DESC');
		 
					
			$resource = Mage::getSingleton('core/resource');
			$storeId=Mage::app()->getStore()->getId();
			$relatedProducts=array();
			
			$range=1;/*for sizecuft +1/-1 */
			
			/* $minRange=(int)$product->getAttributeText('sizecuft')-$range;
			$maxRange=(int)$product->getAttributeText('sizecuft')+$range;
			$table = $resource->getTableName('catalog_product_flat_'.$storeId);
			$this->_itemCollection ->getSelect()
			  ->join(
			   array('so' => $table ),
			   'e.entity_id = so.entity_id and CAST(so.sizecuft_value AS DECIMAL) >= CAST('.$minRange.' AS DECIMAL) and  CAST(so.sizecuft_value AS DECIMAL) >= CAST('.$maxRange.' AS DECIMAL) ',
			   array('sizecuft_value_title'=>'sizecuft_value')
			  )->limit($maxProducts);
			; */
		 
		  
			

		/*Adding Visibility filter so product which are disabled or out of stock will be removed from collection */
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);
		
		
		
        $this->_itemCollection->load();
        foreach ($this->_itemCollection as $product) {
		
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

   
}
