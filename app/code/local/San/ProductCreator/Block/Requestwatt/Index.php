<?php 
class San_ProductCreator_Block_Requestwatt_Index extends Mage_Core_Block_Template
{
	public function __construct()
	{
		$this->setTemplate('productcreator/requestwatt/index.phtml');
	}
	public function canRequest(){
		$customer=Mage::getSingleton('customer/session')->getCustomer();
		if($customer->getRequestWatt()==0)
			return true;
		return false;
	}
	public function getMessage(){
		$customer=Mage::getSingleton('customer/session')->getCustomer();
		if($customer->getRequestWatt()==1)
		{
			return 'You have requested for watt.';
		}
		elseif($customer->getRequestWatt()==2)
		{
			return 'You are autherized to request watt.';
		}
	}
	public function getPoints()
	{
		$customer=Mage::getSingleton('customer/session')->getCustomer();
		return (int)$customer->getRewardPoints();
	}
}