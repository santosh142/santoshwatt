<?php
class San_ProductCreator_Block_Adminhtml_Wattrequest extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{	
		$this->_controller = 'adminhtml_wattrequest';
		$this->_blockGroup = 'productcreator';
		
		$this->_headerText = Mage::helper('productcreator')->__('Manage ProductCreator Wattrequest');
		//$this->_addButtonLabel = Mage::helper('productcreator')->__('Add ProductCreator');
		parent::__construct();
		//$this->removeButton('add');
	}
}