<?php
class San_ProductCreator_Block_Adminhtml_Wattrequest_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
    {
	  parent::__construct();
	  $this->setId('formGrid');
	  $this->setDefaultSort('id');
	  $this->setDefaultDir('DESC');
	  $this->setSaveParametersInSession(true);
    }

  protected function _prepareMassaction()
  {
  	$this->setMassactionIdField('id');
  	$this->getMassactionBlock()->setFormFieldName('id');
  
  	$this->getMassactionBlock()->addItem('delete', array(
  			'label'    => Mage::helper('productcreator')->__('Delete'),
  			'url'      => $this->getUrl('*/*/massDelete'),
  			'confirm'  => Mage::helper('productcreator')->__('Are you sure?')
  	));
  
  	$statuses = Mage::getSingleton('productcreator/wattrequest')->getOptionArray();
  	$this->getMassactionBlock()->addItem('status', array(
  			 'label'=> Mage::helper('productcreator')->__('Change status'),
  			 'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
  			 'additional' => array(
  					 'visibility' => array(
  							 'name' => 'status',
  							 'type' => 'select',
  							 'class' => 'required-entry',
  							 'label' => Mage::helper('productcreator')->__('Status'),
  							 'values' =>$statuses,
  					 )
  			 )
  	 ));
  	return $this;
  }
  
  
  protected function _prepareCollection()
  {
    $collection = Mage::getModel('productcreator/wattrequest')->getCollection();
      $this->setCollection($collection);
  	
  	return parent::_prepareCollection();
  }
  protected function _prepareColumns()
  {
  	$this->addColumn('id', array(
  			'header'    => Mage::helper('productcreator')->__('ID'),
  			'align'     =>'right',
  			'width'     => '50px',
  			'index'     => 'id',
  	));
  

  	$this->addColumn('product_id', array(
  			'header'    => Mage::helper('productcreator')->__('Product Id'),
  			'align'     => 'left',
   			'index'     => 'product_id',
  			'filter_index'  => 'product_id',

  	));
  
	$this->addColumn('customer_email', array(
  			'header'    => Mage::helper('productcreator')->__('Customer Email'),
  			'align'     => 'left',
   			'index'     => 'customer_email',
  			'filter_index'  => 'customer_email',

  	));
	$this->addColumn('requested_watt', array(
  			'header'    => Mage::helper('productcreator')->__('Requested Watt'),
  			'align'     => 'left',
   			'index'     => 'requested_watt',
  			'filter_index'  => 'requested_watt',

  	));
	
  	$this->addColumn('created_time', array(
  			'header'        => Mage::helper('productcreator')->__('Date Created'),
  			'align'         => 'left',
  			'type'          => 'datetime',
  			'width'         => '100px',
  			'filter_index'  => 'created_time',
  			'index'         => 'created_time',
  	));
 
 	// $this->addExportType('*/*/exportCsv', Mage::helper('productcreator')->__('CSV'));
  	//$this->addExportType('*/*/exportXml', Mage::helper('productcreator')->__('XML'));
  	
  	 
  	return parent::_prepareColumns();
  }
  
  public function getRowUrl($row)
  {
  	return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }
}