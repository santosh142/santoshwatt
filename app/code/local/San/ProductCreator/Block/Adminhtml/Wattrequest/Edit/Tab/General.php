<?php
class San_ProductCreator_Block_Adminhtml_Wattrequest_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
	
  protected function _prepareForm()
  {
     $form = new Varien_Data_Form(); 
     $this->setForm($form);
	 $productcreator = Mage::getModel('productcreator/wattrequest');
	 $productcreator_row =  $productcreator->load($this->getRequest()->getParam('id'));
     $fieldset = $form->addFieldset('productcreator_general', array('legend'=>Mage::helper('productcreator')->__("Overview")));
     
		$fieldset->addField('product_id', 'text', array(
            'label'     => Mage::helper('productcreator')->__('product_id'),
	 		'class'     => 'required-entry',
	 		'value'      =>  $productcreator_row->getProduct_id(),
	 		'required'  => true,
	 		'name'      => 'product_id'

        ));
		$fieldset->addField('customer_email', 'text', array(
            'label'     => Mage::helper('productcreator')->__('customer_email'),
	 		'class'     => 'required-entry',
	 		'value'      =>  $productcreator_row->getCustomer_email(),
	 		'required'  => true,
	 		'name'      => 'customer_email'

        ));
		$fieldset->addField('requested_watt', 'text', array(
            'label'     => Mage::helper('productcreator')->__('requested_watt'),
	 		'value'      =>  $productcreator_row->getRequested_watt(),
	 		'name'      => 'requested_watt'

        ));
		
	 	
		
		
	 	
	 	 
      if ( Mage::getSingleton('adminhtml/session')->getProductCreatorData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getProductCreatorData());
          Mage::getSingleton('adminhtml/session')->setProductCreatorData(null);
      } elseif ( Mage::registry('productcreator_data') ) {
          $form->setValues(Mage::registry('productcreator_data')->getData());
      }
      return parent::_prepareForm();
  }
 
}