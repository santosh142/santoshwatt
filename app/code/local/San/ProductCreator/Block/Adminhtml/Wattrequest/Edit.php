<?php

class San_ProductCreator_Block_Adminhtml_Wattrequest_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'productcreator';
        $this->_controller = 'adminhtml_wattrequest';//actual location of block files
        
        $this->_updateButton('save', 'label', Mage::helper('productcreator')->__('Save Wattrequest'));
        $this->_updateButton('delete', 'label', Mage::helper('productcreator')->__('Delete Wattrequest'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('productcreator_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'productcreator_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'productcreator_content');
                }
            }
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/new/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( $this->getRequest()->getParam('id')) {
            return Mage::helper('productcreator')->__('Edit Wattrequest');
        } else {
            return Mage::helper('productcreator')->__('Add Wattrequest');
        }
    }
	
}