<?php

class San_ProductCreator_Block_Adminhtml_Wattrequest_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('productcreator_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('productcreator')->__('Wattrequest Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('general', array(
          'label'     => Mage::helper('productcreator')->__('Overview'),
          'title'     => Mage::helper('productcreator')->__('Overview'),
          'content'   => $this->getLayout()->createBlock('productcreator/adminhtml_wattrequest_edit_tab_general')->toHtml(),
      ));
	  
      return parent::_beforeToHtml();
  }
}