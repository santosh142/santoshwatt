<?php 
class San_ProductCreator_Block_Catalog_Product_Iswh extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface {
    public function __construct()
    {
		
        //$this->setTemplate('easylife_attr/product/custom.phtml'); //set a template
    }
    public function render(Varien_Data_Form_Element_Abstract $element) {
        //$this->setElement($element);
		$script='<script>
					function isWh(element){
						if(element.value=="1"){
							element.value=0;
						}
						else
						{
							element.value=1;
						}
					}
				</script>';
		$checked=(int)$element->getValue()==1 ?' checked="checked" ':'';
        return '<tr>
    <td class="label"><label for="is_wh">Is Wh <span class="required"></span></label></td>
    <td class="value">
        <input type="checkbox" '.$checked.' onclick="isWh(this)" value="'.$element->getValue().'" name="product[is_wh]" id="is_wh">            </td>
    <td class="scope-label"><span class="nobr"></span>'.$script.'</td>
    </tr>';
    }
 
}