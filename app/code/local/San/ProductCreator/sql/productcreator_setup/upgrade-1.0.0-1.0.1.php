<?php
$installer = $model=Mage::getModel('eav/entity_setup','core_setup');
 
$installer->startSetup();
 
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'is_wh', array(
    'group'             => 'General',
    'type'              => 'text',
    'backend'           => '',
    'input_renderer'    => 'productcreator/catalog_product_iswh',//definition of renderer
    'label'             => 'Is WH',
    'class'             => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
));
 
$installer->endSetup();