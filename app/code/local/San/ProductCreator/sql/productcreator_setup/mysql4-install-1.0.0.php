<?php

$installer = $this;
$installer->startSetup();
$installer->run("-- DROP TABLE IF EXISTS {$this->getTable('productcreator/wattrequest')};
CREATE TABLE {$this->getTable('productcreator/wattrequest')} (
	  `id` int(11) unsigned NOT NULL auto_increment,
	  `customer_email` varchar(255) NOT NULL default '',
	  `product_id` int(11) NOT NULL,
	  `requested_watt` decimal(11) NOT NULL default '0',
	  `created_time` datetime NULL,
	  `update_time` datetime NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	");
	
	
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$setup->addAttribute("customer", "request_watt",  array(
    "type"     => "int",
    "backend"  => "",
    "label"    => "Request Watt Status",
    "input"    => "select",
	'source'   => 'productcreator/entity_attribute_requestwatt',
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
   /* "note"       => "Request Watt Status" */

        ));

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "request_watt");
$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'request_watt',
    '999'  //sort_order
);
$used_in_forms=array();

$used_in_forms[]="adminhtml_customer";
//$used_in_forms[]="checkout_register";
//$used_in_forms[]="customer_account_create";
//$used_in_forms[]="customer_account_edit";
//$used_in_forms[]="adminhtml_checkout";
$attribute->setData("used_in_forms", $used_in_forms)
		->setData("is_used_for_customer_segment", true)
		->setData("is_system", 0)
		->setData("is_user_defined", 1)
		->setData("is_visible", 1)
		->setData("sort_order", 100)
		;
$attribute->save();


$setup->addAttribute("customer", "reward_points",  array(
    "type"     => "decimal",
    "backend"  => "",
    "label"    => "Reward Points",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    /* "note"       => "Reward Points" */

        ));

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "reward_points");
$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'reward_points',
    '1000'  //sort_order
);
$used_in_forms=array();

$used_in_forms[]="adminhtml_customer";
//$used_in_forms[]="checkout_register";
//$used_in_forms[]="customer_account_create";
//$used_in_forms[]="customer_account_edit";
//$used_in_forms[]="adminhtml_checkout";
$attribute->setData("used_in_forms", $used_in_forms)
		->setData("is_used_for_customer_segment", true)
		->setData("is_system", 0)
		->setData("is_user_defined", 1)
		->setData("is_visible", 1)
		->setData("sort_order", 100)
		;
$attribute->save();

$installer->endSetup();