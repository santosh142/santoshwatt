<?php 
class San_ProductCreator_Model_Wattrequest extends Mage_Core_Model_Abstract
{
	protected function _construct()
	{
		$this->_init('productcreator/wattrequest');
	}
	public function getOptionArray()
	{
		return  array('apply'=>'Apply','unapply'=>'Unapply');
	}
}

?>