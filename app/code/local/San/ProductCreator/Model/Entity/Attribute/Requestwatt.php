<?php
class San_ProductCreator_Model_Entity_Attribute_Requestwatt extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
		return  array(

			array(
				"label" => Mage::helper("productcreator")->__("Not Approved"),
				"value" =>  0
			),
			array(
				"label" => Mage::helper("productcreator")->__("Requested For Approval"),
				"value" =>  1
			),
			array(
				"label" => Mage::helper("productcreator")->__("Approved"),
				"value" =>  2
			),


		);

    }
}