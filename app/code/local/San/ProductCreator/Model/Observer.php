<?php
class San_ProductCreator_Model_Observer {
    public function convertCustomValues($observer) {
        $form = $observer->getEvent()->getForm();
        $customValues = $form->getElement('is_wh');
        if ($customValues) {
		   $customValues->setRenderer(
                Mage::app()->getLayout()->createBlock('productcreator/catalog_product_iswh')
            );  //set a custom renderer to your attribute
        }
    }
}