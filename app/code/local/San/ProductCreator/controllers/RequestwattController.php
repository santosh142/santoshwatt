<?php
class San_ProductCreator_RequestwattController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}
	public function accessAction()
	{
		$customer=Mage::getSingleton('customer/session')->getCustomer();
		$customer->setRequestWatt(1)->save();
		Mage::getSingleton('customer/session')->addSuccess('Thank You For Your Request.');
		$this->_redirect('*/*/index');
	}
	public function submitAction(){
		if(Mage::helper('productcreator')->canRequestWatt()){
			$rquestwattModel=Mage::getModel('productcreator/wattrequest');
			$rquestwattModel->setProductId($this->getRequest()->getParam('product'));
			$customer=Mage::getSingleton('customer/session')->getCustomer();
			$rquestwattModel->setCustomerEmail($customer->getEmail());
			$rquestwattModel->setRequestedWatt($this->getRequest()->getParam('watt'));
			$rquestwattModel->setCreatedTime(now());
			$rquestwattModel->setUpdatedTime(now());
			$rquestwattModel->save();
			Mage::getSingleton('core/session')->addSuccess('Thank You For Your Submission.');
		}
		else
		{
			Mage::getSingleton('core/session')->addError('You are not autherised for requesting watt.');
		}
		$this->_redirectReferer($this->getRequest()->getParam('redirect'));
	}
	/**
	 * Set referer url for redirect in responce
	 *
	 * @param   string $defaultUrl
	 * @return  Mage_Core_Controller_Varien_Action
	 */
	protected function _redirectReferer($refererUrl=null)
	{
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
	}
}