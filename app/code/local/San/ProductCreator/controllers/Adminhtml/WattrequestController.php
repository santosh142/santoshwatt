<?php 

class San_ProductCreator_Adminhtml_WattrequestController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		
	 $this->loadLayout();
	 $this->_addContent($this->getLayout()->createBlock('productcreator/adminhtml_wattrequest'));
	 $this->renderLayout();	

	}
	public function editAction()

	{
		$this->_forward('new');
	}
	
	public function saveAction()
	{	

		if($id=$this->getRequest()->getParam('id'))
		{ 		
		
				if($post = $this->getRequest()->getPost())		
				{	
					$data=$post;
					$data['updated_time']=now();
					$productcreator = Mage::getModel('productcreator/wattrequest')->load($id); 
					$productcreator->addData($data)->setId($id)->save();
					Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('productcreator')->__('The productcreator details has been saved.'));
					
				}
		}
		else
		{	
				if($post = $this->getRequest()->getPost())
				{
					$data=$post;
					$data['created_time']=now();
					$data['updated_time']=now();
					
						$productcreator = Mage::getModel('productcreator/wattrequest'); 
						$productcreator->setData($data)->save();
						Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('productcreator')->__('The Watt request has been saved.'));
						$this->_redirect('adminhtml/adminhtml_wattrequest/new',array('id'=>$productcreator->getId()));
					
					
				}
		}
		if($this->getRequest()->getParam('back'))
		{
			$this->_forward('new');
		}
		else
		{
			$this->_redirect('adminhtml/adminhtml_wattrequest/index');
		}
		
	}
	public function deleteAction()
	{
		
		$id=$this->getRequest()->getParam('id');
		$obj=Mage::getModel('productcreator/wattrequest');
		$ob=$obj->load($id);
		$ob->delete();
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('productcreator')->__('Watt request Deleted Successfully...'));
		$this->_redirect('adminhtml/adminhtml_wattrequest/index');
	
	}
	public function massStatusAction()
	{
		
		$status=$this->getRequest()->getParam('status');
		if( $this->getRequest()->getParam('id') > 0 ) {
			
			try{
				foreach($this->getRequest()->getParam('id') as $id)
				{	
					$model = Mage::getModel('productcreator/wattrequest')->load($id);
					
					$model->setId($id)
					->setStatus($status)->save();
				}
			}
			catch(Exception $e)	
			{
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('%s',$e->getMessage()));
			}
		}
			
			
		$this->_redirect('*/*/index');
	}

	public function massDeleteAction() {

		if( $this->getRequest()->getParam('id') > 0 ) {
			try {

				$model = Mage::getModel('productcreator/wattrequest');
				foreach($this->getRequest()->getParam('id') as $id)
				{	
					$model = Mage::getModel('productcreator/wattrequest')->load($id);

					 $model->delete();
					
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Watt request\'s  deleted successfully '));
				$this->_redirect('*/*/index');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}

		$this->_redirect('*/*/index');
	}
	public function newAction(){
			
            $this->loadLayout();
			$this->_setActiveMenu('productcreator/wattrequest');
			$this->_addContent($this->getLayout()->createBlock('productcreator/adminhtml_wattrequest_edit'))
				->_addLeft($this->getLayout()->createBlock('productcreator/adminhtml_wattrequest_edit_tabs'));
			$this->renderLayout();
	}
	public function sendinvitationAction()
	{
		
		try{
				$id=$this->getRequest()->getParam('id');
				$productcreator=Mage::getModel('productcreator/wattrequest')->load($id);
				$data=array();
				$data['productcreator_name']= $productcreator->getFirstName().' '.$productcreator->getLastName();
				$data['email']=$productcreator->getEmailAddress();
				$data['admin_email']=Mage::getStoreConfig('trans_email/ident_general/email');
				$data['admin_name']= Mage::getStoreConfig('trans_email/ident_general/name');
				$data['link']='';
				$emailTemplate = Mage::getModel('core/email_template');
				$emailTemplate->loadDefault('send_invitation_email_template');
				$emailTemplate->setTemplateSubject("Message from ".$data['admin_name']);
				$emailTemplate->setSenderName($data['productcreator_name']);
				$emailTemplate->setSenderEmail($data['admin_email']);
				$processedTemplate = $emailTemplate->getProcessedTemplate($data);
				$emailTemplate->send($data['email'],$data);
			}catch(Exception $e){
				die($e->getMessage());
			}
	}
		
	
	
	
}