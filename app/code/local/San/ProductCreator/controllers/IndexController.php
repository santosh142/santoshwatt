<?php
class San_ProductCreator_IndexController extends Mage_Core_Controller_Front_Action
{



    public function indexAction()
    {
		
        $this->loadLayout();
        $this->getLayout()->getBlock('productcreatorForm')
            ->setFormAction( Mage::getUrl('*/*/create') );

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

	public function createAction()
	{
		$data=$this->getRequest()->getParams();
		$data['product']['name']=$data['product']['manufacturer'].' '.$data['product']['modelnumber'];
		$data['product']['manufacturer_name']=$data['product']['manufacturer'];
		$data['product']['wp_amazon_local']='us';
		$data['product']['description']='Find the power consumption and additional energy usage details for the '.$data['product']['manufacturer'].' '.$data['product']['modelnumber'];
		$data['product']['short_description']='Find the power consumption and additional energy usage details for the '.$data['product']['manufacturer'].' '.$data['product']['modelnumber'];
		$data['product']['manufacturer']=$this->getAttributeValue('manufacturer',$data['product']['manufacturer']);
		$sku=str_replace(' ','-',$data['product']['name']);
		$sku=preg_replace('/[^A-Za-z0-9\-]/', '', $sku);
		$id = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
		if($id)
		{
			Mage::getSingleton('core/session')->addError('Model number already exist please try diffrent model number. ');

			$this->_redirect('*/*/index');
			return;
		}
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		$product = Mage::getModel('catalog/product');
		//    if(!$product->getIdBySku('testsku61')):
		$attributeSet= Mage::getModel('eav/entity_attribute_set')->load($data['product']['attribute_set_id']);
		$websites=array();
		$_websites = Mage::app()->getWebsites(); 
		foreach($_websites as $website){    $websites[]=$website->getId();
		}
		
		try{
			$product
		//    ->setStoreId(1) //you can set data in store scope
			->setWebsiteIds($websites) //website ID the product is assigned to, as an array
				//ID of a attribute set named 'default'
			->setTypeId('simple') //product type
			->setCreatedAt(strtotime('now')) ;//product creation time
		//    ->setUpdatedAt(strtotime('now')) //product update time
			foreach($data['product'] as $attribute_code=>$value)
			{
				$product->setData($attribute_code,$value);
			}
			
			$product->setSku($sku) //SKU
			->setWeight(4.0000)
			->setStatus(2) //product status (1 - enabled, 2 - disabled)
			->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
			->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility		 
			->setPrice(1) //price in form 11.22
			
		 
		 
			/*->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
			->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery
			*/
			->setStockData(array(
							   'use_config_manage_stock' => 0, //'Use config settings' checkbox
							   'manage_stock'=>1, //manage stock
							   'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
							   'max_sale_qty'=>999, //Maximum Qty Allowed in Shopping Cart
							   'is_in_stock' => 1, //Stock Availability
							   'qty' => 1 //qty
						   )
			);
			$attributeSetName=$attributeSet->getAttributeSetName();
			if(strtolower($attributeSetName)=='refrigerators'||strtolower($attributeSetName)=='freezers')
			{
				$data['product']['runs24hours']='Yes';
				$product->setRuns24hours(1);
			}
			else
			{
				$data['product']['runs24hours']='No';
				$product->setRuns24hours(0);	
			}
			if(strtolower($attributeSetName)=='washers'||strtolower($attributeSetName)=='dryers'||strtolower($attributeSetName)=='dishwashers')
			{
				$data['product']['loads']='Yes';
				$product->setLoads(1);
			}
			else
			{
				$data['product']['loads']='No';
				$product->setLoads(0);
			}
			if(isset($_FILES['product_image'])&& $_FILES['product_image']['size']>0){
				/* var_dump($_FILES['product_image']) */
				$uploadResponse=$this->UploadImage();
				$image=Mage::getBaseUrl('media').'catalog/product'.$uploadResponse['file'];
				$filePath=Mage::getBaseDir().DS.'media'.DS.'catalog'.DS.'product'.$uploadResponse['file'];
				$product->addImageToMediaGallery($filePath, array ('image','small_image','thumbnail'), false);
			}
			$product
			->setCategoryIds(array(60)) //assign product to categories
			->save() ;
			$this->sendMail($data['product']);
			Mage::getSingleton('core/session')->addSuccess('Thank you for your submission.');
			
		
		}catch(Exception $e){
			Mage::getSingleton('core/session')->addError($e->getMessage());
			Mage::log($e->getMessage());
		}
		
		$this->_redirectReferer($data['url']);
	}
	
	/**
     * Action is for android request
     */
	public function createproductAction(){
		
		$data=$this->getRequest()->getParams();
		$response=array();
		$response['request']=json_encode($data);
		if(isset($data['product'])){
			$data['product']['name']=$data['product']['manufacturer'].' '.$data['product']['modelnumber'];
			$data['product']['manufacturer_name']=$data['product']['manufacturer'];
			$data['product']['wp_amazon_local']='us';
			$data['product']['description']='Find the power consumption and additional energy usage details for the '.$data['product']['manufacturer'].' '.$data['product']['modelnumber'];
			$data['product']['short_description']='Find the power consumption and additional energy usage details for the '.$data['product']['manufacturer'].' '.$data['product']['modelnumber'];
			$data['product']['manufacturer']=$this->getAttributeValue('manufacturer',$data['product']['manufacturer']);
			$sku=str_replace(' ','-',$data['product']['name']);
			$sku=preg_replace('/[^A-Za-z0-9\-]/', '', $sku);
			$id = Mage::getModel('catalog/product')->getResource()->getIdBySku($sku);
			if($id)
			{
				Mage::getSingleton('core/session')->addError('Model number already exist please try diffrent model number. ');

				$this->_redirect('*/*/index');
				return;
			}
			Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
			$product = Mage::getModel('catalog/product');
			//    if(!$product->getIdBySku('testsku61')):
			$attributeSet= Mage::getModel('eav/entity_attribute_set')->load($data['product']['attribute_set_id']);
			$websites=array();
			$_websites = Mage::app()->getWebsites(); 
			foreach($_websites as $website){    $websites[]=$website->getId();
			}
			
			try{
				$product
			//    ->setStoreId(1) //you can set data in store scope
				->setWebsiteIds($websites) //website ID the product is assigned to, as an array
					//ID of a attribute set named 'default'
				->setTypeId('simple') //product type
				->setCreatedAt(strtotime('now')) ;//product creation time
			//    ->setUpdatedAt(strtotime('now')) //product update time
				foreach($data['product'] as $attribute_code=>$value)
				{
					$product->setData($attribute_code,$value);
				}
				
				$product->setSku($sku) //SKU
				->setWeight(4.0000)
				->setStatus(2) //product status (1 - enabled, 2 - disabled)
				->setTaxClassId(0) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
				->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility		 
				->setPrice(1) //price in form 11.22
				
			 
			 
				/*->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
				->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery
				*/
				->setStockData(array(
								   'use_config_manage_stock' => 0, //'Use config settings' checkbox
								   'manage_stock'=>1, //manage stock
								   'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
								   'max_sale_qty'=>999, //Maximum Qty Allowed in Shopping Cart
								   'is_in_stock' => 1, //Stock Availability
								   'qty' => 1 //qty
							   )
				);
				$attributeSetName=$attributeSet->getAttributeSetName();
				if(strtolower($attributeSetName)=='refrigerators'||strtolower($attributeSetName)=='freezers')
				{
					$data['product']['runs24hours']='Yes';
					$product->setRuns24hours(1);
				}
				else
				{
					$data['product']['runs24hours']='No';
					$product->setRuns24hours(0);	
				}
				if(strtolower($attributeSetName)=='washers'||strtolower($attributeSetName)=='dryers'||strtolower($attributeSetName)=='dishwashers')
				{
					$data['product']['loads']='Yes';
					$product->setLoads(1);
				}
				else
				{
					$data['product']['loads']='No';
					$product->setLoads(0);
				}
				if(isset($_FILES['product_image'])){
					$uploadResponse=$this->UploadImage();
					$image=Mage::getBaseUrl('media').'catalog/product'.$uploadResponse['file'];
					$filePath=Mage::getBaseDir().DS.'media'.DS.'catalog'.DS.'product'.$uploadResponse['file'];
					$product->addImageToMediaGallery($filePath, array ('image','small_image','thumbnail'), false);
				}
				$product
				->setCategoryIds(array(60)) //assign product to categories
				->save() ;
				$this->sendMail($data['product']);
				Mage::getSingleton('core/session')->addSuccess('Thank you for your submission.');
				$response['success']=true;
				$response['message']='Thank you for your submission.';
			
			}catch(Exception $e){
				Mage::getSingleton('core/session')->addError($e->getMessage());
				$response['success']=false;
				$response['message']=$e->getMessage();
			}
		}
		else
		{
			$response['success']=false;
			$response['message']='Required Parameters are missing';
		}
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody(json_encode($response));
	}
	
	/**
     * Fetch dropdown details    *
     */
	 
	 public function fetchdropdownsAction()
	 {
		
		$response=array();
		$entityTypeId = Mage::getModel('eav/config')->getEntityType('catalog_product')->getEntityTypeId();/*getting entity type id of catalog product*/
						
		/* Getting default attribute set id*/
		$defaultAttributeSetId = Mage::getSingleton('eav/config')
		->getEntityType(Mage_Catalog_Model_Product::ENTITY)
		->getDefaultAttributeSetId();
		
		/*Getting all attributes sets belongs to catalog product entity*/
		$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')->addFieldToFilter('entity_type_id',$entityTypeId) ->load();
		foreach ($attributeSetCollection as $id=>$attributeSet) {
			if($id==$defaultAttributeSetId)
				continue;
			$response['product_type'][$id]=$attributeSet->getAttributeSetName();
		}
		
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody(json_encode($response));
	 }
	 /**
     * Lead form  return action  * 
     */
	 public function leadsuccessAction(){
		 Mage::getSingleton('core/session')->addSuccess('Thank you for your submission.');
		 $url=$this->getRequest()->getParam('redirect');
		
		 $this->_redirectReferer($url);
	 }
	
	/**
     * Upload Import Images     *
     */
    public function UploadImage()
    {
		try
		{
			
			$path = Mage::getBaseDir().DS.'media'.DS.'catalog'.DS.'product'.DS;
			$upload_img = new Mage_Core_Model_File_Uploader('product_image');
			$upload_img->setAllowedExtensions(array('jpg','jpeg','gif','png'));
			$upload_img->setAllowRenameFiles(true);
			$upload_img->setFilesDispersion(true);
			$result = $upload_img->save($path);
			return $result;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();die;
		}
    }
	public function getAttributeValue($attributeCode,$attributeText)
	{
		$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product',$attributeCode);
		if ($attribute->usesSource()) {
			$options = $attribute->getSource()->getAllOptions(false);
		}
		foreach($options as $option)
		{
			
			if(strtolower($option['label'])==strtolower(trim($attributeText)))
			{
				
				return $option['value'];
			}
		}
		
	}
	public function sendMail($data)
	{
		$emailTemplate  = Mage::getModel('core/email_template')
						->loadDefault('productcreator_email_template');									
		$storeId=Mage::app()->getStore()->getId();
		//Create an array of variables to assign to template
		$emailTemplateVariables = array();
		$content="<p>Attribute Set : {$data['attribute_set_id']}</p>";
		$content.="<p>Manufacturer : {$data['manufacturer_name']}</p>";
		$content.="<p>Model Number : {$data['modelnumber']}</p>";
		$content.="<p>Email : {$data['email']}</p>";
		$content.="<p>Watt Usage : {$data['wattusage']}</p>";
		$content.="<p>Watt Standby : {$data['wattstandby']}</p>";
		$content.="<p>kWh Per Year : {$data['kwhyear']}</p>";
		$content.="<p>ASIN : {$data['wp_amazon_asin']}</p>";
		$content.="<p>Energy Star : {$data['energystarcertified']}</p>";
		$content.="<p>Runs 24 hours : {$data['runs24hours']}</p>";
		$content.="<p>Loads : {$data['loads']}</p>";
		
		$emailTemplateVariables['content'] =$content;
		$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		$emailTemplate->setSenderName('Corey');
		$emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email', $storeId));
		
		$email=Mage::getStoreConfig('trans_email/ident_general/email', $storeId);
		$name=Mage::getStoreConfig('trans_email/ident_general/name', $storeId);
		$emailTemplate->send($email,$name, $emailTemplateVariables);
	}
	
	/**
	 * Set referer url for redirect in responce
	 *
	 * @param   string $defaultUrl
	 * @return  Mage_Core_Controller_Varien_Action
	 */
	protected function _redirectReferer($refererUrl=null)
	{
		$this->getResponse()->setRedirect($refererUrl);
		return $this;
	}

	/**
	 * Identify referer url via all accepted methods (HTTP_REFERER, regular or base64-encoded request param)
	 *
	 * @return string
	 */
	protected function _getRefererUrl()
	{
		$refererUrl = $this->getRequest()->getServer('HTTP_REFERER');
		if ($url = $this->getRequest()->getParam(self::PARAM_NAME_REFERER_URL)) {
			$refererUrl = $url;
		}
		if ($url = $this->getRequest()->getParam(self::PARAM_NAME_BASE64_URL)) {
			$refererUrl = Mage::helper('core')->urlDecode($url);
		}
		if ($url = $this->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
			$refererUrl = Mage::helper('core')->urlDecode($url);
		}

		if (!$this->_isUrlInternal($refererUrl)) {
			$refererUrl = Mage::app()->getStore()->getBaseUrl();
		}
		return $refererUrl;
	}
	
	public function modelsuggestionAction()
	{
		try{
			$finalResult=array();
			$model=$this->getRequest()->getParam('model');
			$maxProducts=5;
			$finalResult['html']='<ul>';
			$products=Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')
					->addAttributeToFilter('modelnumber',array('like'=>$model.'%'))
					->setPageSize($maxProducts);
			foreach($products as $product)
			{
				$finalResult['html'].=$this->getHtml($product);
			}
			
			$finalResult['html'].='</ul>';
			$finalResult['count']=count($products);
			$finalResult['success']=1;
			
		}
		catch(Exception $e)
		{
			$finalResult['success']=0;
			$finalResult['error']=$e->getMessage();
		}
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody(json_encode($finalResult));
	}
	/* Function for getting suggestion html for product*/
	public function getHtml($product)
	{
		$html='<li>';
		$html.='<span><a href="'.$product->getProductUrl().'" target="_blank">'.$product->getModelnumber().'</a></span>';
		$html.='</li>';
		return $html;
	}
	/*
 	 * Return action for sales force
	 */
	public function salessubmitAction()
	{
		if($data=$this->getRequest()->getParams())
		{
			$queryString="";
			foreach($data as $key=>$value) {
				$queryString .= $key.'='.utf8_encode($value).'&';
			} 
			$response=$this->apiCall($queryString);
			$this->_redirectReferer($this->getRequest()->getParam('retURL'));
			return;
		}
		 $this->_redirectReferer($this->getRequest()->getParam('redirecturl'));
	}
	/**
	 * Calling api through curl
	 *
	 * @return array
	 */
	private function apiCall($queryString)
	{
		$url ='https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_POST,true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$queryString);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$curlOutput = curl_exec($ch);
		
		//execute post

		$info = curl_getinfo($ch);
		curl_close($ch);
		return $response;
	}
}
