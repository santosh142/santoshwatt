<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   IFN
 * @package    IFN_Dashboard
 * @copyright  Copyright (c) 2014 IFN Modern
 * @license    
 */

class KKK_Simple_Adminhtml_SproductController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
		//$this->_title($this->__('Catalog / Product'));
        $this->loadLayout()
        	->_setActiveMenu('catalog')
        	->_addBreadcrumb(Mage::helper('adminhtml')->__('Catalog'), Mage::helper('kkksimple')->__('Product'));        
        $this->_addContent($this->getLayout()->createBlock('kkksimple/adminhtml_sproduct'));
        $this->renderLayout();

        return $this;
    }
    public function saveAction()
	{		
		
		$this->_title($this->__('Catalog / Product'));
        $this->loadLayout()
        	->_setActiveMenu('catalog')
        	->_addBreadcrumb(Mage::helper('adminhtml')->__('Catalog'), Mage::helper('kkksimple')->__('Product'));        
        $this->renderLayout();
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		$product = Mage::getModel('catalog/product');
		$pddata=$this->getRequest()->getParams();
		
		if($pddata['product_tax'] == 0){
			$pd_tax = 0;
		}else if($pddata['product_tax'] == 1){
			$pd_tax = 2;
		}else{
			$pd_tax = 4;
		}

		try{
			if(isset($_FILES['product_img'])) {                     
                $newFileName = $this->uploadPhoto('product_img');
                        $data = $newFileName;                     
            }
		}catch(Exception $e){
			Mage::log($e->getMessage());
		}

		$f_letter = substr($data, 0, 1);
        $s_letter = substr($data, 1, 1);

        	Mage::app();//->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
	      $entityType = Mage::getModel('catalog/product')->getResource()->getTypeId();
	      $collection = Mage::getResourceModel('eav/entity_attribute_set_collection')->setEntityTypeFilter($entityType);
	      $allSet = array();
	      foreach($collection as $coll){
	         $attributeSet['name'] = $coll->getAttributeSetName();
	         $attributeSet['id'] = $coll->getAttributeSetId();
	         $allSet[] = $attributeSet;
	      }
	      if($allSet[0]['name'] == 'Default'){
	      	$attr_id = $allSet[0]['id'];
	      }
	      //var_dump($pddata);
		//    if(!$product->getIdBySku('testsku61')):
	      $category = Mage::getModel('catalog/category');
	      $tree = $category->getTreeModel();
	      $tree->load();
	      $ids = $tree->getCollection()->getAllIds();
	      if ($ids){
	          foreach ($ids as $id){
	              $cat = Mage::getModel('catalog/category');
	              $cat->load($id);
	              
	              $entity_id = $cat->getId();
	              $name = $cat->getName();
	              $paths = explode('/',$cat->getPath());
	              //var_dump(count($paths));
	              for($i = 2; $i < count($paths); $i++){
	                $name = '-' . $name;
	              }
	              if($name != 'Root Catalog'){
	                array_push($cat_arr, $name);
	              }
	              $url_key = $cat->getUrlKey();
	              $url_path = $cat->getUrlPath();
	          }
	      }
		  //$attr = $product->getResource()->getAttribute('maxwattusage');
		  //var_dump($attr->getData());
		

		try{
		$product
		//    ->setStoreId(1) //you can set data in store scope
			->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
			->setAttributeSetId($attr_id) //ID of a attribute set named 'default'
			->setTypeId('simple') //product type
			->setCreatedAt(strtotime('now')) //product creation time
		//    ->setUpdatedAt(strtotime('now')) //product update time
		 
			->setSku($pddata['product_sku']) //SKU
			->setName($pddata['product_name']) //product name
			->setWeight($pddata['1'])
			->setStatus($pddata['product_stu'] + 1) //product status (1 - enabled, 2 - disabled)
			->setTaxClassId('None') //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
			->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
		//	->setManufacturer(28) //manufacturer id
		//	->setColor(24)
		//	->setNewsFromDate('06/26/2014') //product set as new from
		//	->setNewsToDate('06/30/2014') //product set as new to
		//	->setCountryOfManufacture('US') //country of manufacture (2-letter country code)
		 
			->setPrice($pddata['product_price']) //price in form 11.22
			->setCost($pddata['product_price']) //price in form 11.22
		//	->setSpecialPrice(00.44) //special price in form 11.22
		//	->setSpecialFromDate('06/1/2014') //special price from (MM-DD-YYYY)
		//	->setSpecialToDate('06/30/2014') //special price to (MM-DD-YYYY)
		//	->setMsrpEnabled(1) //enable MAP
		//	->setMsrpDisplayActualPriceType(1) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
		//	->setMsrp(99.99) //Manufacturer's Suggested Retail Price
		 
			->setMetaTitle('test meta title 2')
			->setMetaKeyword('')
			->setMetaDescription('Find the power consumption of' . $pddata['product_name'])
		 
			->setDescription($pddata['product_desc'])
			->setShortDescription('Find the power consumption of' . $pddata['product_name'])
		 	->setmaxwattusage($pddata['maxwattusage'])
		 	->setwattstandby($pddata['wattstandby'])
		 	->setwattusage($pddata['wattusage'])
		 	->setenergystarcertified($pddata['energystar']+1)
		 	->setmodelnumber($pddata['modelnumber'])
			->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
			->addImageToMediaGallery('media/catalog/product'. DS . $f_letter . DS . $s_letter . DS . $data, array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery
		 
			->setStockData(array(
							   'use_config_manage_stock' => 0, //'Use config settings' checkbox
							//   'manage_stock'=>1, //manage stock
							   'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
							   'max_sale_qty'=>2, //Maximum Qty Allowed in Shopping Cart
							   'is_in_stock' => 1, //Stock Availability
							 //  'qty' => 999 //qty
						   )
			)
		 
			->setCategoryIds($ids[$pddata['product_cat']+1]); //assign product to categories
		$product->save();
		//endif;
		}catch(Exception $e){
			Mage::log($e->getMessage());
			var_dump($e);
		}
		//exit();
		return $this->getResponse()->setRedirect($this->getUrl($this->getRequest()->getParam('ret') == 'index' ? '*/*/index' : '*/*/'));
		
	}

	public function uploadPhoto($photoName)
    {
        try {
            $uploader = new Varien_File_Uploader($photoName);
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            
            $ext = pathinfo($_FILES[$photoName]['name'], PATHINFO_EXTENSION);
            
            $newFileName = basename($_FILES[$photoName]['name'], '.' . $ext) . uniqid() . '.' . $ext;

            $f_letter = substr($newFileName, 0, 1);
            $s_letter = substr($newFileName, 1, 1);
            $path = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . DS . $f_letter . DS . $s_letter;
            $result = $uploader->save($path, $newFileName);   
          } catch(Exception $e) { 
          }
          return $newFileName;
    }    
}
