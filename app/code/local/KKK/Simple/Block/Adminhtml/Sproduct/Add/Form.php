<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml add product review form
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class KKK_Simple_Block_Adminhtml_Sproduct_Add_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
    {
      $form = new Varien_Data_Form(array(
          'enctype' => 'multipart/form-data',
          'method'    => 'post',
          'action'    => $this->getUrl('*/*/save')
      ));

      $fieldset = $form->addFieldset('product_details', array('legend' => Mage::helper('review')->__('Product Details')));

      $fieldset->addField('product_name', 'text', array(
          'label'     => Mage::helper('review')->__('Product Name'),
          'name'      => 'product_name',
          'required'      => true
      ));
      $cat_arr = array();
      //array_push($cat_arr, 'name');

      $category = Mage::getModel('catalog/category');
      $tree = $category->getTreeModel();
      $tree->load();
      $ids = $tree->getCollection()->getAllIds();
      if ($ids){
          foreach ($ids as $id){
              $cat = Mage::getModel('catalog/category');
              $cat->load($id);
              
              $entity_id = $cat->getId();
              $name = $cat->getName();
              $paths = explode('/',$cat->getPath());
              //var_dump(count($paths));
              for($i = 2; $i < count($paths); $i++){
                $name = '-' . $name;
              }
              if($name != 'Root Catalog'){
                array_push($cat_arr, $name);
              }
              $url_key = $cat->getUrlKey();
              $url_path = $cat->getUrlPath();
          }
      }

      $fieldset->addField('product_cat', 'select', array(
          'label'     => Mage::helper('review')->__('Category'),
          'name'      => 'product_cat',
          'required'      => true,
          'values'    => $cat_arr
      ));
/*      
      $fieldset->addField('product_desc', 'textarea', array(
          'label'     => Mage::helper('review')->__('Description'),
          'name'      => 'product_desc',
          'required'      => true
      ));

      $fieldset->addField('product_sdesc', 'textarea', array(
          'label'     => Mage::helper('review')->__('Short Description'),
          'name'      => 'product_sdesc',
          'required'      => true
      ));
*/
      $fieldset->addField('product_sku', 'text', array(
          'label'     => Mage::helper('review')->__('SKU'),
          'name'      => 'product_sku',
          'required'      => true
      ));

      $fieldset->addField('product_img', 'file', array(
          'label'     => Mage::helper('review')->__('Product Image'),
          'name'      => 'product_img',
          'required'      => false
      ));

/*      $fieldset->addField('product_wt', 'text', array(
          'label'     => Mage::helper('review')->__('Weight'),
          'name'      => 'product_wt',
          'required'      => true
      )); */

      $fieldset->addField('product_stu', 'select', array(
          'label'     => Mage::helper('review')->__('Status'),
          'name'      => 'product_stu',
          'values'     => array('Enable','Disable'),
          'required'      => true
      ));

      $fieldset->addField('product_price', 'text', array(
          'label'     => Mage::helper('review')->__('Price'),
          'name'      => 'product_price',
          'required'      => true
      ));
/*
      $fieldset->addField('product_tax', 'select', array(
          'label'     => Mage::helper('review')->__('TAX'),
          'name'      => 'product_tax',
          'values'     => array('None','Taxable Good','Shipping'),
          'required'      => true
      ));
*/
      $fieldset1 = $form->addFieldset('additional_info', array('legend' => Mage::helper('review')->__('Additional Information')));

      $fieldset1->addField('maxwattusage', 'text', array(
          'label'     => Mage::helper('review')->__('MaxWattUsage'),
          'name'      => 'maxwattusage'
      ));

      $fieldset1->addField('wattstandby', 'text', array(
          'label'     => Mage::helper('review')->__('WattStandby'),
          'name'      => 'wattstandby'
      ));

      $fieldset1->addField('wattusage', 'text', array(
          'label'     => Mage::helper('review')->__('WattUsage'),
          'name'      => 'wattusage'
      ));

      $fieldset1->addField('energystar', 'select', array(
          'label'     => Mage::helper('review')->__('EnergyStarCertified'),
          'name'      => 'energystar',
          'values'     => array('Yes','No')
      ));

      $fieldset1->addField('modelnumber', 'text', array(
          'label'     => Mage::helper('review')->__('ModelNumber'),
          'name'      => 'modelnumber'
      ));

      $form->setMethod('post');
      $form->setUseContainer(true);
      $form->setId('edit_form');
      $form->setAction($this->getUrl('*/*/save'));
      $this->setForm($form);
    }
}
