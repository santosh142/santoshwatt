<?php
/**
 * @copyright   Copyright (c) 2010 Amasty (http://www.amasty.com)
 */   
class KKK_Simple_Block_Adminhtml_Sproduct extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_sproduct';
        $this->_blockGroup = 'kkksimple';
        $this->_mode = 'add';
        $this->_headerText = Mage::helper('kkksimple')->__('Simple Product Create');
        
        //$this->_removeButton('add');
    }
}