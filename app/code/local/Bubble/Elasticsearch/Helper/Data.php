<?php
/**
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @var array
     */
    protected $_config;

    /**
     * Allowed languages
     * Example: array('en_US' => 'en', 'fr_FR' => 'fr')
     *
     * @var array
     */
    protected $_languageCodes = array();

    /**
     * Searchable attributes
     *
     * @var array
     */
    protected $_searchableAttributes;

    /**
     * Escapes specified value
     *
     * @param string $value
     * @return mixed
     */
    public function escape($value)
    {
        return $value;
    }

    /**
     * @param mixed $store
     * @return array
     */
    public function getEngineConfigData($store = null)
    {
        if (null !== $this->_config) {
            return $this->_config;
        }

        $config = Mage::getStoreConfig('catalog/search', $store);
        $data = array();
        foreach ($config as $key => $value) {
            $matches = array();
            if (preg_match("#^elasticsearch_(.*)#", $key, $matches)) {
                $data[$matches[1]] = $value;
            }
        }
        $servers = array();
        foreach (explode(',', $data['servers']) as $server) {
            $server = trim($server);
            if (substr($server, 0, 4) !== 'http') {
                $server = 'http://' . $server;
            }
            $info = parse_url($server);
            $host = $info['host'];
            $path = '/';
            if (isset($info['path'])) {
                $path = trim($info['path'], '/') . '/';
            }
            if (isset($info['user']) && isset($info['pass'])) {
                $host = $info['user'] . ':' . $info['pass'] . '@' . $host;
            }
            if (isset($info['port'])) {
                $port = $info['port'];
            } else {
                $port = ($info['scheme'] == 'https') ? 443 : 80;
            }
            $connection = array(
                'transport' => ucfirst($info['scheme']),
                'host' => $host,
                'port' => $port,
                'path' => $path,
            );
            if ($info['scheme'] == 'https' && !$data['verify_host']) {
                $connection['curl'] = array(
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                );
            }
            $servers[] = $connection;
        }
        $data['servers'] = $servers;

        $this->_config = $data;

        return $this->_config;
    }

    /**
     * @return int
     */
    public function getCacheLifetime()
    {
        return Mage::getStoreConfig('core/cache/lifetime');
    }

    /**
     * @param string $field
     * @param mixed $store
     * @return array
     */
    public function getSearchConfigData($field, $store = null)
    {
        $path = 'catalog/search/' . $field;

        return Mage::getStoreConfig($path, $store);
    }

    /**
     * Returns attribute type for indexation
     *
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return string
     */
    public function getAttributeType($attribute)
    {
        $type = 'string';
        if ($attribute->getBackendType() == 'decimal') {
            $type = 'double';
        } elseif ($attribute->getSourceModel() == 'eav/entity_attribute_source_boolean') {
            $type = 'boolean';
        } elseif ($attribute->getBackendType() == 'datetime') {
            $type = 'date';
        } elseif ($this->isAttributeUsingOptions($attribute)) {
            $type = 'option'; // custom type
        } elseif ($attribute->usesSource() && $attribute->getBackendType() == 'int'
            || $attribute->getFrontendClass() == 'validate-digits')
        {
            $type = 'integer';
        }

        return $type;
    }

    /**
     * Returns EAV config singleton
     *
     * @return Mage_Eav_Model_Config
     */
    public function getEavConfig()
    {
        return Mage::getSingleton('eav/config');
    }

    /**
     * Returns language code of specified locale code
     *
     * @param string $localeCode
     * @return bool
     */
    public function getLanguageCodeByLocaleCode($localeCode)
    {
        $localeCode = (string) $localeCode;
        if (!$localeCode) {
            return false;
        }

        if (!isset($this->_languageCodes[$localeCode])) {
            $languages = $this->getSupportedLanguages();
            $this->_languageCodes[$localeCode] = false;
            foreach ($languages as $code => $locales) {
                if (is_array($locales)) {
                    if (in_array($localeCode, $locales)) {
                        $this->_languageCodes[$localeCode] = $code;
                    }
                } elseif ($localeCode == $locales) {
                    $this->_languageCodes[$localeCode] = $code;
                }
            }
        }

        return $this->_languageCodes[$localeCode];
    }

    /**
     * Returns store language code
     *
     * @param mixed $store
     * @return bool
     */
    public function getLanguageCodeByStore($store = null)
    {
        return $this->getLanguageCodeByLocaleCode($this->getLocaleCode($store));
    }

    /**
     * Returns store locale code
     *
     * @param mixed $store
     * @return string
     */
    public function getLocaleCode($store = null)
    {
        return Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE, $store);
    }

    /**
     * Perform search directly on product collection based on a query string
     *
     * @param $queryText
     * @param null $store
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection($queryText, $store = null)
    {
        $store = Mage::app()->getStore($store);
        if ($this->isActiveEngine()) {
            $results = Mage::helper('catalogsearch')->getEngine()->search($queryText, $store);
            $productIds = array();
            foreach ($results as $result) {
                /** @var \Elastica\Result $result */
                $productIds[] = (int) $result->getId();
                if (isset($result->parent_ids)) {
                    $productIds = array_merge($productIds, $result->parent_ids);
                }
            }
        } else {
            $resource       = Mage::getResourceModel('catalogsearch/fulltext');
            $adapter        = $resource->getReadConnection();
            $searchType     = $store->getConfig(Mage_CatalogSearch_Model_Fulltext::XML_PATH_CATALOG_SEARCH_TYPE);
            $maxQueryWords  = Mage::helper('catalogsearch')->getMaxQueryWords($store);
            $preparedTerms  = Mage::getResourceHelper('catalogsearch')
                ->prepareTerms($queryText, $maxQueryWords);

            $bind = array();
            $like = array();
            $likeCond  = '';
            if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_LIKE
                || $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE
            ) {
                $helper = Mage::getResourceHelper('core');
                $words = Mage::helper('core/string')->splitWords($queryText, true, $maxQueryWords);
                foreach ($words as $word) {
                    $like[] = $helper->getCILike('s.data_index', $word, array('position' => 'any'));
                }
                if ($like) {
                    $likeCond = '(' . join(' OR ', $like) . ')';
                }
            }
            $mainTableAlias = 's';
            $select = $adapter->select()
                ->from(array($mainTableAlias => $resource->getMainTable()), 'product_id')
                ->joinInner(
                    array('e' => $resource->getTable('catalog/product')),
                    'e.entity_id = s.product_id',
                    array()
                )
                ->where($mainTableAlias.'.store_id = ?', (int) $store->getId());

            if ($searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_FULLTEXT
                || $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE
            ) {
                $bind[':query'] = implode(' ', $preparedTerms[0]);
                $where = Mage::getResourceHelper('catalogsearch')
                    ->chooseFulltext($resource->getMainTable(), $mainTableAlias, $select);
            }

            if ($likeCond != '' && $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_COMBINE) {
                $where .= ($where ? ' OR ' : '') . $likeCond;
            } elseif ($likeCond != '' && $searchType == Mage_CatalogSearch_Model_Fulltext::SEARCH_TYPE_LIKE) {
                $select->columns(array('relevance'  => new Zend_Db_Expr(0)));
                $where = $likeCond;
            }

            if ($where != '') {
                $select->where($where);
            }

            $productIds = $adapter->fetchCol($select, $bind);
        }

        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addIdFilter($productIds);

        if (!empty($productIds)) {
            $collection->getSelect()
                ->order(new Zend_Db_Expr('FIELD(e.entity_id, ' . implode(', ', $productIds) . ')'));
        }

        return $collection;
    }

    /**
     * Retrieves all searchable product attributes
     * Possibility to filter attributes by backend type
     *
     * @param array $backendType
     * @return array
     */
    public function getSearchableAttributes($backendType = null)
    {
        if (null === $this->_searchableAttributes) {
            $this->_searchableAttributes = array();
            $entityType = $this->getEavConfig()->getEntityType('catalog_product');
            $entity = $entityType->getEntity();

            /* @var $productAttributeCollection Mage_Catalog_Model_Resource_Product_Attribute_Collection */
            $productAttributeCollection = Mage::getResourceModel('catalog/product_attribute_collection')
                ->setEntityTypeFilter($entityType->getEntityTypeId())
                ->addVisibleFilter()
                ->addToIndexFilter(true);

            $attributes = $productAttributeCollection->getItems();
            foreach ($attributes as $attribute) {
                /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                $attribute->setEntity($entity);
                $this->_searchableAttributes[$attribute->getAttributeCode()] = $attribute;
            }
        }

        if (null !== $backendType) {
            $backendType = (array) $backendType;
            $attributes = array();
            foreach ($this->_searchableAttributes as $attribute) {
                /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                if (in_array($attribute->getBackendType(), $backendType)) {
                    $attributes[$attribute->getAttributeCode()] = $attribute;
                }
            }

            return $attributes;
        }

        return $this->_searchableAttributes;
    }

    /**
     * Defines supported languages for snowball filter
     *
     * @return array
     */
    public function getSupportedLanguages()
    {
        $default = array(
            /**
             * SnowBall filter based
             */
            // Danish
            'da' => 'da_DK',
            // Dutch
            'nl' => 'nl_NL',
            // English
            'en' => array('en_AU', 'en_CA', 'en_NZ', 'en_GB', 'en_US'),
            // Finnish
            'fi' => 'fi_FI',
            // French
            'fr' => array('fr_CA', 'fr_FR'),
            // German
            'de' => array('de_DE','de_CH','de_AT'),
            // Hungarian
            'hu' => 'hu_HU',
            // Italian
            'it' => array('it_IT','it_CH'),
            // Norwegian
            'nb' => array('nb_NO', 'nn_NO'),
            // Portuguese
            'pt' => array('pt_BR', 'pt_PT'),
            // Romanian
            'ro' => 'ro_RO',
            // Russian
            'ru' => 'ru_RU',
            // Spanish
            'es' => array('es_AR', 'es_CL', 'es_CO', 'es_CR', 'es_ES', 'es_MX', 'es_PA', 'es_PE', 'es_VE'),
            // Swedish
            'sv' => 'sv_SE',
            // Turkish
            'tr' => 'tr_TR',

            /**
             * Lucene class based
             */
            // Czech
            'cs' => 'cs_CZ',
            // Greek
            'el' => 'el_GR',
            // Thai
            'th' => 'th_TH',
            // Chinese
            'zh' => array('zh_CN', 'zh_HK', 'zh_TW'),
            // Japanese
            'ja' => 'ja_JP',
            // Korean
            'ko' => 'ko_KR'
        );

        return $default;
    }

    /**
     * Handles error
     *
     * @param string $error
     * @return $this
     */
    public function handleError($error)
    {
        if (!Mage::app()->getRequest()->isAjax()) {
            if (Mage::app()->getStore()->isAdmin()) {
                Mage::getSingleton('adminhtml/session')->addError($error);
            } elseif ($this->isDebugEnabled()) {
                echo Mage::app()->getLayout()
                    ->createBlock('core/messages')
                    ->addError($error)
                    ->getGroupedHtml();
            }
        }

        Mage::log($error, Zend_Log::CRIT, 'elasticsearch.log', true);

        return $this;
    }

    /**
     * Handles message
     *
     * @return $this
     */
    public function handleMessage()
    {
        $args = func_get_args();
        $msg = array_shift($args);
        $msg = @vsprintf($msg, $args);
        if (php_sapi_name() == 'cli') {
            echo @vsprintf($msg, $args) . "\n";
        } else {
            Mage::log($msg, Zend_Log::DEBUG, 'elasticsearch.log', true);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isActiveEngine()
    {
        $engine = $this->getSearchConfigData('engine');
        if ($engine && Mage::getConfig()->getResourceModelClassName($engine)) {
            $model = Mage::getResourceSingleton($engine);
            return $model
            && $model instanceof Bubble_Elasticsearch_Model_Resource_Engine
            && $model->test();
        }

        return false;
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return bool
     */
    public function isAttributeIndexable($attribute)
    {
        return ($attribute->getIsSearchable() || $attribute->getIsVisibleInAdvancedSearch())
        && !in_array($attribute->getAttributeCode(), array('status', 'tax_class_id', 'price'));
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @return bool
     */
    public function isAttributeUsingOptions($attribute)
    {
        $model = Mage::getModel($attribute->getSourceModel());
        $backend = $attribute->getBackendType();

        return $attribute->usesSource() &&
        ($backend == 'int' && $model instanceof Mage_Eav_Model_Entity_Attribute_Source_Table) ||
        ($backend == 'varchar' && $attribute->getFrontendInput() == 'multiselect');
    }

    /**
     * @return bool
     */
    public function isDebugEnabled()
    {
        $config = $this->getEngineConfigData();

        return array_key_exists('enable_debug_mode', $config) && $config['enable_debug_mode'];
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isIndexOutOfStockProducts($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_CatalogInventory_Helper_Data::XML_PATH_SHOW_OUT_OF_STOCK, $store);
    }

    /**
     * Prepares and escape query text
     *
     * @param $text
     * @return string
     */
    public function prepareQueryText($text)
    {
        $words = explode(' ', $text);
        $words = array_filter($words, 'strlen');
        foreach ($words as $key => &$val) {
            if (!empty($val)) {
                $val = $this->escape($val);
            }
        }
        $text = implode(' ', $words);

        return $text;
    }

    /**
     * @return Varien_Db_Adapter_Pdo_Mysql
     */
    protected function _getAdapter()
    {
        return $this->_getResource()->getConnection('read');
    }

    /**
     * @return Mage_Core_Model_Resource
     */
    protected function _getResource()
    {
        return Mage::getSingleton('core/resource');
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $attribute
     * @param string $value
     * @return mixed
     */
    protected function _formatValue($attribute, $value)
    {
        if ($attribute->getBackendType() == 'decimal') {
            if (strpos($value, ',')) {
                $value = array_unique(array_map('floatval', explode(',', $value)));
            } else {
                $value = (float) $value;
            }
        } elseif ($attribute->getSourceModel() == 'eav/entity_attribute_source_boolean'
            || $attribute->getFrontendInput() == 'boolean')
        {
            $value = (bool) $value;
        } elseif ($attribute->usesSource() || $attribute->getFrontendClass() == 'validate-digits') {
            if (strpos($value, ',')) {
                $value = array_unique(array_map('intval', explode(',', $value)));
            } else {
                $value = (int) $value;
            }
        }

        return $value;
    }
}