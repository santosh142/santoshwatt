<?php
/**
 * Query default operator configuration
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_System_Config_Source_Query_Operator
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'OR', 'label' => 'OR'),
            array('value' => 'AND', 'label' => 'AND'),
        );
    }
}