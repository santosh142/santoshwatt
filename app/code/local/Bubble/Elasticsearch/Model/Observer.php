<?php
/**
 * Elasticsearch observer
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Observer
{
    /**
     * Adds search weight parameter in attribute form
     *
     * @param Varien_Event_Observer $observer
     */
    public function onEavAttributeEditFormInit(Varien_Event_Observer $observer)
    {
        /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
        $attribute = $observer->getEvent()->getAttribute();
        /** @var Varien_Data_Form $form */
        $form = $observer->getEvent()->getForm();
        $fieldset = $form->addFieldset('elasticsearch_fieldset', array(
            'legend'    => Mage::helper('elasticsearch')->__('Elasticsearch Settings')
        ));

        $fieldset->addField('search_weight', 'select', array(
            'name' => 'search_weight',
            'label' => Mage::helper('elasticsearch')->__('Search Weight'),
            'note' => Mage::helper('elasticsearch')->__('Boost some attributes by giving them a higher weight.'),
            'values' => array(
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 6,
                7 => 7,
                8 => 8,
                9 => 9,
                10 => 10,
            ),
        ), 'is_searchable');

        if ($attribute->getAttributeCode() == 'name') {
            $form->getElement('is_searchable')->setDisabled(1);
        }
    }
}
