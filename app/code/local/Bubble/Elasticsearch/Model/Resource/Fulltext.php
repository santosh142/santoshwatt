<?php
/**
 * Catalog search fulltext override
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Resource_Fulltext extends Mage_CatalogSearch_Model_Resource_Fulltext
{
    /**
     * Regenerate search index for specific store
     *
     * @param int $storeId Store View Id
     * @param int|array $productIds Product Entity Id
     * @return Mage_CatalogSearch_Model_Resource_Fulltext
     */
    protected function _rebuildStoreIndex($storeId, $productIds = null)
    {
        if (!Mage::helper('elasticsearch')->isActiveEngine() || !$this->_engine) {
            return parent::_rebuildStoreIndex($storeId, $productIds);
        }

        $this->_engine->rebuildIndex($storeId, $productIds);

        $this->resetSearchResults();

        return $this;
    }
}
