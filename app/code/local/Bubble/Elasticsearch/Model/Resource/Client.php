<?php
// Add custom autoload logic since Elactica library uses namespaces
spl_autoload_register(function($class) {
    if (false !== strpos($class, 'Elastica\\')) {
        $class = trim($class, '\\');
        $classFile = str_replace(' ', DIRECTORY_SEPARATOR, ucwords(str_replace('\\', ' ', $class)));
        $classFile .= '.php';
        @include $classFile;
    }
});

/**
 * Elasticsearch client
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Resource_Client extends \Elastica\Client
{
    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * @var bool Saves search engine availability
     */
    protected $_test;

    /**
     * @var string Date format
     * @link http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/mapping-date-format.html
     */
    protected $_dateFormat = 'date';

    /**
     * @var array Stop languages for token filter
     * @link http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/analysis-stop-tokenfilter.html
     */
    protected $_stopLanguages = array(
        'arabic', 'armenian', 'basque', 'brazilian', 'bulgarian', 'catalan', 'czech', 'danish', 'dutch', 'english',
        'finnish', 'french', 'galician', 'german', 'greek', 'hindi', 'hungarian', 'indonesian', 'irish', 'italian',
        'latvian', 'norwegian', 'persian', 'portuguese', 'romanian', 'russian', 'sorani', 'spanish', 'swedish',
        'thai', 'turkish',
    );

    /**
     * @var array Snowball languages
     * @link http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/analysis-snowball-tokenfilter.html
     */
    protected $_snowballLanguages = array(
        'Armenian', 'Basque', 'Catalan', 'Danish', 'Dutch', 'English', 'Finnish', 'French',
        'German', 'Hungarian', 'Italian', 'Kp', 'Lovins', 'Norwegian', 'Porter', 'Portuguese',
        'Romanian', 'Russian', 'Spanish', 'Swedish', 'Turkish',
    );

    /**
     * Initializes search engine configuration
     */
    public function __construct()
    {
        $this->_helper = Mage::helper('elasticsearch');
        $config = $this->_helper->getEngineConfigData();
        if (!isset($config['connection_strategy'])) {
            $config['connectionStrategy'] = '\\Elastica\\Connection\\Strategy\\Simple';
        }
        parent::__construct($config);
    }

    /**
     * Cleans store index
     *
     * @param mixed $store
     * @param int $ids
     * @param string $type
     * @return Bubble_Elasticsearch_Model_Resource_Client
     */
    public function cleanStoreIndex($store = null, $ids = null, $type = 'product')
    {
        $index = $this->getStoreIndexName($store);
        if (empty($ids)) {
            // Delete ALL docs from specific index
            $this->getIndex($index)->getType($type)->delete();
        } else {
            // Delete matching ids from specific index
            $this->deleteIds((array) $ids, $index, $type);
        }

        return $this;
    }

    /**
     * Creates document in a clean format
     *
     * @param string $index
     * @param string $id
     * @param array $data
     * @param string $type
     * @return \Elastica\Document
     */
    public function createDoc($index, $id = '', array $data = array(), $type = 'product')
    {
        return new \Elastica\Document($id, $data, $type, $index);
    }

    /**
     * Returns query operator
     *
     * @return string
     */
    public function getQueryOperator()
    {
        return $this->getConfig('query_operator');
    }

    /**
     * @param $store
     * @param null $ids
     * @param string $type
     * @return mixed
     */
    public function getStoreData($store = null, $ids = null, $type = 'product')
    {
        $store = Mage::app()->getStore($store);
        $filters = array('store_id' => $store->getId());
        if (!empty($ids)) {
            $filters['entity_id'] = array_unique($ids);
        }
        $data = Mage::helper('elasticsearch/' . $type)->export($filters);

        return $data[$store->getId()];
    }

    /**
     * @param mixed $store
     * @param bool $new
     * @return \Elastica\Index
     * @throws Exception
     */
    public function getStoreIndex($store = null, $new = false)
    {
        if (!$this->isSafeReindexEnabled()) {
            $new = false; // reindex on current index and not on a temporary one
        }
        $name = $this->getStoreIndexName($store, $new);
        $index = $this->getIndex($name);
        if ($new && $this->indexExists($name)) {
            $index->delete(); // delete index if exists because we are indexing all documents
        }
        if (!$this->indexExists($name)) {
            $index->create(array('settings' => $this->getStoreIndexSettings($store)));

            $mapping = new \Elastica\Type\Mapping();
            $mapping->setType($index->getType('product'));
            if (!$this->isSourceEnabled()) {
                $mapping->disableSource();
            }
            $mapping->enableAllField(false); // not needed since we specify search fields manually
            $mapping->setProperties($this->getStoreIndexProperties($store));

            Mage::dispatchEvent('bubble_elasticsearch_mapping_send_before', array(
                'client' => $this,
                'store' => $store,
                'mapping' => $mapping,
            ));

            $mapping->send();
        }

        return $index;
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getStoreIndexAlias($store)
    {
        $prefix = $this->getConfig('index_prefix');

        return $prefix . Mage::app()->getStore($store)->getCode();
    }

    /**
     * @param mixed $store
     * @param bool $new
     * @return string
     */
    public function getStoreIndexName($store, $new = false)
    {
        $alias = $this->getStoreIndexAlias($store);
        $name = $alias . '_idx1'; // index name must be different than alias name
        foreach ($this->getStatus()->getIndicesWithAlias($alias) as $indice) {
            if ($new) {
                $name = $indice->getName() != $name ? $name : $alias . '_idx2';
            } else {
                $name = $indice->getName();
            }
        }

        return $name;
    }

    /**
     * Retrieves store search fields of specific analyzer if specified
     *
     * @param mixed $store
     * @param mixed $analyzer
     * @param bool $withBoost
     * @return array
     */
    public function getStoreSearchFields($store = null, $analyzer = false, $withBoost = true)
    {
        $properties = $this->getStoreIndexProperties($store);
        $fields = array();
        foreach ($properties as $fieldName => $property) {
            // If field is not searchable, ignore it
            if (!isset($property['searchable']) || !$property['searchable']) {
                continue;
            }

            $weight = 1;
            if ($withBoost && isset($property['weight'])) {
                $weight = intval($property['weight']);
            }

            if (!$analyzer || (isset($property['analyzer']) && $property['analyzer'] == $analyzer)) {
                $fields[] = $fieldName . ($weight > 1 ? '^' . $weight : '');
            }

            if (isset($property['fields'])) {
                foreach ($property['fields'] as $key => $field) {
                    if (!$analyzer || (isset($field['analyzer']) && $field['analyzer'] == $analyzer)) {
                        $fields[] = $fieldName . '.' . $key . ($weight > 1 ? '^' . $weight : '');
                    }
                }
            }
        }

        $fields = new Varien_Object($fields);
        Mage::dispatchEvent('bubble_elasticsearch_search_fields', array(
            'client' => $this,
            'store' => Mage::app()->getStore($store),
            'analyzer' => $analyzer,
            'fields' => $fields,
        ));

        return $fields->getData();
    }

    /**
     * Builds store index properties for indexation according to available attributes
     *
     * @param mixed $store
     * @return array
     */
    public function getStoreIndexProperties($store = null)
    {
        $store = Mage::app()->getStore($store);
        $cacheId = 'elasticsearch_index_properties_' . $store->getId();
        if (Mage::app()->useCache('config')) {
            $properties = Mage::app()->loadCache($cacheId);
            if ($properties) {
                return unserialize($properties);
            }
        }

        $helper = $this->_helper;
        $indexSettings = $this->getStoreIndexSettings($store);
        $properties = array();

        $attributes = $helper->getSearchableAttributes(array('varchar', 'int'));
        foreach ($attributes as $attribute) {
            /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            if ($helper->isAttributeIndexable($attribute)) {
                $key = $attribute->getAttributeCode();
                $type = $helper->getAttributeType($attribute);
                $weight = $attribute->getSearchWeight();

                if ($type === 'option') {
                    // Define field for option label
                    $properties[$key] = array(
                        'type' => 'string',
                        'analyzer' => 'std',
                        'index_options' => 'docs', // do not use tf/idf for options
                        'norms' => array('enabled' => false), // useless for options
                        'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                        'weight' => $weight > 0 ? intval($weight) : 1, // boost at query time
                        'fields' => array(
                            'std' => array(
                                'type' => 'string',
                                'analyzer' => 'std',
                                'index_options' => 'docs', // do not use tf/idf for options
                                'norms' => array('enabled' => false), // useless for options
                            ),
                        ),
                    );
                    if (isset($indexSettings['analysis']['analyzer']['language'])) {
                        $properties[$key]['analyzer'] = 'language';
                    }
                } elseif ($type !== 'string') {
                    $properties[$key] = array(
                        'type' => $type,
                        'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                        'search_analyzer' => 'std',
                    );
                    if ($type === 'integer') {
                        $properties[$key]['ignore_malformed'] = true;
                    }
                } else {
                    $properties[$key] = array(
                        'type' => 'string',
                        'analyzer' => 'std',
                        'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                        'weight' => $weight > 0 ? intval($weight) : 1, // boost at query time
                        'fields' => array(
                            'std' => array(
                                'type' => 'string',
                                'analyzer' => 'std',
                            ),
                            'prefix' => array(
                                'type' => 'string',
                                'analyzer' => 'text_prefix',
                                'search_analyzer' => 'std',
                            ),
                            'suffix' => array(
                                'type' => 'string',
                                'analyzer' => 'text_suffix',
                                'search_analyzer' => 'std',
                            ),
                        ),
                    );
                    if (isset($indexSettings['analysis']['analyzer']['language'])) {
                        $properties[$key]['analyzer'] = 'language';
                    }
                }
            }
        }

        $attributes = $helper->getSearchableAttributes('text');
        foreach ($attributes as $attribute) {
            /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            $key = $attribute->getAttributeCode();
            $weight = $attribute->getSearchWeight();
            $properties[$key] = array(
                'type' => 'string',
                'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                'weight' => $weight > 0 ? intval($weight) : 1, // boost at query time
                'fields' => array(
                    'std' => array(
                        'type' => 'string',
                        'analyzer' => 'std',
                    ),
                ),
            );
            if (isset($indexSettings['analysis']['analyzer']['language'])) {
                $properties[$key]['analyzer'] = 'language';
            }
        }

        $attributes = $helper->getSearchableAttributes(array('static', 'varchar', 'decimal', 'datetime'));
        foreach ($attributes as $attribute) {
            /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            $key = $attribute->getAttributeCode();
            if ($helper->isAttributeIndexable($attribute) && !isset($properties[$key])) {
                $type = $helper->getAttributeType($attribute);
                if ($type === 'option') {
                    continue;
                }
                $weight = $attribute->getSearchWeight();
                $properties[$key] = array(
                    'type' => $type,
                    'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                    'weight' => $weight > 0 ? intval($weight) : 1, // boost at query time
                );
                if ($key == 'sku') {
                    $properties[$key]['fields'] = array(
                        'keyword' => array(
                            'type' => 'string',
                            'analyzer' => 'keyword',
                        ),
                        'prefix' => array(
                            'type' => 'string',
                            'analyzer' => 'keyword_prefix',
                            'search_analyzer' => 'keyword',
                        ),
                    );
                }
                if ($attribute->getBackendType() == 'datetime') {
                    $properties[$key]['format'] = $this->_dateFormat;
                }
            }
        }

        // Add categories field
        $properties['categories'] = array(
            'type' => 'string',
            'searchable' => true,
            'analyzer' => 'std',
            'position_offset_gap' => 100, // separate each phrase positions by 100
        );
        if (isset($indexSettings['analysis']['analyzer']['language'])) {
            $properties['categories']['analyzer'] = 'language';
        }

        // Add parent_ids field
        $properties['parent_ids'] = array(
            'type' => 'integer',
            'store' => true,
            'index' => 'no',
        );

        if (Mage::app()->useCache('config')) {
            $lifetime = $helper->getCacheLifetime();
            Mage::app()->saveCache(serialize($properties), $cacheId, array('config'), $lifetime);
        }

        return $properties;
    }

    /**
     * Returns indexation analyzers and filters configuration
     *
     * @param mixed $store
     * @return array
     */
    public function getStoreIndexSettings($store = null)
    {
        $store = Mage::app()->getStore($store);
        $indexSettings = array();
        $indexSettings['number_of_replicas'] = (int) $this->getConfig('number_of_replicas');
        $indexSettings['number_of_shards'] = (int) $this->getConfig('number_of_shards');
        $indexSettings['analysis']['analyzer'] = array(
            'std' => array( // Will allow query 'shoes' to match better than 'shoe' which the stemmed version
                'tokenizer' => 'standard',
                'char_filter' => 'html_strip', // strip html tags
                'filter' => array('standard', 'elision', 'asciifolding', 'lowercase', 'stop', 'length'),
            ),
            'keyword' => array(
                'tokenizer' => 'keyword',
                'filter' => array('asciifolding', 'lowercase'),
            ),
            'keyword_prefix' => array(
                'tokenizer' => 'keyword',
                'filter' => array('asciifolding', 'lowercase', 'edge_ngram_front'),
            ),
            'text_prefix' => array(
                'tokenizer' => 'standard',
                'char_filter' => 'html_strip', // strip html tags
                'filter' => array('standard', 'elision', 'asciifolding', 'lowercase', 'stop', 'edge_ngram_front'),
            ),
            'text_suffix' => array(
                'tokenizer' => 'standard',
                'char_filter' => 'html_strip', // strip html tags
                'filter' => array('standard', 'elision', 'asciifolding', 'lowercase', 'stop', 'edge_ngram_back'),
            ),
        );
        $indexSettings['analysis']['filter'] = array(
            'edge_ngram_front' => array(
                'type' => 'edgeNGram',
                'min_gram' => 3,
                'max_gram' => 10,
                'side' => 'front',
            ),
            'edge_ngram_back' => array(
                'type' => 'edgeNGram',
                'min_gram' => 3,
                'max_gram' => 10,
                'side' => 'back',
            ),
            'stop' => array(
                'type' => 'stop',
                'stopwords' => '_none_',
            ),
            'length' => array(
                'type' => 'length',
                'min' => 2,
            ),
        );
        /** @var $store Mage_Core_Model_Store */
        $languageCode = $this->_helper->getLanguageCodeByStore($store);
        $language = Zend_Locale_Data::getContent('en_GB', 'language', $languageCode);
        $languageExists = true;
        if (!in_array($language, $this->_snowballLanguages)) {
            $parts = explode(' ', $language); // try with potential first string
            $language = $parts[0];
            if (!in_array($language, $this->_snowballLanguages)) {
                $languageExists = false; // language not present by default in elasticsearch
            }
        }
        if ($languageExists) {
            if ($language == 'English') {
                $stemmer = 'kstem'; // less agressive than snowball
            } else {
                // Define snowball filter according to current language
                $stemmer = 'snowball';
                $indexSettings['analysis']['filter'][$stemmer] = array(
                    'type' => 'snowball',
                    'language' => $language,
                );
            }

            // Define a custom analyzer adapted to the store language
            $indexSettings['analysis']['analyzer']['language'] = array(
                'type' => 'custom',
                'tokenizer' => 'standard',
                'char_filter' => 'html_strip', // strip html tags
                'filter' => array(
                    'standard', 'elision', 'asciifolding', 'lowercase', 'stop', $stemmer, 'length',
                ),
            );

            // Define stop words filter according to current language if possible
            $stopwords = strtolower($language);
            if (in_array($stopwords, $this->_stopLanguages)) {
                $indexSettings['analysis']['filter']['stop']['stopwords'] = '_' . $stopwords . '_';
            }
        }

        $indexSettings = new Varien_Object($indexSettings);

        Mage::dispatchEvent('bubble_elasticsearch_index_settings', array(
            'client' => $this,
            'store' => $store,
            'settings' => $indexSettings,
        ));

        return $indexSettings->getData();
    }

    /**
     * Retrieve suggest fields
     *
     * @return array
     */
    public function getSuggestFields()
    {
        $fields = new Varien_Object(array('name.std'));

        Mage::dispatchEvent('bubble_elasticsearch_suggest_fields', array(
            'client' => $this,
            'fields' => $fields,
        ));

        return $fields->getData();
    }

    /**
     * Checks if given index already exists
     * Here because of a bug when calling exists() method directly on index object during index process
     *
     * @param mixed $index
     * @return bool
     */
    public function indexExists($index)
    {
        return $this->getStatus()->indexExists($index);
    }

    /**
     * Checks if fuzzy query is enabled
     *
     * @link http://www.elasticsearch.org/guide/reference/query-dsl/flt-query.html
     * @return bool
     */
    public function isFuzzyQueryEnabled()
    {
        return (bool) $this->getConfig('enable_fuzzy_query');
    }

    /**
     * Checks if we reindex in a temporary index or not
     *
     * @return bool
     */
    public function isSafeReindexEnabled()
    {
        return (bool) $this->getConfig('safe_reindex');
    }

    /**
     * Checks if data should be either stored or not
     *
     * @return bool
     */
    public function isSourceEnabled()
    {
        return (bool) $this->getConfig('enable_source');
    }

    /**
     * Checks if suggestion is enabled
     *
     * @link http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/search-suggesters-phrase.html
     * @return bool
     */
    public function isSuggestEnabled()
    {
        return (bool) $this->getConfig('enable_suggest');
    }

    /**
     * Saves entities to specified index
     *
     * @param $index
     * @param $data
     * @param string $type
     * @return $this
     */
    public function saveEntities($index, $data, $type = 'product')
    {
        if (is_string($index)) {
            $index = $this->getIndex($index);
        }
        $chunks = array_chunk($data, 100);
        foreach ($chunks as $docs) {
            foreach ($docs as $k => $doc) {
                $docs[$k] = $this->createDoc($index, $doc['id'], $doc, $type);
            }
            try {
                $this->addDocuments($docs);
            } catch (Exception $e) {
                $this->_helper->handleError($e->getMessage());
                $this->_helper->handleError(print_r($docs, true));
            }
        }
        $index->refresh();

        return $this;
    }

    /**
     * Saves store entities
     *
     * @param $store
     * @param $ids
     * @param $type
     * @return Bubble_Elasticsearch_Model_Resource_Client
     */
    public function saveStoreEntities($store = null, $ids = null, $type = 'product')
    {
        $data = $this->getStoreData($store, $ids, $type);
        $new = empty($ids); // create a new index if full reindexation
        $index = $this->getStoreIndex($store, $new);
        $this->saveEntities($index, $data, $type);

        if ($new) {
            $this->switchStoreIndex($index, $store);
        }

        return $this;
    }

    /**
     * @param $q
     * @param $store
     * @param $params
     * @param $type
     * @return array|\Elastica\ResultSet
     */
    public function search($q, $store = null, $params = array(), $type = 'product')
    {
        $index = $this->getStoreIndex($store);

        Varien_Profiler::start('ELASTICA_SEARCH');

        if (empty($params)) {
            $params = array('limit' => 10000); // should be enough
        }

        $q = $this->_helper->prepareQueryText($q);

        $bool = new \Elastica\Query\Bool();

        /**
         * Using cross-fields because it seems the best approach for entity search (products).
         * Cross-fields multi-match query has to work on fields that have the same analyzer.
         * So we build such a query for each configured analyzers.
         *
         * @link http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/_cross_fields_entity_search.html
         */
        $analyzers = array('language', 'std', 'text_prefix', 'text_suffix', 'keyword', 'keyword_prefix');
        foreach ($analyzers as $analyzer) {
            $fields = $this->getStoreSearchFields($store, $analyzer);
            if (!empty($fields)) {
                $query = new \Elastica\Query\MultiMatch();
                $query->setQuery($q);
                $query->setType('cross_fields');
                $query->setFields($fields);
                $query->setOperator($this->getQueryOperator());
                $query->setTieBreaker(.1);
                $bool->addShould($query);
            }
        }

        if ($this->isFuzzyQueryEnabled()) {
            // Standard search fields + fuzziness
            $fields = $this->getStoreSearchFields($store, 'std');
            if (!empty($fields)) {
                $query = new \Elastica\Query\MultiMatch();
                $query->setQuery($q);
                $query->setUseDisMax(false); // use a boolean query in order to use fuzziness
                $query->setFields($fields);
                $query->setOperator($this->getQueryOperator());
                $query->setParam('fuzziness', 'auto'); // generates an edit distance based on the length of the term
                $bool->addShould($query);
            }
        }

        $search = $index->getType($type)->createSearch($bool, $params);
        $search->getQuery()->setFields(array('parent_ids')); // retrieve only id (implicitly included) and parent ids

        if ($this->isSuggestEnabled()) {
            $suggest = new \Elastica\Suggest();
            $fields = $this->getStoreSearchFields($store, 'std', false);
            foreach ($this->getSuggestFields() as $field) {
                if (in_array($field, $fields)) {
                    $suggestField = new \Elastica\Suggest\Phrase($field, $field);
                    $suggestField->setText($q);
                    $suggestField->setGramSize(1);
                    $suggestField->setMaxErrors(.9);
                    $candidate = new \Elastica\Suggest\CandidateGenerator\DirectGenerator($field);
                    $candidate->setParam('min_word_length', 3);
                    $suggestField->addCandidateGenerator($candidate);
                    $suggest->addSuggestion($suggestField);
                    $search->getQuery()->setSuggest($suggest);
                }
            }
        }

        Mage::dispatchEvent('bubble_elasticsearch_before_search', array(
            'client' => $this,
            'search' => $search,
        ));

        $result = $search->search();

        Varien_Profiler::stop('ELASTICA_SEARCH');

        return $result;
    }

    /**
     * Switch index of specified store by linking alias on it
     *
     * @param mixed $index
     * @param mixed $store
     * @return Bubble_Elasticsearch_Model_Resource_Client
     */
    public function switchStoreIndex($index, $store = null)
    {
        if (is_string($index)) {
            $index = $this->getIndex($index);
        }
        $alias = $this->getStoreIndexAlias($store);
        foreach ($this->getStatus()->getIndicesWithAlias($alias) as $indice) {
            if ($indice->getName() != $index->getName()) {
                $indice->delete(); // remove old indice that was linked to the alias
            }
        }
        $index->addAlias($alias, true);

        return $this;
    }

    /**
     * Test if Elasticsearch server is reachable
     *
     * @return bool
     */
    public function test()
    {
        if (null === $this->_test) {
            try {
                $this->getStatus();
                $this->_test = true;
            } catch (Exception $e) {
                $this->_test = false;
                $this->_helper->handleError('Elasticsearch server is not reachable');
            }
        }

        return $this->_test;
    }
}