<?php
/**
 * Fulltext collection override
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Resource_Fulltext_Collection
    extends Mage_CatalogSearch_Model_Resource_Fulltext_Collection
{
    /**
     * @var bool
     */
    protected $_active = false;

    /**
     * @var array Ids retrieved from Elasticsearch and matching query
     */
    protected $_ids;

    protected function _construct()
    {
        parent::_construct();
        $this->_active = Mage::helper('elasticsearch')->isActiveEngine();
    }

    /**
     * @param string $query
     * @return Bubble_Elasticsearch_Model_Resource_Fulltext_Collection
     */
    public function addSearchFilter($query)
    {
        if (!$this->_active) {
            parent::addSearchFilter($query);
        } else {
            if (null === $this->_ids) {
                Mage::getSingleton('catalogsearch/fulltext')->prepareResult();
                /** @var Bubble_Elasticsearch_Model_Resource_Engine $engine */
                $engine = Mage::helper('catalogsearch')->getEngine();
                $search = $engine->search($query);

                $this->_ids = array();
                foreach ($search->getResults() as $result) {
                    /** @var \Elastica\Result $result */
                    $this->_ids[] = (int) $result->getId();
                    if (isset($result->parent_ids)) {
                        $this->_ids = array_merge($this->_ids, $result->parent_ids);
                    }
                }
                $this->_ids = array_unique($this->_ids);

                $suggests = array();
                foreach ($search->getSuggests() as $suggestions) {
                    foreach ($suggestions as $suggestion) {
                        if (isset($suggestion['options']) && !empty($suggestion['options'])) {
                            foreach ($suggestion['options'] as $phrase) {
                                $text = $phrase['text'];
                                $score = $phrase['score'];
                                if ($score < .01) {
                                    continue;
                                }
                                if (!isset($suggests[$text])) {
                                    $suggests[$text] = 0;
                                }
                                if ($score > $suggests[$text]) {
                                    $suggests[$text] = $score;
                                }
                            }
                        }
                    }
                }
                if (!empty($suggests)) {
                    arsort($suggests); // retrieve the best score
                    $this->setFlag('suggest', key($suggests));
                }
            }
            if (empty($this->_ids)) {
                $this->addIdFilter(array(0)); // Workaround for no result
            } else {
                $this->addIdFilter($this->_ids);
            }
        }

        return $this;
    }

    /**
     * @param string $attribute
     * @param string $dir
     * @return Bubble_Elasticsearch_Model_Resource_Fulltext_Collection
     */
    public function setOrder($attribute, $dir = 'desc')
    {
        if ($this->_active && $attribute == 'relevance') {
            if (!empty($this->_ids)) {
                if ($dir == 'asc') {
                    $this->_ids = array_reverse($this->_ids);
                }
                $this->getSelect()
                    ->order(new Zend_Db_Expr('FIELD(e.entity_id, ' . implode(', ', $this->_ids) . ')'));
            }
        } else {
            parent::setOrder($attribute, $dir);
        }

        return $this;
    }
}
