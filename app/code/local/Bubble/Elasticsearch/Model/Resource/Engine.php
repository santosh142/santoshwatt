<?php
/**
 * Elasticsearch engine
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Resource_Engine
{
    /**
     * @var Bubble_Elasticsearch_Model_Resource_Client
     */
    protected $_client;

    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * Cache search results per request
     *
     * @var array
     */
    protected $_cachedData = array();

    /**
     * Initializes search engine
     *
     * @see Bubble_Elasticsearch_Model_Resource_Client
     */
    public function __construct()
    {
        $this->_helper = Mage::helper('elasticsearch');
        $this->_client = Mage::getResourceSingleton('elasticsearch/client');
    }

    /**
     * @return bool
     */
    public function allowAdvancedIndex()
    {
        return true;
    }

    /**
     * Cleans Elasticsearch index
     *
     * @param int $storeId
     * @param int $ids
     * @param string $type
     * @return Bubble_Elasticsearch_Model_Resource_Engine
     * @throws Exception
     */
    public function cleanIndex($storeId = null, $ids = null, $type = 'product')
    {
        if (null !== $storeId) {
            $stores = array(Mage::app()->getStore($storeId));
        } else {
            $stores = Mage::app()->getStores();
        }

        try {
            foreach ($stores as $store) {
                /** @var $store Mage_Core_Model_Store */
                $this->_client->cleanStoreIndex($store, $ids, $type);
            }
        } catch (Exception $e) {
            $this->_helper->handleError($e->getMessage());
            throw $e;
        }

        return $this;
    }

    /**
     * Returns advanced search results
     *
     * @return Mage_CatalogSearch_Model_Resource_Advanced_Collection
     */
    public function getAdvancedResultCollection()
    {
        return Mage::getResourceModel('catalogsearch/advanced_collection');
    }

    /**
     * Stub method for compatibility with other search engines
     *
     * @return null
     */
    public function getResourceName()
    {
        return null;
    }

    /**
     * Checks if layered navigation is available for current search engine
     *
     * @return bool
     */
    public function isLayeredNavigationAllowed()
    {
        return true;
    }

    /**
     * Alias of isLayeredNavigationAllowed()
     *
     * @return bool
     */
    public function isLeyeredNavigationAllowed()
    {
        return $this->isLayeredNavigationAllowed();
    }

    /**
     * @param null $storeId
     * @param null $ids
     * @param string $type
     * @return Bubble_Elasticsearch_Model_Resource_Engine
     * @throws Exception
     */
    public function rebuildIndex($storeId = null, $ids = null, $type = 'product')
    {
        if (null !== $storeId) {
            $stores = array(Mage::app()->getStore($storeId));
        } else {
            $stores = Mage::app()->getStores();
        }

        try {
            foreach ($stores as $store) {
                /** @var $store Mage_Core_Model_Store */
                if (!$store->getIsActive()) {
                    continue;
                }
                $start = microtime(true);
                $this->_helper->handleMessage('Start building Elasticsearch index for store %s', $store->getCode());
                $this->_client->saveStoreEntities($store, $ids, $type);
                $this->_helper->handleMessage('Done in %ds', round(microtime(true) - $start));
            }
        } catch (Exception $e) {
            $this->_helper->handleError($e->getMessage());
            throw $e;
        }

        return $this;
    }

    /**
     * @param $q
     * @param $store
     * @param $params
     * @param $type
     * @return array|\Elastica\ResultSet
     * @throws Exception
     */
    public function search($q, $store = null, $params = array(), $type = 'product')
    {
        $result = array();

        try {
            $cacheId = sha1(serialize(func_get_args()));
            if (isset($this->_cachedData[$cacheId])) {
                $result = $this->_cachedData[$cacheId];
            } else {
                $store = Mage::app()->getStore($store);
                $result = $this->_client->search($q, $store, $params, $type);
                $this->_cachedData[$cacheId] = $result;
            }
        } catch (Exception $e) {
            $this->_helper->handleError($e->getMessage());
        }

        return $result;
    }

    /**
     * Checks Elasticsearch availability.
     *
     * @return bool
     */
    public function test()
    {
        return $this->_client->test();
    }
}
