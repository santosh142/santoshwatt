<?php
/**
 * Search indexer override
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Indexer_Fulltext extends Mage_CatalogSearch_Model_Indexer_Fulltext
{
    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * @var Bubble_Elasticsearch_Model_Resource_Engine
     */
    protected $_engine;

    /**
     * Indexer constructor
     */
    public function __construct()
    {
        $this->_helper = Mage::helper('elasticsearch');
        $this->_engine = Mage::helper('catalogsearch')->getEngine();
    }

    /**
     * @return bool
     */
    public function isActiveEngine()
    {
        return $this->_helper->isActiveEngine();
    }

    /**
     * Rebuild all index data
     *
     * @return void
     */
    public function reindexAll()
    {
        if (!$this->isActiveEngine()) {
            parent::reindexAll();
        } else {
            try {
                $this->_engine->rebuildIndex();
            } catch (\Elastica\Exception\ResponseException $e) {
                $this->_helper->handleError($e->getMessage());
                throw $e;
            }
        }
    }
}