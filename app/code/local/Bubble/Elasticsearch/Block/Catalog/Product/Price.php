<?php
/**
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Block_Catalog_Product_Price extends Mage_Catalog_Block_Product_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->addPriceBlockType('bundle', 'bundle/catalog_product_price', 'bundle/catalog/product/price.phtml');
    }

    public function setUseLinkForAsLowAs($bool = true)
    {
        $this->_useLinkForAsLowAs = $bool;

        return $this;
    }
}