<?php
/**
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     2.0.10
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Block_Catalogsearch_Autocomplete extends Mage_Core_Block_Template
{
    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $_products;

    /**
     * @var Mage_Core_Block_Template
     */
    protected $_priceBlock;

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('bubble/elasticsearch/autocomplete.phtml');
    }

    protected function _prepareLayout()
    {
        $this->setChild(
            'price_template',
            $this->getLayout()->createBlock('catalog/product_price_template', 'catalog_product_price_template')
        );

        return parent::_prepareLayout();
    }

    protected function _toHtml()
    {
        if (!Mage::getStoreConfigFlag('elasticsearch/autocomplete/enable')) {
            $block = new Mage_CatalogSearch_Block_Autocomplete();

            return $block->toHtml();
        }

        return parent::_toHtml();
    }

    public function getProductCollection()
    {
        if (!$this->_products) {
            $collection = $this->helper('elasticsearch')->getProductCollection($this->getQueryText());

            $collection->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->setStore($this->getStore())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addStoreFilter()
                ->addUrlRewrite();

            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInSearchFilterToCollection($collection);

            if ($limit = $this->getLimit()) {
                $collection->getSelect()->limit($limit);
            }

            $this->_products = $collection;
        }

        return $this->_products;
    }

    public function getLimit()
    {
        return Mage::getStoreConfig('elasticsearch/autocomplete/limit', $this->getStore());
    }

    public function getStore()
    {
        return Mage::app()->getStore();
    }

    public function getQueryText()
    {
        return $this->helper('catalogsearch')->getQueryText();
    }

    public function getResultUrl()
    {
        return $this->helper('catalogsearch')->getResultUrl($this->getQueryText());
    }

    public function shouldDisplayPrice()
    {
        return Mage::getStoreConfigFlag('elasticsearch/autocomplete/display_price', $this->getStore());
    }

    public function getPriceHtml(Mage_Catalog_Model_Product $product)
    {
        return $this->getPriceBlock()
            ->setUseLinkForAsLowAs(false)
            ->getPriceHtml($product, true);
    }

    public function getPriceBlock()
    {
        if (null === $this->_priceBlock) {
            $this->_priceBlock = $this->getLayout()->createBlock('elasticsearch/catalog_product_price');
        }

        return $this->_priceBlock;
    }

    public function shouldDisplayImage()
    {
        return Mage::getStoreConfigFlag('elasticsearch/autocomplete/display_image', $this->getStore());
    }

    public function getImageSize()
    {
        return (int) Mage::getStoreConfig('elasticsearch/autocomplete/image_size', $this->getStore());
    }
}