<?php

class WP_Amazonimportproductsaddon_Model_Observers
{
    public function attributesMappingAfter(Varien_Event_Observer $observer)
    {
        /*
        $product                = $observer->getData('product');
        $apiResponseItemData    = $observer->getData('apiResponseItemData');
        $processingType         = $observer->getData('processingType');
        */

        /**
        * $processingType possible values:
        * - WP_Amazonimportproducts_Model_Observer::PRODUCT_PROCESSING_TYPE_CREATE
        * - WP_Amazonimportproducts_Model_Observer::PRODUCT_PROCESSING_TYPE_UPDATE
        *
        */

        // --- do something here ---
        #Mage::log(array($product->getId(), $processingType, $apiResponseItemData), null, 'amazonimportproductsaddon_1.log');

        /**
         * Example for import Size attribute = (Width + Height + Length) / 100
         *
         */

        /*
        $size = ($apiResponseItemData->ItemAttributes->ItemDimensions->Width->_
        + $apiResponseItemData->ItemAttributes->ItemDimensions->Height->_
        + $apiResponseItemData->ItemAttributes->ItemDimensions->Length->_) / 100;

        Mage::getSingleton('catalog/product_action')
            ->updateAttributes(array($product->getId()), array('size' => $size), Mage_Core_Model_App::ADMIN_STORE_ID);
        */

        return $this;
    }

    public function categoryCreateBefore(Varien_Event_Observer $observer)
    {
        /*
        $category = $observer->getData('category');
        */

        // --- do something here ---
        #Mage::log($category->getData(), null, 'amazonimportproductsaddon_2.log');

        /*
        $data = array(
            'estimated_delivery_enable' => 0,
            'estimated_shipping_enable' => 0,
        );
        $category->addData($data);
        */

        return $this;
    }

    public function productCreateBefore(Varien_Event_Observer $observer)
    {
        /*
        $product                = $observer->getData('product');
        $apiResponseItemData    = $observer->getData('apiResponseItemData');
        */

        // --- do something here ---
        #Mage::log(array($product->getData(), $apiResponseItemData), null, 'amazonimportproductsaddon_3.log');

        return $this;
    }

    public function stockCreateBefore(Varien_Event_Observer $observer)
    {
        /*
        $stock                  = $observer->getData('stock');
        $product                = $observer->getData('product');
        $apiResponseItemData    = $observer->getData('apiResponseItemData');
        */

        // --- do something here ---
        #Mage::log(array($stock->getData(), $product->getId()), null, 'amazonimportproductsaddon_4.log');

        return $this;
    }
}
