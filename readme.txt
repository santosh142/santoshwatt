----------------------------------------------------------------------
Magento Extension - Amazon Products Manager (Web-Experiment.Info)
----------------------------------------------------------------------
Thank you for choosing the Web-Experiment Magento extension.

INSTALLATION GUIDE
You can find the installation guide here:
http://web-experiment.info/articles/cat/amazon/

SUPPORT
If you have any questions, please feel free to email us via our support form here: 
http://web-experiment.info/support

LICENSING
This extension is licensed under the Regular License: 
http://web-experiment.info/regular_license


