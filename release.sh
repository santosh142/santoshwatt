#!/bin/bash

## This script needs to be run from the magento root directory

## Disable the compiler
echo "==Compiler=="
echo "* Clearing the compiler"
php -f shell/compiler.php clear
echo "* Disabling the compiler"
php -f shell/compiler.php disable

## Get most recent master
echo "==Updating branch=="
echo "* Getting most recent master branch"
git fetch
echo "* Doing hard reset to origin/master"
git reset --hard origin/master

## Clear cache and sessions
echo "==Clearing Cache and Session=="
echo "* Clearing cache"
rm -rf var/cache/*
echo "flushall" | nc -U /var/tmp/redis-multi_wattdoes.wattdoesituse.com_cache.sock

echo "* Clearing Session"
rm -rf var/session/*
echo "flush_all" | nc -U /var/tmp/memcached.wattdoes.wattdoesituse.com_sessions.sock

echo "UPGRADE COMPLETE!! YAY!!!!"